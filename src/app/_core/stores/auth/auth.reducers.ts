import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { UserData } from '@auth-models';
import { fromAuthActions } from '@stores/auth/auth.actions';

export const ENTITY_FEATURE_KEY = 'authentication';

export interface State extends EntityState<UserData> {
  isLoading: boolean;
  error?: Error | any;
}

export const adapter: EntityAdapter<UserData> = createEntityAdapter<UserData>({
  selectId: (item: UserData) => item.id
});

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoading: false,
  error: null
});

const reducer = createReducer(
  initialState,
  on(fromAuthActions.loginAuth, state => {
    return {
      ...state,
      isLoading: true
    };
  }),
  on(
    fromAuthActions.loginAuthSuccess,
    (state, { data }) => {
      return adapter.setOne(data.data, {
        ...state,
        isLoading: false
      });
    }
  ),
  on(
    fromAuthActions.loginAuthFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoading: false,
        error
      };
    }
  )
);

// tslint:disable-next-line:typedef
export function AuthReducer(
  state: State | undefined,
  action: Action
) {
  return reducer(state, action);
}
