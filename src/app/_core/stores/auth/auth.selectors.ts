import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  State,
  ENTITY_FEATURE_KEY
} from '@core/stores/auth/auth.reducers';

// Lookup feature state managed by NgRx
const getState = createFeatureSelector<State>(ENTITY_FEATURE_KEY);

// select loaded flag
export const selectIsLoading = createSelector(
  getState,
  state => state.isLoading
);

// select error
export const selectError = createSelector(getState, state => state.error);
