import { createAction, props } from '@ngrx/store';
import { User } from '@auth-models';

export enum AuthActionTypes {
  LoginAuth = '[AUTHENTICATION] Login Authentication',
  LoginAuthSuccess = '[AUTHENTICATION] Login Authentication Success',
  LoginAuthFailure = '[AUTHENTICATION] Login Authentication Failure'
}

// Login
export const loginAuth = createAction(
  AuthActionTypes.LoginAuth,
  props<{ body: any }>()
);

export const loginAuthSuccess = createAction(
  AuthActionTypes.LoginAuthSuccess,
  props<{ data: User }>()
);

export const loginAuthFailure = createAction(
  AuthActionTypes.LoginAuthFailure,
  props<{ error: Error | any }>()
);

export const fromAuthActions = {
  loginAuth,
  loginAuthSuccess,
  loginAuthFailure
};
