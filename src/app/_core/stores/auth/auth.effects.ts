import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of, timer } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { undo } from 'ngrx-undo';

import { fromAuthActions } from '@stores/auth/auth.actions';
import { AuthService } from '@services';
import { User } from '@auth-models';

@Injectable()
export class AuthEffects {
  // Login
  loginAuthentication$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromAuthActions.loginAuth),
      switchMap(action =>
        this.authService.login(action.body).pipe(
          map((result: User) =>
            fromAuthActions.loginAuthSuccess({
              data: result
            })
          ),
          catchError((error: HttpErrorResponse) => {
            return of(
              fromAuthActions.loginAuthFailure({
                error: error.statusText
              })
            );
          })
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private authService: AuthService
  ) {}
}
