import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { RecapitulationDailyContent } from '@pikobar-models';
import { fromRecapitulationDailyActions } from '@stores/pikobar/recapitulation-daily/recapitulation-daily.actions';

export const ENTITY_FEATURE_KEY = 'recapitulation daily';

export interface State extends EntityState<RecapitulationDailyContent> {
  metadata: any;
  isLoading: boolean;
  error?: Error | any;
}

export const adapter: EntityAdapter<RecapitulationDailyContent> = createEntityAdapter<RecapitulationDailyContent>({
  selectId: item => String(item.tanggal)
});

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  metadata: {},
  isLoading: false,
  error: null
});

const reducer = createReducer(
  initialState,
  on(fromRecapitulationDailyActions.loadRecapitulationDaily, state => {
    return {
      ...state,
      isLoading: true
    };
  }),
  on(
    fromRecapitulationDailyActions.loadRecapitulationDailySuccess,
    (state, { data, metadata }) => {
      return adapter.setAll(data, {
        ...state,
        metadata,
        isLoading: false
      });
    }
  ),
  on(
    fromRecapitulationDailyActions.loadRecapitulationDailyFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoading: false,
        error
      };
    }
  ),
  on(
    fromRecapitulationDailyActions.clearRecapitulationDaily,
    state => adapter.removeAll({ ...state })
  )
);

// tslint:disable-next-line:typedef
export function RecapitulationDailyReducer(
  state: State | undefined,
  action: Action
) {
  return reducer(state, action);
}
