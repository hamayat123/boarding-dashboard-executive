import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  State,
  adapter,
  ENTITY_FEATURE_KEY
} from '@stores/pikobar/recapitulation-daily/recapitulation-daily.reducers';

// Lookup feature state managed by NgRx
const getState = createFeatureSelector<State>(ENTITY_FEATURE_KEY);

// get the selector
const { selectIds, selectAll } = adapter.getSelectors();

// select the array of ids
export const selectRecapitulationDailyIds = createSelector(
  getState,
  selectIds
);

// select the array
export const selectAllRecapitulationDaily = createSelector(
  getState,
  selectAll
);

// select loaded flag
export const selectIsLoading = createSelector(
  getState,
  state => state.isLoading
);

// select error
export const selectError = createSelector(getState, state => state.error);
