import { createAction, props } from '@ngrx/store';
import { RecapitulationDailyContent, RecapitulationDailyMetadata } from '@pikobar-models';

export enum RecapitulationDailyActionTypes {
  loadRecapitulationDaily = '[CHART TERKONFIRMASI] Load Recapitulation Daily',
  loadRecapitulationDailySuccess = '[CHART TERKONFIRMASI] Load Recapitulation Daily Success',
  loadRecapitulationDailyFailure = '[CHART TERKONFIRMASI] Load Recapitulation Daily Failure',
  clearRecapitulationDaily = '[CHART TERKONFIRMASI] Clear Recapitulation Daily'
}

// Load Data
export const loadRecapitulationDaily = createAction(
  RecapitulationDailyActionTypes.loadRecapitulationDaily,
  props<{ params: string }>()
);

export const loadRecapitulationDailySuccess = createAction(
  RecapitulationDailyActionTypes.loadRecapitulationDailySuccess,
  props<{
    metadata: RecapitulationDailyMetadata,
    data: RecapitulationDailyContent[]
  }>()
);

export const loadRecapitulationDailyFailure = createAction(
  RecapitulationDailyActionTypes.loadRecapitulationDailyFailure,
  props<{ error: Error | any }>()
);

// Clear
export const clearRecapitulationDaily = createAction(
  RecapitulationDailyActionTypes.clearRecapitulationDaily
);

export const fromRecapitulationDailyActions = {
  loadRecapitulationDaily,
  loadRecapitulationDailySuccess,
  loadRecapitulationDailyFailure,
  clearRecapitulationDaily
};
