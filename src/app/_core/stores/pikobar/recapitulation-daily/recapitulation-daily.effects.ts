import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of, timer } from 'rxjs';
import { map, switchMap, catchError, mergeMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { undo } from 'ngrx-undo';

import { RecapitulationDailyService } from '@pikobar-services';
import { fromRecapitulationDailyActions } from '@stores/pikobar/recapitulation-daily/recapitulation-daily.actions';

@Injectable()
export class RecapitulationDailyEffects {
  // Load Data
  loadRecapitulationDaily$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromRecapitulationDailyActions.loadRecapitulationDaily),
      switchMap(action =>
        timer(0, environment.refreshData).pipe(
          mergeMap(() =>
            this.recapitulationDailyService.getItems(action.params).pipe(
              map((result: any) =>
                fromRecapitulationDailyActions.loadRecapitulationDailySuccess({
                  data: result.data,
                  metadata: result.metadata,
                })
              ),
              catchError((error: HttpErrorResponse) => {
                return of(
                  fromRecapitulationDailyActions.loadRecapitulationDailyFailure({
                    error: error.statusText
                  })
                );
              })
            )
          )
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private recapitulationDailyService: RecapitulationDailyService
  ) {}
}
