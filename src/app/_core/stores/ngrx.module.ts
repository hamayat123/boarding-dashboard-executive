import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from 'src/environments/environment';
import { handleUndo } from 'ngrx-undo';

import { AuthEffects } from '@stores/auth/auth.effects';
import { AuthReducer } from '@stores/auth/auth.reducers';
import { RecapitulationDailyReducer } from '@stores/pikobar/recapitulation-daily/recapitulation-daily.reducers';
import { RecapitulationDailyEffects } from '@stores/pikobar/recapitulation-daily/recapitulation-daily.effects';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forRoot(
      {
        auth: AuthReducer,
        recapitulationDaily: RecapitulationDailyReducer
      },
      {
        metaReducers: [handleUndo]
      }
    ),
    EffectsModule.forRoot([
      AuthEffects,
      RecapitulationDailyEffects
    ]),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production
    })
  ],
  exports: [StoreModule, EffectsModule, StoreDevtoolsModule]
})
export class NgrxModule {}
