import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  AfterViewInit,
  SimpleChanges,
  ChangeDetectorRef,
  ViewChild,
  ElementRef,
  ViewEncapsulation, Input,
} from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

// SERVICE
import { GlobalService, PikobarService } from '@core/services';
import { ChartPcrSumberService } from './chart-pcr-sumber.service';

// PACKAGE
import * as _ from 'lodash';
import * as moment from 'moment';
import * as d3 from 'd3';
import { TranslateService } from '@ngx-translate/core';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';

@Component({
  selector: 'app-comp-chart-pcr-sumber',
  templateUrl: './chart-pcr-sumber.component.html',
  styleUrls: ['./chart-pcr-sumber.component.scss'],
  providers: [ChartPcrSumberService],
  encapsulation: ViewEncapsulation.None
})

export class ChartPcrSumberComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  @ViewChild('chartPcrSumber')
  private chartContainer!: ElementRef;

  @Input() dataPcrSumber!: any;
  @Input() dataFilters!: any;

  dataset!: any;

  moment: any = moment;

  margin = {top: 40, right: 20, bottom: 100, left: 40};
  contentWidth = 0;
  contentHeight = 0;

  loading = true;
  lastWeek: number[] = [];

  animation: any;
  svg: any;
  canvas: any;
  context: any;
  g: any;
  x: any;
  y: any;
  xAxis: any;
  yAxis: any;
  yAxisGrid: any;
  valueline: any;
  path: any;
  pathLength: any;
  dataPoint: any;
  legends: any;
  importantDates: any;
  tooltip: any;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private globalService: GlobalService,
    private pikobarService: PikobarService,
    private chartService: ChartPcrSumberService,
    private translateService: TranslateService,
    private localize: LocalizeRouterService
  ) {
    setTimeout(() => {
      this.initData();
    }, 500);
  }

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.dataPcrSumber && (changes.dataPcrSumber.previousValue === undefined)) {
      return;
    }

    if (changes.dataPcrSumber && (!_.isEqual(changes.dataPcrSumber.currentValue, changes.dataPcrSumber.previousValue))) {
      return;
    }

    this.loading = true;
    this.updateData();
  }

  ngOnDestroy(): void {
    window.cancelAnimationFrame(this.animation);
  }

  ngAfterViewInit(): void {}

  // Main Function
  onResize(event: any): void {
    this.updateData(99);
  }

  initData(): void {
    this.updateData();
  }

  updateData(percent: number = 0): void {
    if (!this.dataPcrSumber) {
      setTimeout(() => {
        this.updateData();
      }, 500);
      return;
    }

    switch (this.dataFilters.periodeOption.key) {
      case 'harian':
        if (!this.dataPcrSumber[0].tanggal) {
          setTimeout(() => {
            this.updateData();
          }, 500);
          return;
        }
        this.dataset = this.dataPcrSumber.map((content: any) => {
            return {
              sumber: content.sumber,
              sum_sumber: d3.sum(content.sumber, (d: any) => d.jumlah_sampel),
              tanggal: content.tanggal
            };
          }).filter((content: any) => this.chartService.filterDate(this.dataFilters, content.tanggal));
        break;

      case 'mingguan':
        if (!this.dataPcrSumber[0].minggu) {
          setTimeout(() => {
            this.updateData();
          }, 500);
          return;
        }
        this.dataset = this.dataPcrSumber.map((content: any) => {
          return {
            sumber: content.sumber,
            sum_sumber: d3.sum(content.sumber, (d: any) => d.jumlah_sampel),
            tanggal: content.minggu,
            tanggal_awal: content.tanggal_awal,
            tanggal_akhir: content.tanggal_akhir,
          };
        }).filter((content: any) => this.chartService.filterDate(this.dataFilters, content.tanggal));
        break;

      default:
        if (!this.dataPcrSumber[0].dwiminggu) {
          setTimeout(() => {
            this.updateData();
          }, 500);
          return;
        }

        this.dataset = this.dataPcrSumber.map((content: any) => {
          return {
            sumber: content.sumber,
            sum_sumber: d3.sum(content.sumber, (d: any) => d.jumlah_sampel),
            tanggal: content.dwiminggu,
            tanggal_awal: content.tanggal_awal,
            tanggal_akhir: content.tanggal_akhir,
          };
        }).filter((content: any) => this.chartService.filterDate(this.dataFilters, content.tanggal));
        break;
    }

    const element = this.chartContainer.nativeElement;
    d3.select(element).selectAll('svg').remove();
    d3.select(element).selectAll('canvas').remove();

    this.createChart(percent);

    this.loading = false;
  }

  createChart(percent: number = 0): void {
    if (this.animation) {
      window.cancelAnimationFrame(this.animation);
    }
    const element = this.chartContainer.nativeElement;

    this.contentWidth = element.offsetWidth - this.margin.left - this.margin.right;
    this.contentHeight = element.offsetHeight - this.margin.top - this.margin.bottom;

    this.initDataChart();

    // Draw axis & grid
    this.initSVG('axis-grid-container');
    this.drawGrid();
    this.drawAxis();
    // this.drawLabel();

    // Draw bars & line
    this.initCanvas('pcr-sumber-canvas');
    this.drawBarCanvas(percent);

    // Draw Legend & highlight
    this.initCanvasHighlight();
    this.drawLegend();
  }

  // Graphic Function
  initDataChart(): void {
    const data = this.dataset;

    this.x = d3
      .scaleBand()
      .range([20, this.contentWidth])
      .padding(0.5)
      .domain(data.map((d: any) => d.tanggal));

    this.y = d3
      .scaleLinear()
      .rangeRound([this.contentHeight, 0])
      .domain([0, d3.max(data, (d: any) => (d.sum_sumber * 1)) as number]);

    this.dataPoint = [];
    this.dataset.forEach((d: any) => {
      this.dataPoint.push({
        x: this.x(d.tanggal),
        y: this.y(d.sum_sumber),
        ...d
      });
    });

    this.xAxis = d3
      .axisBottom(this.x)
      .tickFormat((d: any) => this.chartService.reformatDate(d))
      .tickValues(this.x.domain().filter((d: any, i: any) => !(i % (Math.floor(this.x.domain().length / 16)))));

    this.yAxis = d3.axisLeft(this.y)
      .tickSize(-this.contentWidth)
      .ticks(5);

    this.yAxisGrid = d3.axisLeft(this.y)
      .tickSize(-this.contentWidth)
      .ticks(10)
      .tickFormat((d: any) =>  '');

    if (this.importantDates === undefined) {
      this.importantDates = this.chartService.getImportantDates();
    }

    if (this.legends === undefined) {
      this.legends = [
        {
          text: 'Man. Lab',
          key: 'Manlab',
          color: '#069550',
          type: 'rect',
          fillType: 'solid',
          show: true,
          selector: '.pcr-sumber-1'
        },
        {
          text: 'All Record',
          key: 'All Record TC-19',
          color: '#FFA600',
          type: 'rect',
          fillType: 'solid',
          show: true,
          selector: '.pcr-sumber-2'
        },
        {
          text: 'Jejaring Lab',
          key: 'Jejaring Lab',
          color: '#1E88E5',
          type: 'rect',
          fillType: 'solid',
          show: true,
          selector: '.pcr-sumber-3'
        },
      ];
    }

    d3.selectAll('.pcr-sumber-tooltip').remove();
    this.tooltip = d3.select('body').append('div')
      .attr('class', 'chart-tooltip pcr-sumber-tooltip')
      .style('opacity', 0);
  }

  initSVG(className: string = ''): void {
    const element = this.chartContainer.nativeElement;

    this.svg = d3.select(element).append('svg')
      .attr('width', element.offsetWidth)
      .attr('height', element.offsetHeight + 50);

    if (className) {
      this.svg = this.svg.attr('class', className);
    }

    this.g = this.svg.append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
  }

  initCanvas(className: string): void {
    const element = this.chartContainer.nativeElement;

    this.canvas = d3.select(element).append('canvas')
      .attr('width', this.contentWidth)
      .attr('height', this.contentHeight)
      .style('position', 'absolute')
      .style('left', this.margin.left + 'px')
      .style('top', this.margin.top + 'px');

    this.context = this.canvas.node().getContext('2d');

    if (className) {
      this.canvas = this.canvas.attr('class', className);
    }
  }

  initCanvasHighlight(): void {
    const element = this.chartContainer.nativeElement;

    const highlightCanvas = d3.select(element).append('canvas')
      .attr('width', this.contentWidth)
      .attr('height', this.contentHeight)
      .style('position', 'absolute')
      .attr('class', 'highlight-pcr-sumber')
      .style('left', this.margin.left + 'px')
      .style('top', this.margin.top + 'px') as any;

    const highlightContext = highlightCanvas.node().getContext('2d');

    d3.select('.highlight-pcr-sumber')
      .on('mouseover', (event: any, d: any) => {
        this.tooltip.transition()
          .duration(100)
          .style('opacity', .9);
      })
      .on('mousemove', (event: any, d: any) => {
        let html = '';

        const rect = highlightCanvas.node().getBoundingClientRect();
        const canvasX = event.clientX - rect.left;

        // Check pcr sumber
        const selectedData = this.dataPoint.filter((content: any) => ( // cek kalo cursor dalam object
          canvasX >= content.x
          && canvasX <= content.x + this.x.bandwidth()));

        if (selectedData.length > 0) {
          const selectedX = selectedData[0].x;
          const selectedY = selectedData[0].y;

          highlightContext.clearRect(0, 0, this.contentWidth, this.contentHeight);
          highlightContext.globalAlpha = 0.6;
          this.drawRect(
            highlightContext,
            selectedX,
            selectedY,
            this.x.bandwidth(),
            this.contentHeight - selectedY,
            '#fff'
          );
          highlightContext.globalAlpha = 1;

          this.tooltip.transition()
            .duration(100)
            .style('opacity', .9);

          html = '<b>' + this.chartService.reformatDate(selectedData[0].tanggal)
            + '</b>';

          if (selectedData[0].tanggal_awal) {
            html = '<b>'
              + this.chartService.reformatDate(selectedData[0].tanggal_awal)
              + ' s/d '
              + this.chartService.reformatDate(selectedData[0].tanggal_akhir)
              + '</b>';
          }

          selectedData[0].sumber.forEach((d2: any, k: any) => {
            html += '<br/>' + d2.sumber + ': '
              + this.chartService.numberFormat(d2.jumlah_sampel);
          });
        }

        if (selectedData.length > 0) {
          this.tooltip.transition()
            .duration(100)
            .style('opacity', .9);
          this.tooltip.html(html)
            .style('left', (event.pageX + 10) + 'px')
            .style('top', (event.pageY - 28) + 'px');
        } else {
          this.tooltip.transition()
            .duration(500)
            .style('opacity', 0);
        }
      })
      .on('mouseout', (d: any) => {
        highlightContext.clearRect(0, 0, this.contentWidth, this.contentHeight);

        this.tooltip.transition()
          .duration(500)
          .style('opacity', 0);
      });
  }

  drawGrid(): void {
    this.g.append('g')
      .attr('class', 'grid grid--horizontal')
      .call(this.yAxisGrid);
  }

  drawAxis(): void {
    // bottom axis
    this.g.append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', 'translate(0,' + this.contentHeight + ')')
      .call(this.xAxis)
      .selectAll('text')
        .style('text-anchor', 'start')
        .attr('dx', '.9em')
        .attr('dy', '.15em')
        .attr('transform', 'rotate(65)');

    // left axis
    this.g.append('g')
      .attr('class', 'axis axis--y')
      .call(this.yAxis)
      .append('text')
        .attr('transform', 'rotate(-90)')
        .attr('y', 6)
        .attr('dy', '0.71em')
        .attr('text-anchor', 'end')
        .text('Jumlah Sampel');
  }

  drawDates(): void {
    const element = this.chartContainer.nativeElement;
    const dateAxis = d3.axisBottom(this.x)
      .tickSize(-this.contentHeight)
      .tickFormat((d: any) =>  '')
      .tickValues(this.x.domain().filter((d: any, i: any) =>
        this.importantDates.filter((d2: any) => d === d2.date).length > 0));

    this.g.append('g')
      .attr('class', 'axis axis--date')
      .attr('transform', `translate(0, ${this.contentHeight})`)
      .call(dateAxis)
      .call((d: any) => d.select('.domain').remove());

    d3.select(element).selectAll('.axis--date .tick')
      .each((d: any, k: any, n: any) => {
        const tick = d3.select(n[k]);
        const line = tick.select('line');
        const selectedDate = this.importantDates.filter((d2: any) => d === d2.date);

        tick.classed('pcr-sumber-' + selectedDate[0].date, true);
        tick.classed('d-none', !selectedDate[0]?.show);
        line.attr('stroke', selectedDate[0]?.color)
          .attr('stroke-width', 2);

        if (selectedDate[0]?.fillType === 'dashed') {
          line.attr('stroke-dasharray', '5, 5');
        }
      });
  }

  drawBarCanvas(percent: number = 0): void {
    // refresh frame
    if (percent++ < 100) {
      this.animation = window.requestAnimationFrame(() => this.drawBarCanvas(percent));
    }

    const data = this.dataset;

    this.context.clearRect(0, 0, this.contentWidth, this.contentHeight);

    let offset: any = [];
    this.legends.forEach((legend: any, legendKey: any) => {

      data.forEach((d: any, k: any) => {
        const jumlah_sampel = d.sumber.filter((d2: any) => d2.sumber === legend.key)[0]?.jumlah_sampel;
        const height = (this.contentHeight - this.y(jumlah_sampel)) * percent / 100;

        offset[k] = (legendKey === 0) ? 0 : offset[k];

        this.drawRect(
          this.context,
          this.x(d.tanggal),
          this.y(jumlah_sampel) + ((this.contentHeight - this.y(jumlah_sampel) - offset[k]) - height),
          this.x.bandwidth(),
          height,
          legend.color
        );

        offset[k] = offset[k] + height;
      });

    });
  }

  drawRect(context: any, x: number, y: number, width: number, height: number, color: string): void {
    context.fillStyle = color;
    context.fillRect(x, y, width, height);
  }

  drawLegend(): void {
    const legendSize = 20;
    const legendContainer = d3.select('.legend-pcr-sumber');
    legendContainer.selectAll('.legend-item').remove();

    d3.select('#btn-pcr-sumber-more').text('Tampilkan lebih banyak');

    const legendItem = legendContainer.selectAll('.legend-item')
      .data(this.legends)
      .enter().append('div')
      .attr('class', 'legend-item me-3 d-inline-flex align-items-center mb-3')
      .attr('id', (d: any, k: any) => 'legend-' + d.selector.replace('.', ''))
      .on('click', (e: any, d: any) => {
        let legendElement: any;

        if (e.target.nodeName === 'svg' || e.target.nodeName.toLowerCase() === 'span') {
          legendElement = e.target.parentElement;
        } else if (e.target.nodeName === 'line' || e.target.nodeName === 'circle' || e.target.nodeName === 'rect') {
          legendElement = e.target.parentElement.parentElement;
        } else {
          legendElement = e.target;
        }

        const itemSelector = d3.selectAll(d.selector);
        itemSelector.classed('d-none', d3.select(legendElement).classed('active'));
        d3.select(legendElement).classed('active', !d3.select(legendElement).classed('active'));

        this.legends = this.legends.map((content: any) => {
          content.show = d3.select('#legend-' + content.selector.replace('.', '')).classed('active');
          return {
            ...content
          };
        });
      });

    legendItem.each((d: any, i: any, n: any) => {
      const item = d3.select(n[i]);

      item.classed('active', d.show);
      item.classed('d-none', i > 2);
      item.classed('legend-more', i > 2);

      const svg = item.append('svg')
        .attr('width', legendSize)
        .attr('height', legendSize)
        .attr('class', 'me-2');

      if (d.type === 'circle') {
        svg.append('circle')
          .attr('cx', legendSize / 2)
          .attr('cy', legendSize / 2)
          .attr('r', legendSize / 3)
          .attr('fill', d.color);
      } else if (d.type === 'line') {
        const line = svg.append('line')
          .attr('x1', 0)
          .attr('y1', legendSize / 2)
          .attr('x2', legendSize)
          .attr('y2', legendSize / 2)
          .attr('stroke', d.color)
          .attr('stroke-width', 2);

        if (d.fillType === 'dashed') {
          line.attr('stroke-dasharray', '5, 5');
        }
      } else {
        svg.append('rect')
          .attr('x', legendSize / 4)
          .attr('y', legendSize / 4)
          .attr('width', legendSize / 2)
          .attr('height', legendSize / 2)
          .attr('fill', d.color);
      }

      item.append('span')
        .attr('style', 'font-size: 12px')
        .text(d.text);
    });

    this.legends.forEach((d: any) => {
      d3.select(d.selector).classed('d-none', !d.show);
    });
  }

  toggleLegend(e: any): void {
    d3.selectAll('.legend-pcr-sumber .legend-more')
      .each((d: any, i: any, n: any) => {
        const item = d3.select(n[i]);

        item.classed('d-none', !item.classed('d-none'));
      });

    if (d3.select('.legend-pcr-sumber .legend-more').classed('d-none')) {
      e.target.innerText = 'Tampilkan lebih banyak';
    } else {
      e.target.innerText = 'Tutup legend';
    }
  }
}
