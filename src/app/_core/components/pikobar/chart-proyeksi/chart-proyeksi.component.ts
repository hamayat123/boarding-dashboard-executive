import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  AfterViewInit,
  SimpleChanges,
  ChangeDetectorRef,
  ViewChild,
  ElementRef,
  ViewEncapsulation, Input,
} from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

// SERVICE
import { GlobalService, PikobarService } from '@core/services';
import { ChartProyeksiService } from './chart-proyeksi.service';

// PACKAGE
import * as _ from 'lodash';
import * as moment from 'moment';
import * as d3 from 'd3';
import { TranslateService } from '@ngx-translate/core';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';

@Component({
  selector: 'app-comp-chart-proyeksi',
  templateUrl: './chart-proyeksi.component.html',
  styleUrls: ['./chart-proyeksi.component.scss'],
  providers: [ChartProyeksiService],
  encapsulation: ViewEncapsulation.None
})

export class ChartProyeksiComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  @ViewChild('chartProyeksi')
  private chartContainer!: ElementRef;

  @Input() dataProyeksi!: any;
  @Input() dataFilters!: any;

  dataset!: any;

  moment: any = moment;

  margin = {top: 20, right: 20, bottom: 100, left: 50};
  contentWidth = 0;
  contentHeight = 0;

  loading = true;
  lastWeek: number[] = [];

  svg: any;
  canvas: any;
  context: any;
  g: any;
  x: any;
  y: any;
  xAxis: any;
  yAxis: any;
  yAxisGrid: any;
  factory: any;
  valueline: any;
  boundline: any;
  path: any;
  pathLength: any;
  zoom: any;
  dataPoint: any;
  legends: any;
  tooltip: any;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private globalService: GlobalService,
    private pikobarService: PikobarService,
    private chartService: ChartProyeksiService,
    private translateService: TranslateService,
    private localize: LocalizeRouterService
  ) {
    setTimeout(() => {
      this.initData();
    }, 500);
  }

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.dataProyeksi && (changes.dataProyeksi.previousValue === undefined)) {
      return;
    }

    this.updateData();
  }

  ngOnDestroy(): void {}

  ngAfterViewInit(): void {}

  // Main Function
  onResize(event: any): void {
    this.updateData(false);
  }

  initData(): void {
    this.updateData();
  }

  updateData(animate: boolean = true): void {
    if (!this.dataProyeksi) {
      setTimeout(() => {
        this.updateData();
      }, 500);
      return;
    }

    this.dataset = this.dataProyeksi.filter((content: any) =>
      this.chartService.filterDate(this.dataFilters, content.date));

    this.dataset = this.dataProyeksi.map((content: any, key: any) => {
      return {
        date: content.date,
        incidence: content[this.dataFilters.proyeksiOption.incidence],
        lower_bound: content[this.dataFilters.proyeksiOption.lower_bound],
        upper_bound: content[this.dataFilters.proyeksiOption.upper_bound],
      };
    }).filter((content: any) => this.chartService.filterDate(this.dataFilters, content.date));

    const element = this.chartContainer.nativeElement;
    d3.select(element).selectAll('svg').remove();
    d3.select(element).selectAll('canvas').remove();

    this.createChart(animate);

    this.loading = false;
  }

  createChart(animate: boolean = true): void {
    const element = this.chartContainer.nativeElement;

    this.contentWidth = element.offsetWidth - this.margin.left - this.margin.right;
    this.contentHeight = element.offsetHeight - this.margin.top - this.margin.bottom;

    this.initChartData();
    // Draw axis line & grid
    this.initSVG('axis-grid-container');
    this.drawGrid();
    this.drawAxis();
    // End draw

    // Draw scatter plot & line
    this.initSVG('proyeksi-line');
    this.drawBound();
    this.drawLine(animate);
    // End draw

    this.initHighlightCanvas();
  }

  // Graphic Function
  initChartData(): void {
    const data = this.dataset;

    this.zoom = d3.zoomIdentity;

    this.x = d3
      .scaleBand()
      .range([0, this.contentWidth])
      .padding(0.5)
      .domain(data.map((d: any) => d.date));

    this.y = d3
      .scaleLinear()
      .range([this.contentHeight, 0])
      .domain([0, d3.max(data, (d: any) => (d.upper_bound * 1)) as number + 5]);

    this.dataPoint = [];
    this.dataset.forEach((d: any, key: any) => {
      this.dataPoint.push({
        x: this.x(d.date),
        y: this.y(d.incidence),
        ...d
      });
    });

    this.xAxis = d3
      .axisBottom(this.x)
      .tickFormat((d: any) => this.chartService.reformatDate(d))
      .tickValues(this.x.domain().filter((d: any, i: any) => !(i % (Math.floor(this.x.domain().length / 16)))));

    this.yAxis = d3.axisLeft(this.y)
      .tickFormat((d: any) => this.chartService.numberFormat(d))
      .tickSize(-this.contentWidth)
      .ticks(5);

    this.yAxisGrid = d3.axisLeft(this.y)
      .tickSize(-this.contentWidth)
      .ticks(10)
      .tickFormat((d: any) =>  '');

    this.valueline = d3.line()
      .x((d: any) => (this.x(d.date) + (this.x.bandwidth() / 2)) || 0)
      .y((d: any) => this.y(d.incidence) || 0);

    this.boundline = d3.area()
      .x((d: any) => (this.x(d.date) + (this.x.bandwidth() / 2)) || 0)
      .y0((d: any) => this.y(d.lower_bound) || 0)
      .y1((d: any) => this.y(d.upper_bound) || 0);

    d3.selectAll('.proyeksi-tooltip').remove();
    this.tooltip = d3.select('body').append('div')
      .attr('class', 'chart-tooltip proyeksi-tooltip')
      .style('opacity', 0);
  }

  initSVG(className: string = ''): void {
    const element = this.chartContainer.nativeElement;

    this.svg = d3.select(element).append('svg')
      .attr('width', element.offsetWidth)
      .attr('height', element.offsetHeight + 50);

    if (className) {
      this.svg = this.svg.attr('class', className);
    }

    this.g = this.svg.append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
  }

  initCanvas(className: string): void {
    const element = this.chartContainer.nativeElement;

    this.canvas = d3.select(element).append('canvas')
      .attr('width', this.contentWidth)
      .attr('height', this.contentHeight)
      .style('position', 'absolute')
      .style('left', this.margin.left + 'px')
      .style('top', this.margin.top + 'px');

    if (className) {
      this.canvas = this.canvas.attr('class', className);
    }

    this.context = this.canvas.node().getContext('2d');
  }

  initHighlightCanvas(): void {
    const element = this.chartContainer.nativeElement;

    const highlightCanvas = d3.select(element).append('canvas')
      .attr('width', this.contentWidth)
      .attr('height', this.contentHeight)
      .style('position', 'absolute')
      .attr('class', 'highlight-proyeksi')
      .style('left', this.margin.left + 'px')
      .style('top', this.margin.top + 'px') as any;

    const highlightContext = highlightCanvas.node().getContext('2d');

    d3.select('.highlight-proyeksi')
      .on('mouseover', (event: any, d: any) => {
        this.tooltip.transition()
          .duration(100)
          .style('opacity', .9);
      })
      .on('mousemove', (event: any, d: any) => {
        let html = '';
        const rect = highlightCanvas.node().getBoundingClientRect();
        const canvasX = event.clientX - rect.left;
        const canvasY = event.clientY - rect.top;

        let radius = 10;
        if (this.x.bandwidth() < 5) {
          radius = this.x.bandwidth();
        }

        // cek kalo cursor dalam area
        const selectedData = this.dataPoint.filter((content: any) => (
          canvasX >= ((content.x + (this.x.bandwidth() / 2)) || 0 ) - (this.x.bandwidth() / 2)
          && canvasX <= ((content.x + (this.x.bandwidth() / 2)) || 0 ) + (this.x.bandwidth() / 2)
        ));

        if (selectedData.length > 0) {
          const cx = selectedData[0].x + (this.x.bandwidth() / 2);
          const cy = selectedData[0].y;

          highlightContext.clearRect(0, 0, this.contentWidth, this.contentHeight);
          this.drawCircle(highlightContext, cx, cy, '#ffab00', 5, true);

          html = '<b>'
            + this.chartService.reformatDate(selectedData[0].date)
            + '</b><br/>Ambang bawah: ' + this.chartService.numberFormat(selectedData[0].lower_bound || 0)
            + '<br/>Nilai: ' + this.chartService.numberFormat(selectedData[0].incidence || 0)
            + '<br/>Ambang Atas: ' + this.chartService.numberFormat(selectedData[0].upper_bound || 0);
        }

        if (selectedData.length > 0) {
          this.tooltip.transition()
            .duration(100)
            .style('opacity', .9);
          this.tooltip.html(html)
            .style('left', (event.pageX + 10) + 'px')
            .style('top', (event.pageY - 28) + 'px');
        } else {
          this.tooltip.transition()
            .duration(500)
            .style('opacity', 0);
        }
      })
      .on('mouseout', (d: any) => {
        highlightContext.clearRect(0, 0, this.contentWidth, this.contentHeight);

        this.tooltip.transition()
          .duration(500)
          .style('opacity', 0);
      });
  }

  drawGrid(): void {
    this.g.append('g')
      .attr('class', 'grid grid--horizontal')
      .call(this.yAxisGrid);
  }

  drawAxis(): void {
    // bottom axis
    this.g.append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', 'translate(0,' + this.contentHeight + ')')
      .call(this.xAxis)
      .selectAll('text')
      .style('text-anchor', 'start')
      .attr('dx', '.9em')
      .attr('dy', '.15em')
      .attr('transform', 'rotate(65)');

    // left axis
    this.g.append('g')
      .attr('class', 'axis axis--y')
      .call(this.yAxis)
      .append('text')
      .attr('transform', 'rotate(-90)')
      .attr('y', 6)
      .attr('dy', '0.71em')
      .attr('text-anchor', 'end')
      .text('proyeksi');
  }

  drawCircle(context: any, cx: number, cy: number, color: string = '#8df584', r: number = 1.5, stroke: boolean = false): void {
    context.fillStyle = color;
    context.beginPath();
    context.arc(cx, cy, r, 0, 2 * Math.PI);
    // context.closePath();
    context.fill();

    if (stroke && (r > 1.5)) {
      context.strokeStyle = '#fff';
      context.strokeWidth = 1;
      context.stroke();
    }
  }

  drawBound(): void {
    const data = this.dataset;

    this.path = this.g.append('path')
      .data([data])
      .attr('class', 'bound')
      .attr('d', this.boundline as any);

    this.pathLength = this.path.node().getTotalLength();
  }

  drawLine(animate: boolean = true): void {
    const data = this.dataset;

    this.path = this.g.append('path')
      .data([data])
      .attr('class', 'line')
      .attr('d', this.valueline as any);

    this.pathLength = this.path.node().getTotalLength();

    if (animate) {
      this.animatePath();
    }
  }

  // repeat function to animate path
  animatePath(): void {
    if (this.path) {
      this.path.attr('stroke-dasharray', this.pathLength + ' ' + this.pathLength)
        .attr('stroke-dashoffset', this.pathLength)
        .transition()
        .ease(d3.easeLinear)
        .attr('stroke-dashoffset', 0)
        .duration(2000)
        .on('end', () => setTimeout(this.animatePath, 500));
    }
  }
}
