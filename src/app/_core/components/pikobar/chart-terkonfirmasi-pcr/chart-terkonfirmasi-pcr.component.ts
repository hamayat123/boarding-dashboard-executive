import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  AfterViewInit,
  SimpleChanges,
  ChangeDetectorRef,
  ViewChild,
  ElementRef,
  ViewEncapsulation, Input,
} from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

// SERVICE
import { GlobalService, PikobarService } from '@core/services';
import { ChartTerkonfirmasiPcrService } from './chart-terkonfirmasi-pcr.service';

// PACKAGE
import * as _ from 'lodash';
import * as moment from 'moment';
import * as d3 from 'd3';
import { TranslateService } from '@ngx-translate/core';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';

@Component({
  selector: 'app-comp-chart-terkonfirmasi-pcr',
  templateUrl: './chart-terkonfirmasi-pcr.component.html',
  styleUrls: ['./chart-terkonfirmasi-pcr.component.scss'],
  providers: [ChartTerkonfirmasiPcrService],
  encapsulation: ViewEncapsulation.None
})

export class ChartTerkonfirmasiPcrComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  @ViewChild('chartTerkonfirmasiPcr')
  private chartContainer!: ElementRef;

  @Input() dataTerkonfirmasiPcr!: any;
  @Input() dataFilters!: any;

  dataset!: any;

  moment: any = moment;

  margin = {top: 60, right: 20, bottom: 100, left: 70};
  contentWidth = 0;
  contentHeight = 0;

  loading = true;
  lastWeek: number[] = [];

  animation: any;
  svg: any;
  canvas: any;
  context: any;
  g: any;
  x: any;
  y: any;
  xAxis: any;
  yAxis: any;
  yAxisGrid: any;
  valueline: any;
  path: any;
  pathLength: any;
  dataPoint: any;
  legends: any;
  importantDates: any;
  tooltip: any;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private globalService: GlobalService,
    private pikobarService: PikobarService,
    private chartService: ChartTerkonfirmasiPcrService,
    private translateService: TranslateService,
    private localize: LocalizeRouterService
  ) {
    setTimeout(() => {
      this.initData();
    }, 500);
  }

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.dataTerkonfirmasiPcr && (changes.dataTerkonfirmasiPcr.previousValue === undefined)) {
      return;
    }

    if (changes.dataTerkonfirmasiPcr
      && (!changes.dataTerkonfirmasiPcr.currentValue.terkonfirmasi || !changes.dataTerkonfirmasiPcr.currentValue.pcr)) {
      return;
    }

    this.loading = true;
    this.updateData();
  }

  ngOnDestroy(): void {
    window.cancelAnimationFrame(this.animation);
  }

  ngAfterViewInit(): void {}

  // Main Function
  onResize(event: any): void {
    this.updateData(99);
  }

  initData(): void {
    this.updateData();
  }

  updateData(percent: number = 0): void {
    if (!this.dataTerkonfirmasiPcr.terkonfirmasi || !this.dataTerkonfirmasiPcr.pcr) {
      setTimeout(() => {
        this.updateData();
      }, 500);
      return;
    }

    switch (this.dataFilters.periodeOption.key) {
      case 'harian':
        if (!this.dataTerkonfirmasiPcr.terkonfirmasi[0]?.harian || !this.dataTerkonfirmasiPcr.pcr) {
          setTimeout(() => {
            this.updateData();
          }, 500);
          return;
        }

        this.dataset = this.dataTerkonfirmasiPcr.terkonfirmasi.map((content: any) => {
            return {
              jumlah_sampel: this.dataTerkonfirmasiPcr.pcr.filter((contentPcr: any) => {
                return content.tanggal === contentPcr.tanggal;
              })[0]?.harian?.jumlah_sampel || 0,
              terkonfirmasi: content.harian.confirmation_total,
              tanggal: content.tanggal
            };
          }).filter((content: any) => this.chartService.filterDate(this.dataFilters, content.tanggal));
        break;

      case 'mingguan':
        if (!this.dataTerkonfirmasiPcr.terkonfirmasi[0]?.mingguan || !this.dataTerkonfirmasiPcr.pcr?.mingguan) {
          setTimeout(() => {
            this.updateData();
          }, 500);
          return;
        }

        this.dataset = this.dataTerkonfirmasiPcr.terkonfirmasi.map((content: any) => {
          return {
            jumlah_sampel: this.dataTerkonfirmasiPcr.pcr.mingguan.filter((contentPcr: any) => {
              return content.minggu === this.moment(contentPcr.minggu, 'YYYY-MM-DD').subtract(1, 'days').format('YYYY-MM-DD');
            })[0]?.jumlah_sampel || 0,
            jumlah_sampel_growth: this.dataTerkonfirmasiPcr.pcr.mingguan.filter((contentPcr: any) => {
              return content.minggu === this.moment(contentPcr.minggu, 'YYYY-MM-DD').subtract(1, 'days').format('YYYY-MM-DD');
            })[0]?.jumlah_sampel_growth || 0,
            terkonfirmasi: content.mingguan.confirmation_total,
            terkonfirmasi_growth: content.growth.confirmation_total,
            tanggal: content.minggu,
            tanggal_awal: content.tanggal_awal,
            tanggal_akhir: content.tanggal_akhir,
          };
        }).filter((content: any) => this.chartService.filterDate(this.dataFilters, content.tanggal));
        break;

      default:
        if (!this.dataTerkonfirmasiPcr.terkonfirmasi[0]?.dwimingguan || !this.dataTerkonfirmasiPcr.pcr?.dwimingguan) {
          setTimeout(() => {
            this.updateData();
          }, 500);
          return;
        }

        this.dataset = this.dataTerkonfirmasiPcr.terkonfirmasi.map((content: any) => {
          return {
            jumlah_sampel: this.dataTerkonfirmasiPcr.pcr.dwimingguan.filter((contentPcr: any) => {
              return content.dwiminggu === this.moment(contentPcr.dwiminggu, 'YYYY-MM-DD').subtract(1, 'days').format('YYYY-MM-DD');
            })[0]?.jumlah_sampel || 0,
            jumlah_sampel_growth: this.dataTerkonfirmasiPcr.pcr.dwimingguan.filter((contentPcr: any) => {
              return content.dwiminggu === this.moment(contentPcr.dwiminggu, 'YYYY-MM-DD').subtract(1, 'days').format('YYYY-MM-DD');
            })[0]?.jumlah_sampel_growth || 0,
            terkonfirmasi: content.dwimingguan.confirmation_total,
            terkonfirmasi_growth: content.growth.confirmation_total,
            tanggal: content.dwiminggu,
            tanggal_awal: content.tanggal_awal,
            tanggal_akhir: content.tanggal_akhir,
          };
        }).filter((content: any) => this.chartService.filterDate(this.dataFilters, content.tanggal));
        break;
    }

    if (this.dataFilters.periodeOption.key === 'mingguan' || this.dataFilters.periodeOption.key === 'dwimingguan') {
      this.margin.top = 60;
    } else {
      this.margin.top = 40;
    }

    const element = this.chartContainer.nativeElement;
    d3.select(element).selectAll('svg').remove();
    d3.select(element).selectAll('canvas').remove();

    this.createChart(percent);

    this.loading = false;
  }

  createChart(percent: number = 0): void {
    if (this.animation) {
      window.cancelAnimationFrame(this.animation);
    }
    const element = this.chartContainer.nativeElement;

    this.contentWidth = element.offsetWidth - this.margin.left - this.margin.right;
    this.contentHeight = element.offsetHeight - this.margin.top - this.margin.bottom;

    this.initDataChart();

    // Draw axis & grid
    this.initSVG('axis-grid-container');
    this.drawGrid();
    this.drawAxis();
    // this.drawLabel();

    // Draw growth
    if (this.dataFilters.periodeOption.key === 'mingguan' || this.dataFilters.periodeOption.key === 'dwimingguan') {
      this.initSVG('growth-bar');
      this.drawAxisGrowth();
      this.drawGrowth();
    }

    // Draw bars & line
    this.initCanvas('terkonfirmasi-pcr-bar-1');
    this.drawBarTerkonfirmasiCanvas(percent);
  }

  // Graphic Function
  initDataChart(): void {
    const data = this.dataset;

    this.x = d3
      .scaleBand()
      .range([20, this.contentWidth])
      .padding(0.5)
      .domain(data.map((d: any) => d.tanggal));

    this.y = d3
      .scaleLinear()
      .rangeRound([this.contentHeight, 0])
      .domain([0, Math.max(
        d3.max(data, (d: any) => (d.terkonfirmasi * 1)) as number,
        d3.max(data, (d: any) => (d.jumlah_sampel * 1)) as number)
      ]);

    this.dataPoint = [];
    this.dataset.forEach((d: any) => {
      this.dataPoint.push({
        x: this.x(d.tanggal),
        y1: this.y(d.terkonfirmasi),
        y2: this.y(d.jumlah_sampel),
        ...d
      });
    });

    this.xAxis = d3
      .axisBottom(this.x)
      .tickFormat((d: any) => this.chartService.reformatDate(d))
      .tickValues(this.x.domain().filter((d: any, i: any) => !(i % (Math.floor(this.x.domain().length / 16)))));

    this.yAxis = d3.axisLeft(this.y)
      .tickSize(-this.contentWidth)
      .ticks(5);

    this.yAxisGrid = d3.axisLeft(this.y)
      .tickSize(-this.contentWidth)
      .ticks(10)
      .tickFormat((d: any) =>  '');

    if (this.importantDates === undefined) {
      this.importantDates = this.chartService.getImportantDates();
    }

    if (this.legends === undefined) {
      this.legends = [
        {
          text: 'Terkonfirmasi',
          color: '#069550',
          type: 'rect',
          fillType: 'solid',
          show: true,
          selector: '.terkonfirmasi-pcr-bar-1'
        },
        {
          text: 'PCR',
          color: '#FFA600',
          type: 'rect',
          fillType: 'solid',
          show: true,
          selector: '.terkonfirmasi-pcr-bar-2'
        },
      ];
    }

    d3.selectAll('.terkonfirmasi-pcr-tooltip').remove();
    this.tooltip = d3.select('body').append('div')
      .attr('class', 'chart-tooltip terkonfirmasi-pcr-tooltip')
      .style('opacity', 0);
  }

  initSVG(className: string = ''): void {
    const element = this.chartContainer.nativeElement;

    this.svg = d3.select(element).append('svg')
      .attr('width', element.offsetWidth)
      .attr('height', element.offsetHeight + 50);

    if (className) {
      this.svg = this.svg.attr('class', className);
    }

    this.g = this.svg.append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
  }

  initCanvas(className: string): void {
    const element = this.chartContainer.nativeElement;

    this.canvas = d3.select(element).append('canvas')
      .attr('width', this.contentWidth)
      .attr('height', this.contentHeight)
      .style('position', 'absolute')
      .style('left', this.margin.left + 'px')
      .style('top', this.margin.top + 'px');

    this.context = this.canvas.node().getContext('2d');

    if (className) {
      this.canvas = this.canvas.attr('class', className);
    }
  }

  initCanvasHighlight(): void {
    const element = this.chartContainer.nativeElement;

    const highlightCanvas = d3.select(element).append('canvas')
      .attr('width', this.contentWidth)
      .attr('height', this.contentHeight)
      .style('position', 'absolute')
      .attr('class', 'highlight-terkonfirmasi')
      .style('left', this.margin.left + 'px')
      .style('top', this.margin.top + 'px') as any;

    const highlightContext = highlightCanvas.node().getContext('2d');

    d3.select('.highlight-terkonfirmasi')
      .on('mouseover', (event: any, d: any) => {
        this.tooltip.transition()
          .duration(100)
          .style('opacity', .9);
      })
      .on('mousemove', (event: any, d: any) => {
        let html = '';

        const rect = highlightCanvas.node().getBoundingClientRect();
        const canvasX = event.clientX - rect.left;
        const canvasY = event.clientY - rect.top;

        // Check terkonfirmasi & pcr
        const selectedData = this.dataPoint.filter((content: any) => ( // cek kalo cursor dalam object
          canvasX >= content.x
          && canvasX <= content.x + this.x.bandwidth()));

        if (selectedData.length > 0) {
          const selectedX = selectedData[0].x;
          const selectedY1 = selectedData[0].y1;
          const selectedY2 = selectedData[0].y2;

          highlightContext.clearRect(0, 0, this.contentWidth, this.contentHeight);
          highlightContext.globalAlpha = 0.6;
          this.drawRect(
            highlightContext,
            selectedX,
            selectedY1,
            (this.x.bandwidth() / 2) - 0.15,
            this.contentHeight - selectedY1,
            '#fff'
          );
          this.drawRect(
            highlightContext,
            selectedX + (this.x.bandwidth() / 2) + 0.3,
            selectedY2,
            (this.x.bandwidth() / 2) - 0.15,
            this.contentHeight - selectedY2,
            '#fff'
          );
          highlightContext.globalAlpha = 1;

          this.tooltip.transition()
            .duration(100)
            .style('opacity', .9);

          html = '<b>' + this.chartService.reformatDate(selectedData[0].tanggal)
            + '</b>';

          if (selectedData[0].tanggal_awal) {
            html = '<b>'
              + this.chartService.reformatDate(selectedData[0].tanggal_awal)
              + ' s/d '
              + this.chartService.reformatDate(selectedData[0].tanggal_akhir)
              + '</b>';
          }

          html += '<br/>Terkonfirmasi: '
            + this.chartService.numberFormat(selectedData[0].terkonfirmasi);

          if (this.dataFilters.periodeOption.key === 'mingguan' || this.dataFilters.periodeOption.key === 'dwimingguan') {
            html += ' (' + '<span class="' + (selectedData[0].terkonfirmasi_growth > 0 ? 'text-red-600' : 'text-green-600') + '">'
              + this.chartService.percentage(selectedData[0].terkonfirmasi_growth, true) + '</span>)';
          }

          html += '<br/>Tes PCR: '
            + this.chartService.numberFormat(selectedData[0].jumlah_sampel);

          if (this.dataFilters.periodeOption.key === 'mingguan' || this.dataFilters.periodeOption.key === 'dwimingguan') {
            html += ' (' + '<span class="' + (selectedData[0].jumlah_sampel_growth > 0 ? 'text-green-600' : 'text-red-600') + '">'
              + this.chartService.percentage(selectedData[0].jumlah_sampel_growth, true) + '</span>)';
          }
        }

        if (selectedData.length > 0) {
          this.tooltip.transition()
            .duration(100)
            .style('opacity', .9);
          this.tooltip.html(html)
            .style('left', (event.pageX + 10) + 'px')
            .style('top', (event.pageY - 28) + 'px');
        } else {
          this.tooltip.transition()
            .duration(500)
            .style('opacity', 0);
        }
      })
      .on('mouseout', (d: any) => {
        highlightContext.clearRect(0, 0, this.contentWidth, this.contentHeight);

        this.tooltip.transition()
          .duration(500)
          .style('opacity', 0);
      });
  }

  drawGrid(): void {
    this.g.append('g')
      .attr('class', 'grid grid--horizontal')
      .call(this.yAxisGrid);
  }

  drawAxis(): void {
    // bottom axis
    this.g.append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', 'translate(0,' + this.contentHeight + ')')
      .call(this.xAxis)
      .selectAll('text')
        .style('text-anchor', 'start')
        .attr('dx', '.9em')
        .attr('dy', '.15em')
        .attr('transform', 'rotate(65)');

    // left axis
    this.g.append('g')
      .attr('class', 'axis axis--y')
      .call(this.yAxis)
      .append('text')
        .attr('transform', 'rotate(-90)')
        .attr('y', 6)
        .attr('dy', '0.71em')
        .attr('text-anchor', 'end')
        .text('terkonfirmasi');
  }

  drawDates(): void {
    const element = this.chartContainer.nativeElement;
    const dateAxis = d3.axisBottom(this.x)
      .tickSize(-this.contentHeight)
      .tickFormat((d: any) =>  '')
      .tickValues(this.x.domain().filter((d: any, i: any) =>
        this.importantDates.filter((d2: any) => d === d2.date).length > 0));

    this.g.append('g')
      .attr('class', 'axis axis--date')
      .attr('transform', `translate(0, ${this.contentHeight})`)
      .call(dateAxis)
      .call((d: any) => d.select('.domain').remove());

    d3.select(element).selectAll('.axis--date .tick')
      .each((d: any, k: any, n: any) => {
        const tick = d3.select(n[k]);
        const line = tick.select('line');
        const selectedDate = this.importantDates.filter((d2: any) => d === d2.date);

        tick.classed('terkonfirmasi-pcr-' + selectedDate[0].date, true);
        tick.classed('d-none', !selectedDate[0]?.show);
        line.attr('stroke', selectedDate[0]?.color)
          .attr('stroke-width', 2);

        if (selectedDate[0]?.fillType === 'dashed') {
          line.attr('stroke-dasharray', '5, 5');
        }
      });
  }

  drawBarTerkonfirmasiCanvas(percent: number = 0): void {
    // refresh frame
    if (percent++ < 100) {
      this.animation = window.requestAnimationFrame(() => this.drawBarTerkonfirmasiCanvas(percent));
    }

    const data = this.dataset;

    this.context.clearRect(0, 0, this.contentWidth, this.contentHeight);

    data.forEach((d: any) => {
      const height = (this.contentHeight - this.y(d.terkonfirmasi)) * percent / 100;

      this.drawRect(
        this.context,
        this.x(d.tanggal),
        this.y(d.terkonfirmasi) + ((this.contentHeight - this.y(d.terkonfirmasi)) - height),
        (this.x.bandwidth() / 2) - 0.15,
        height,
        '#069550'
      );
    });

    if (percent === 100) {
      this.initCanvas('terkonfirmasi-pcr-bar-2');
      this.drawBarPcrCanvas();
    }
  }

  drawBarPcrCanvas(percent: number = 0): void {
    // refresh frame
    if (percent++ < 100) {
      this.animation = window.requestAnimationFrame(() => this.drawBarPcrCanvas(percent));
    }

    const data = this.dataset;

    this.context.clearRect(0, 0, this.contentWidth, this.contentHeight);

    data.forEach((d: any) => {
      const height = (this.contentHeight - this.y(d.jumlah_sampel)) * percent / 100;

      this.drawRect(
        this.context,
        this.x(d.tanggal) + (this.x.bandwidth() / 2) + 0.3,
        this.y(d.jumlah_sampel) + ((this.contentHeight - this.y(d.jumlah_sampel)) - height),
        (this.x.bandwidth() / 2) - 0.15,
        height,
        '#FFA600'
      );
    });

    if (percent === 100) {
      this.initCanvasHighlight();
      this.drawLegend();
    }
  }

  drawGrowth(): void {
    const data = this.dataset;

    const growthContainer = this.g.append('g')
      .attr('class', 'container-growth');

    const growthChart = growthContainer.selectAll('.chart-growth')
      .data(data)
      .enter().append('g')
      .attr('class', 'chart-growth');

    growthContainer.append('text')
      .attr('x', -this.margin.left)
      .attr('y', -this.margin.top + 15)
      .attr('text-anchor', 'start')
      .style('font-size', '10px')
      .style('fill', 'currentColor')
      .text('Terkonfirmasi');

    growthContainer.append('text')
      .attr('x', -this.margin.left)
      .attr('y', -this.margin.top + 15 + 25)
      .attr('text-anchor', 'start')
      .style('font-size', '10px')
      .style('fill', 'currentColor')
      .text('Test PCR');

    growthChart.each((d: any, i: any, n: any) => {
      if (i % (Math.floor(data.length / 7))) {
        return;
      }

      const growthLabel = d3.select(n[i]);

      growthLabel.append('text')
        .attr('class', 'terkonfirmasi-growth')
        .attr('x', (d2: any) => (this.x(d2.tanggal) + (this.x.bandwidth() / 2)) || 0)
        .attr('y', -this.margin.top + 15)
        .attr('text-anchor', 'middle')
        .style('font-size', '10px')
        .style('fill', (d2: any) => d2.terkonfirmasi_growth > 0 ? '#DD5E5E' : '#20A95A')
        .text((d2: any) => this.chartService.percentage(d2.terkonfirmasi_growth, true));

      growthLabel.append('text')
        .attr('class', 'jumlah-sampel-growth')
        .attr('x', (d2: any) => (this.x(d2.tanggal) + (this.x.bandwidth() / 2)) || 0)
        .attr('y', -this.margin.top + 15 + 25)
        .attr('text-anchor', 'middle')
        .style('font-size', '10px')
        .style('fill', (d2: any) => d2.jumlah_sampel_growth > 0 ? '#20A95A' : '#DD5E5E')
        .text((d2: any) => this.chartService.percentage(d2.jumlah_sampel_growth, true));
    });
  }

  drawAxisGrowth(): void {
    const element = this.chartContainer.nativeElement;
    const dateAxis = d3.axisBottom(this.x)
      .tickSize(-this.contentHeight)
      .tickFormat((d: any) =>  '')
      .tickValues(this.x.domain().filter((d: any, i: any) => !(i % (Math.floor(this.x.domain().length / 7)))));

    this.g.append('g')
      .attr('class', 'axis axis--growth')
      .attr('transform', `translate(0, ${this.contentHeight})`)
      .call(dateAxis)
      .call((d: any) => d.select('.domain').remove());

    d3.select(element).selectAll('.axis--growth .tick')
      .each((d: any, k: any, n: any) => {
        const tick = d3.select(n[k]);
        const line = tick.select('line');

        line.attr('stroke', '#999')
          .attr('stroke-width', 1)
          .attr('stroke-dasharray', '5, 5');
      });
  }

  drawRect(context: any, x: number, y: number, width: number, height: number, color: string): void {
    context.fillStyle = color;
    context.fillRect(x, y, width, height);
  }

  drawLine(animate: boolean = true): void {
    const data = this.dataset;

    this.path = this.g.append('path')
      .data([data])
      .attr('class', 'line')
      .attr('d', this.valueline as any);

    this.pathLength = this.path.node().getTotalLength();

    if (animate) {
      this.animatePath();
    }
  }

  // repeat function to animate path
  animatePath(): void {
    if (this.path) {
      this.path.attr('stroke-dasharray', this.pathLength + ' ' + this.pathLength)
        .attr('stroke-dashoffset', this.pathLength)
        .transition()
        .ease(d3.easeLinear)
        .attr('stroke-dashoffset', 0)
        .duration(2000)
        .on('end', () => setTimeout(this.animatePath, 500));
    }
  }

  drawLabel(): void{
    const data = this.dataset;

    this.g.selectAll('.text')
      .data(data)
      .enter()
      .append('text')
      .attr('class', 'chart-label terkonfirmasi-pcr-bar')
      .attr('x', (d: any) => (this.x(d.tanggal) + (this.x.bandwidth() / 2)) || 0)
      .attr('y', (d: any) => (this.y(d.terkonfirmasi) - 5) || 0)
      .attr('text-anchor', 'middle')
      .text((d: any) => this.chartService.numberFormat(d.terkonfirmasi));
  }

  drawLegend(): void {
    const legendSize = 20;
    const legendContainer = d3.select('.legend-terkonfirmasi-pcr');
    legendContainer.selectAll('.legend-item').remove();

    d3.select('#btn-terkonfirmasi-pcr-more').text('Tampilkan lebih banyak');

    const legendItem = legendContainer.selectAll('.legend-item')
      .data(this.legends)
      .enter().append('div')
      .attr('class', 'legend-item me-3 d-inline-flex align-items-center mb-3')
      .attr('id', (d: any, k: any) => 'legend-' + d.selector.replace('.', ''))
      .on('click', (e: any, d: any) => {
        let legendElement: any;

        if (e.target.nodeName === 'svg' || e.target.nodeName.toLowerCase() === 'span') {
          legendElement = e.target.parentElement;
        } else if (e.target.nodeName === 'line' || e.target.nodeName === 'circle' || e.target.nodeName === 'rect') {
          legendElement = e.target.parentElement.parentElement;
        } else {
          legendElement = e.target;
        }

        const itemSelector = d3.selectAll(d.selector);
        itemSelector.classed('d-none', d3.select(legendElement).classed('active'));
        d3.select(legendElement).classed('active', !d3.select(legendElement).classed('active'));

        this.legends = this.legends.map((content: any) => {
          content.show = d3.select('#legend-' + content.selector.replace('.', '')).classed('active');
          return {
            ...content
          };
        });
      });

    legendItem.each((d: any, i: any, n: any) => {
      const item = d3.select(n[i]);

      item.classed('active', d.show);
      item.classed('d-none', i > 2);
      item.classed('legend-more', i > 2);

      const svg = item.append('svg')
        .attr('width', legendSize)
        .attr('height', legendSize)
        .attr('class', 'me-2');

      if (d.type === 'circle') {
        svg.append('circle')
          .attr('cx', legendSize / 2)
          .attr('cy', legendSize / 2)
          .attr('r', legendSize / 3)
          .attr('fill', d.color);
      } else if (d.type === 'line') {
        const line = svg.append('line')
          .attr('x1', 0)
          .attr('y1', legendSize / 2)
          .attr('x2', legendSize)
          .attr('y2', legendSize / 2)
          .attr('stroke', d.color)
          .attr('stroke-width', 2);

        if (d.fillType === 'dashed') {
          line.attr('stroke-dasharray', '5, 5');
        }
      } else {
        svg.append('rect')
          .attr('x', legendSize / 4)
          .attr('y', legendSize / 4)
          .attr('width', legendSize / 2)
          .attr('height', legendSize / 2)
          .attr('fill', d.color);
      }

      item.append('span')
        .attr('style', 'font-size: 12px')
        .text(d.text);
    });

    this.legends.forEach((d: any) => {
      d3.select(d.selector).classed('d-none', !d.show);
    });
  }

  toggleLegend(e: any): void {
    d3.selectAll('.legend-terkonfirmasi-pcr .legend-more')
      .each((d: any, i: any, n: any) => {
        const item = d3.select(n[i]);

        item.classed('d-none', !item.classed('d-none'));
      });

    if (d3.select('.legend-terkonfirmasi-pcr .legend-more').classed('d-none')) {
      e.target.innerText = 'Tampilkan lebih banyak';
    } else {
      e.target.innerText = 'Tutup legend';
    }
  }
}
