import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class ChartTerkonfirmasiPcrService {
  private dataSource = new BehaviorSubject([]);
  data = this.dataSource.asObservable();

  constructor() {}

  setData(data: any): void {
    this.dataSource.next(data);
  }

  reformatDate(date: any): string {
    const dateArr = date.split('-');
    const months = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'];
    const reformat = `${dateArr[2]} ${months[(dateArr[1] * 1)]} ${dateArr[0]}`;

    return reformat;
  }

  weeklyAverage(lastWeek: any, confirmed: any): number {
    let weeklyAvg = 0;
    let temp = [];

    lastWeek.push(confirmed);

    if (lastWeek.length > 7) {
      lastWeek.shift();
    }

    temp = lastWeek;
    weeklyAvg = Math.floor(temp.reduce((a: any, b: any) => a + b, 0) / lastWeek.length);

    return weeklyAvg;
  }

  numberFormat(x: any): string {
    return x.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }

  percentage(x: any, plusMin: boolean = false): string {
    if (plusMin){
      const percent = x.toFixed(2);
      return (percent > 0 ? '+' : '') + percent + '%';
    } else {
      return x.toFixed(2) + '%';
    }
  }

  filterDate(dataFilters: any, tanggal: any): boolean{
    const arrDate = tanggal.split('-');
    const newDate = `${arrDate[1]}/${arrDate[2]}/${arrDate[0]}`;
    const curDate = new Date(newDate);

    const fromDate = new Date(`${dataFilters.range.from.month}/${dataFilters.range.from.day}/${dataFilters.range.from.year}`);
    const toDate = new Date(`${dataFilters.range.to.month}/${dataFilters.range.to.day}/${dataFilters.range.to.year}`);

    return curDate.getTime() >= fromDate.getTime() && curDate.getTime() <= toDate.getTime();
  }

  getImportantDates(): Array<any> {
    return [
      {
        text: 'Bekerja, Belajar, dan Beribadah dari Rumah (15 Maret 20)',
        date: '2020-03-15',
        color: '#4d7c62',
        type: 'line',
        fillType: 'dashed',
        show: false
      },
      {
        text: 'PSBB Bodebek (15 April 20)',
        date: '2020-04-15',
        color: '#b77d36',
        type: 'line',
        fillType: 'dashed',
        show: false
      },
      {
        text: 'PSBB Bandung Raya (22 April 20)',
        date: '2020-04-22',
        color: '#597791',
        type: 'line',
        fillType: 'solid',
        show: false
      },
      {
        text: 'PSBB Jawa Barat (6 Mei 20)',
        date: '2020-05-06',
        color: '#824b83',
        type: 'line',
        fillType: 'solid',
        show: true
      },
      {
        text: 'PSBB Proporsional (15 Mei 20)',
        date: '2020-05-15',
        color: '#318e8f',
        type: 'line',
        fillType: 'dashed',
        show: true
      },
      {
        text: 'AKB:30 (26 Juni 20)',
        date: '2020-06-26',
        color: '#c06a58',
        type: 'line',
        fillType: 'dashed',
        show: true
      },
      {
        text: 'Idul Adha (31 juli 20)',
        date: '2020-07-31',
        color: '#4f9636',
        type: 'line',
        fillType: 'dashed',
        show: true
      },
      {
        text: 'Akhir Pekan Panjang (20 Agustus 20)',
        date: '2020-08-20',
        color: '#519eb6',
        type: 'line',
        fillType: 'solid',
        show: false
      },
      {
        text: 'Demo Buruh (6 Oktober 20)',
        date: '2020-10-06',
        color: '#a1764e',
        type: 'line',
        fillType: 'dashed',
        show: true
      },
      {
        text: 'Akhir Pekan Panjang (28 Oktober 20)',
        date: '2020-10-28',
        color: '#519eb6',
        type: 'line',
        fillType: 'solid',
        show: false
      },
      {
        text: 'Tabligh Akbar (13 November 20)',
        date: '2020-11-13',
        color: '#7449aa',
        type: 'line',
        fillType: 'dashed',
        show: true
      },
      {
        text: 'Pilkada Serentak (9 Desember 20)',
        date: '2020-12-09',
        color: '#316183',
        type: 'line',
        fillType: 'dashed',
        show: true
      },
      {
        text: 'Akhir Pekan Panjang (24 Desember 20)',
        date: '2020-12-24',
        color: '#519eb6',
        type: 'line',
        fillType: 'solid',
        show: false
      },
      {
        text: 'Akhir Pekan Panjang (31 Desember 20)',
        date: '2020-12-31',
        color: '#519eb6',
        type: 'line',
        fillType: 'dashed',
        show: false
      },
      {
        text: 'PPKM Jawa Bali (11 Januari 21)',
        date: '2021-01-11',
        color: '#316183',
        type: 'line',
        fillType: 'dashed',
        show: false
      },
      {
        text: 'Akhir Pekan Panjang (12 Februari 21)',
        date: '2021-02-12',
        color: '#519eb6',
        type: 'line',
        fillType: 'solid',
        show: false
      },
      {
        text: 'Akhir Pekan Panjang (2 April 21)',
        date: '2021-04-02',
        color: '#519eb6',
        type: 'line',
        fillType: 'solid',
        show: false
      },
      {
        text: 'Larangan Pra Mudik (22 April 21)',
        date: '2021-04-22',
        color: '#58a991',
        type: 'line',
        fillType: 'solid',
        show: true
      },
      {
        text: 'Larangan Mudik (6 Mei 21)',
        date: '2021-05-06',
        color: '#2466cb',
        type: 'line',
        fillType: 'dashed',
        show: true
      },
      {
        text: 'Larangan Pasca Mudik (18 Mei 21)',
        date: '2021-05-18',
        color: '#cb5624',
        type: 'line',
        fillType: 'solid',
        show: true
      }
    ];
  }
}
