import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  AfterViewInit,
  SimpleChanges,
  ChangeDetectorRef,
  ViewChild,
  ElementRef,
  ViewEncapsulation, Input,
} from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

// SERVICE
import { GlobalService, PikobarService } from '@core/services';
import { ChartTrenPositivityService } from './chart-tren-positivity.service';

// PACKAGE
import * as _ from 'lodash';
import * as moment from 'moment';
import * as d3 from 'd3';
import { TranslateService } from '@ngx-translate/core';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';

@Component({
  selector: 'app-comp-chart-tren-positivity',
  templateUrl: './chart-tren-positivity.component.html',
  styleUrls: ['./chart-tren-positivity.component.scss'],
  providers: [ChartTrenPositivityService],
  encapsulation: ViewEncapsulation.None
})

export class ChartTrenPositivityComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  @ViewChild('chartTrenPositivity')
  private chartContainer!: ElementRef;

  @Input() dataTrenPcr!: any;
  @Input() dataFilters!: any;

  dataset!: any;

  moment: any = moment;

  margin = {top: 20, right: 20, bottom: 100, left: 40};
  contentWidth = 0;
  contentHeight = 0;

  loading = true;
  lastWeek: number[] = [];

  animation: any;
  svg: any;
  canvas: any;
  context: any;
  g: any;
  x: any;
  y: any;
  xAxis: any;
  yAxis: any;
  yAxisGrid: any;
  valueline: any;
  path: any;
  pathLength: any;
  dataPoint: any;
  legends: any;
  importantDates: any;
  tooltip: any;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private globalService: GlobalService,
    private pikobarService: PikobarService,
    private chartService: ChartTrenPositivityService,
    private translateService: TranslateService,
    private localize: LocalizeRouterService
  ) {
    setTimeout(() => {
      this.initData();
    }, 500);
  }

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.dataTrenPcr && (changes.dataTrenPcr.previousValue === undefined)) {
      return;
    }

    if (changes.dataTrenPcr && (!_.isEqual(changes.dataTrenPcr.currentValue, changes.dataTrenPcr.previousValue))) {
      return;
    }

    this.updateData();
  }

  ngOnDestroy(): void {
    window.cancelAnimationFrame(this.animation);
  }

  ngAfterViewInit(): void {}

  // Main Function
  onResize(event: any): void {
    this.updateData(99, false);
  }

  initData(): void {
    this.updateData();
  }

  updateData(percent: number = 0, animate: boolean = true): void {
    if (!this.dataTrenPcr || !this.dataTrenPcr[this.dataFilters.periodeOption.key]) {
      setTimeout(() => {
        this.updateData();
      }, 500);
      return;
    }

    this.lastWeek = [];
    this.dataset = this.dataTrenPcr[this.dataFilters.periodeOption.key].map((content: any, key: any) => {
      return {
        jumlah_sampel: content.jumlah_sampel,
        positif: content.positif,
        tanggal: this.dataFilters.periodeOption.key === 'mingguan' ? content.minggu : content.dwiminggu,
        tanggal_awal: content.tanggal_awal,
        tanggal_akhir: content.tanggal_akhir,
      };
    }).filter((content: any) => this.chartService.filterDate(this.dataFilters, content.tanggal));

    const element = this.chartContainer.nativeElement;
    d3.select(element).selectAll('svg').remove();
    d3.select(element).selectAll('canvas').remove();

    this.createChart(percent, animate);

    this.loading = false;
  }

  createChart(percent: number = 0, animate: boolean = true): void {
    if (this.animation) {
      window.cancelAnimationFrame(this.animation);
    }
    const element = this.chartContainer.nativeElement;

    this.contentWidth = element.offsetWidth - this.margin.left - this.margin.right;
    this.contentHeight = element.offsetHeight - this.margin.top - this.margin.bottom;

    this.initDataChart();

    // Draw axis & grid
    this.initSVG('axis-grid-container');
    this.drawGrid();
    this.drawAxis();
    // this.drawLabel();
    // End draw

    // Draw bars & line
    this.initCanvas('tren-positivity-bar');
    this.initSVG('tren-positivity-line');
    this.drawBarCanvas(percent, animate);
    // this.drawLineCanvas();
    // End draw
    this.initCanvasHighlight();
    this.drawLegend();
  }

  // Graphic Function

  initDataChart(): void {
    const data = this.dataset;

    this.x = d3
      .scaleBand()
      .range([20, this.contentWidth])
      .padding(0.5)
      .domain(data.map((d: any) => d.tanggal));

    this.y = d3
      .scaleLinear()
      .rangeRound([this.contentHeight, 0])
      .domain([0, d3.max(data, (d: any) => (d.jumlah_sampel * 1)) as number]);

    this.dataPoint = [];
    this.dataset.forEach((d: any) => {
      this.dataPoint.push({
        x: this.x(d.tanggal),
        y: this.y(d.jumlah_sampel),
        yPositivity: this.y(d.positif),
        ...d
      });
    });

    this.xAxis = d3
      .axisBottom(this.x)
      .tickFormat((d: any) => this.chartService.reformatDate(d))
      .tickValues(this.x.domain().filter((d: any, i: any) => !(i % (Math.floor(this.x.domain().length / 16)))));

    this.yAxis = d3.axisLeft(this.y)
      .tickSize(-this.contentWidth)
      .ticks(5);

    this.yAxisGrid = d3.axisLeft(this.y)
      .tickSize(-this.contentWidth)
      .ticks(10)
      .tickFormat((d: any) =>  '');

    this.valueline = d3
      .line()
      .x((d: any) => (this.x(d.tanggal) + (this.x.bandwidth() / 2)) || 0)
      .y((d: any) => this.y(d.positif) || 0)
      .curve(d3.curveMonotoneX);

    if (this.importantDates === undefined) {
      this.importantDates = this.chartService.getImportantDates();
    }

    if (this.legends === undefined) {
      this.legends = [
        {
          text: 'Jumlah Sampel',
          color: '#292C2A',
          type: 'rect',
          fillType: 'solid',
          show: true,
          selector: '.tren-positivity-bar'
        },
        {
          text: 'Positivity Rate',
          color: '#069550',
          type: 'line',
          fillType: 'solid',
          show: true,
          selector: '.tren-positivity-line'
        },
      ];
    }

    d3.selectAll('.tren-positivity-tooltip').remove();
    this.tooltip = d3.select('body').append('div')
      .attr('class', 'chart-tooltip tren-positivity-tooltip')
      .style('opacity', 0);
  }

  initSVG(className: string = ''): void {
    const element = this.chartContainer.nativeElement;

    this.svg = d3.select(element).append('svg')
      .attr('width', element.offsetWidth)
      .attr('height', element.offsetHeight + 50);

    if (className) {
      this.svg = this.svg.attr('class', className);
    }

    this.g = this.svg.append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
  }

  initCanvas(className: string): void {
    const element = this.chartContainer.nativeElement;

    this.canvas = d3.select(element).append('canvas')
      .attr('width', this.contentWidth)
      .attr('height', this.contentHeight)
      .style('position', 'absolute')
      .style('left', this.margin.left + 'px')
      .style('top', this.margin.top + 'px');

    this.context = this.canvas.node().getContext('2d');

    if (className) {
      this.canvas = this.canvas.attr('class', className);
    }
  }

  initCanvasHighlight(): void {
    const element = this.chartContainer.nativeElement;

    const highlightCanvas = d3.select(element).append('canvas')
      .attr('width', this.contentWidth)
      .attr('height', this.contentHeight)
      .style('position', 'absolute')
      .attr('class', 'highlight-tren-positivity')
      .style('left', this.margin.left + 'px')
      .style('top', this.margin.top + 'px') as any;

    const highlightContext = highlightCanvas.node().getContext('2d');

    d3.select('.highlight-tren-positivity')
      .on('mouseover', (event: any, d: any) => {
        this.tooltip.transition()
          .duration(100)
          .style('opacity', .9);
      })
      .on('mousemove', (event: any, d: any) => {
        let html = '';

        const rect = highlightCanvas.node().getBoundingClientRect();
        const canvasX = event.clientX - rect.left;
        const canvasY = event.clientY - rect.top;
        const selectedData = this.dataPoint.filter((content: any) => (
          canvasX >= ((content.x + (this.x.bandwidth() / 2)) || 0 ) - (this.x.bandwidth() / 2)
          && canvasX <= ((content.x + (this.x.bandwidth() / 2)) || 0 ) + (this.x.bandwidth() / 2)
        ));

        if (selectedData.length > 0) {
          const cx2 = ((selectedData[0].x + (this.x.bandwidth() / 2)) || 0 );
          const cy2 = selectedData[0].yPositivity;
          const selectedX = selectedData[0].x;
          const selectedY = selectedData[0].y;

          highlightContext.clearRect(0, 0, this.contentWidth, this.contentHeight);

          // Draw highlight bar
          highlightContext.globalAlpha = 0.6;
          this.drawRect(
            highlightContext,
            selectedX,
            selectedY,
            this.x.bandwidth(),
            this.contentHeight - selectedY,
            '#fff'
          );
          highlightContext.globalAlpha = 1;

          // Draw highlight circle
          highlightContext.fillStyle = '#069550';
          highlightContext.beginPath();
          highlightContext.arc(cx2, cy2, 5, 0, 2 * Math.PI);
          highlightContext.fill();
          highlightContext.strokeStyle = '#BDBDBD';
          highlightContext.strokeWidth = 1;
          highlightContext.stroke();

          html = '<b>'
            + this.chartService.reformatDate(selectedData[0].tanggal_awal)
            + ' s/d '
            + this.chartService.reformatDate(selectedData[0].tanggal_akhir)
            + '</b><br/>Jumlah Sampel PCR: '
            + this.chartService.numberFormat(selectedData[0].jumlah_sampel)
            + '<br/>Tren Positif: '
            + this.chartService.numberFormat(selectedData[0].positif);
        }

        if (selectedData.length > 0) {
          this.tooltip.transition()
            .duration(100)
            .style('opacity', .9);
          this.tooltip.html(html)
            .style('left', (event.pageX + 10) + 'px')
            .style('top', (event.pageY - 28) + 'px');
        } else {
          this.tooltip.transition()
            .duration(500)
            .style('opacity', 0);
        }
      })
      .on('mouseout', (d: any) => {
        highlightContext.clearRect(0, 0, this.contentWidth, this.contentHeight);

        this.tooltip.transition()
          .duration(500)
          .style('opacity', 0);
      });
  }

  drawGrid(): void {
    this.g.append('g')
      .attr('class', 'grid grid--horizontal')
      .call(this.yAxisGrid);
  }

  drawAxis(): void {
    // bottom axis
    this.g.append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', 'translate(0,' + this.contentHeight + ')')
      .call(this.xAxis)
      .selectAll('text')
        .style('text-anchor', 'start')
        .attr('dx', '.9em')
        .attr('dy', '.15em')
        .attr('transform', 'rotate(65)');

    // left axis
    this.g.append('g')
      .attr('class', 'axis axis--y')
      .call(this.yAxis)
      .append('text')
        .attr('transform', 'rotate(-90)')
        .attr('y', 6)
        .attr('dy', '0.71em')
        .attr('text-anchor', 'end')
        .text('Tren Positivity');
  }

  drawDates(): void {
    const element = this.chartContainer.nativeElement;
    const dateAxis = d3.axisBottom(this.x)
      .tickSize(-this.contentHeight)
      .tickFormat((d: any) =>  '')
      .tickValues(this.x.domain().filter((d: any, i: any) =>
        this.importantDates.filter((d2: any) => d === d2.date).length > 0));

    this.g.append('g')
      .attr('class', 'axis axis--date')
      .attr('transform', `translate(0, ${this.contentHeight})`)
      .call(dateAxis)
      .call((d: any) => d.select('.domain').remove());

    d3.select(element).selectAll('.axis--date .tick')
      .each((d: any, k: any, n: any) => {
        const tick = d3.select(n[k]);
        const line = tick.select('line');
        const selectedDate = this.importantDates.filter((d2: any) => d === d2.date);

        tick.classed('tren-positivity-' + selectedDate[0].date, true);
        tick.classed('d-none', !selectedDate[0]?.show);
        line.attr('stroke', selectedDate[0]?.color)
          .attr('stroke-width', 2);

        if (selectedDate[0]?.fillType === 'dashed') {
          line.attr('stroke-dasharray', '5, 5');
        }
      });
  }

  drawBarCanvas(percent: number = 0, animate: boolean = true): void {
    // refresh frame
    if (percent++ < 100) {
      this.animation = window.requestAnimationFrame(() => this.drawBarCanvas(percent, animate));
    }

    const data = this.dataset;

    this.context.clearRect(0, 0, this.contentWidth, this.contentHeight);

    data.forEach((d: any) => {
      const height = (this.contentHeight - this.y(d.jumlah_sampel)) * percent / 100;

      this.drawRect(
        this.context,
        this.x(d.tanggal),
        this.y(d.jumlah_sampel) + ((this.contentHeight - this.y(d.jumlah_sampel)) - height),
        this.x.bandwidth(),
        height,
        '#292C2A'
      );
    });

    if (percent === 100) {
      this.drawLine(animate);
    }
  }

  drawRect(context: any, x: number, y: number, width: number, height: number, color: string): void {
    context.fillStyle = color;
    context.fillRect(x, y, width, height);
  }

  drawLine(animate: boolean = true): void {
    const data = this.dataset;

    this.path = this.g.append('path')
      .data([data])
      .attr('class', 'line')
      .attr('d', this.valueline as any);

    setTimeout(() => {
      this.g.append('g')
        .selectAll('.dot')
        .data(data)
        .enter().append('circle')
        .attr('class', 'dot')
        .attr('cx', (d: any) => (this.x(d.tanggal) + (this.x.bandwidth() / 2)) || 0)
        .attr('cy', (d: any) => this.y(d.positif) || 0)
        .attr('r', 5);
    }, 2000);

    this.pathLength = this.path.node().getTotalLength();

    if (animate) {
      this.animatePath();
    }
  }

  // repeat function to animate path
  animatePath(): void {
    if (this.path) {
      this.path.attr('stroke-dasharray', this.pathLength + ' ' + this.pathLength)
        .attr('stroke-dashoffset', this.pathLength)
        .transition()
        .ease(d3.easeLinear)
        .attr('stroke-dashoffset', 0)
        .duration(2000)
        .on('end', () => setTimeout(this.animatePath, 500));
    }
  }

  drawLabel(): void{
    const data = this.dataset;

    this.g.selectAll('.text')
      .data(data)
      .enter()
      .append('text')
      .attr('class', 'chart-label tren-positivity-bar')
      .attr('x', (d: any) => (this.x(d.tanggal) + (this.x.bandwidth() / 2)) || 0)
      .attr('y', (d: any) => (this.y(d.jumlah_sampel) - 5) || 0)
      .attr('text-anchor', 'middle')
      .text((d: any) => this.chartService.numberFormat(d.jumlah_sampel));
  }

  drawLegend(): void {
    const legendSize = 20;
    const legendContainer = d3.select('.legend-tren-positivity');
    legendContainer.selectAll('.legend-item').remove();

    d3.select('#btn-tren-positivity-more').text('Tampilkan lebih banyak');

    const legendItem = legendContainer.selectAll('.legend-item')
      .data(this.legends)
      .enter().append('div')
      .attr('class', 'legend-item me-3 d-inline-flex align-items-center mb-3')
      .attr('id', (d: any, k: any) => 'legend-' + d.selector.replace('.', ''))
      .on('click', (e: any, d: any) => {
        let legendElement: any;

        if (e.target.nodeName === 'svg' || e.target.nodeName.toLowerCase() === 'span') {
          legendElement = e.target.parentElement;
        } else if (e.target.nodeName === 'line' || e.target.nodeName === 'circle' || e.target.nodeName === 'rect') {
          legendElement = e.target.parentElement.parentElement;
        } else {
          legendElement = e.target;
        }

        const itemSelector = d3.selectAll(d.selector);
        itemSelector.classed('d-none', d3.select(legendElement).classed('active'));
        d3.select(legendElement).classed('active', !d3.select(legendElement).classed('active'));

        this.legends = this.legends.map((content: any) => {
          content.show = d3.select('#legend-' + content.selector.replace('.', '')).classed('active');
          return {
            ...content
          };
        });
      });

    legendItem.each((d: any, i: any, n: any) => {
      const item = d3.select(n[i]);

      item.classed('active', d.show);
      item.classed('d-none', i > 2);
      item.classed('legend-more', i > 2);

      const svg = item.append('svg')
        .attr('width', legendSize)
        .attr('height', legendSize)
        .attr('class', 'me-2');

      if (d.type === 'circle') {
        svg.append('circle')
          .attr('cx', legendSize / 2)
          .attr('cy', legendSize / 2)
          .attr('r', legendSize / 3)
          .attr('fill', d.color);
      } else if (d.type === 'line') {
        const line = svg.append('line')
          .attr('x1', 0)
          .attr('y1', legendSize / 2)
          .attr('x2', legendSize)
          .attr('y2', legendSize / 2)
          .attr('stroke', d.color)
          .attr('stroke-width', 2);

        if (d.fillType === 'dashed') {
          line.attr('stroke-dasharray', '5, 5');
        }
      } else {
        svg.append('rect')
          .attr('x', legendSize / 4)
          .attr('y', legendSize / 4)
          .attr('width', legendSize / 2)
          .attr('height', legendSize / 2)
          .attr('fill', d.color);
      }

      item.append('span')
        .attr('style', 'font-size: 12px')
        .text(d.text);
    });

    this.legends.forEach((d: any) => {
      d3.select(d.selector).classed('d-none', !d.show);
    });
  }

  toggleLegend(e: any): void {
    d3.selectAll('.legend-tren-positivity .legend-more')
      .each((d: any, i: any, n: any) => {
        const item = d3.select(n[i]);

        item.classed('d-none', !item.classed('d-none'));
      });

    if (d3.select('.legend-tren-positivity .legend-more').classed('d-none')) {
      e.target.innerText = 'Tampilkan lebih banyak';
    } else {
      e.target.innerText = 'Tutup legend';
    }
  }
}
