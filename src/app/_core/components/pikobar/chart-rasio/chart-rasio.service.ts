import { Injectable } from '@angular/core';

// PACKAGE

@Injectable()
export class ChartRasioService {
  constructor() {}

  numberFormat(d: any): string {
    return d.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }
}
