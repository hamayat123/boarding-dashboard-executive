import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  AfterViewInit,
  SimpleChanges,
  ChangeDetectorRef,
  ViewChild,
  ElementRef,
  ViewEncapsulation, Input,
} from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

// SERVICE
import { GlobalService, PikobarService } from '@core/services';
import { ChartRasioService } from './chart-rasio.service';

// PACKAGE
import * as _ from 'lodash';
import * as moment from 'moment';
import * as d3 from 'd3';
import { TranslateService } from '@ngx-translate/core';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';

@Component({
  selector: 'app-comp-chart-rasio',
  templateUrl: './chart-rasio.component.html',
  styleUrls: ['./chart-rasio.component.scss'],
  providers: [ChartRasioService],
  encapsulation: ViewEncapsulation.None
})

export class ChartRasioComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  @ViewChild('chartRasio')
  private chartContainer!: ElementRef;

  @Input() dataRasio!: any;
  @Input() dataFilters!: any;

  dataset!: any;

  moment: any = moment;

  margin = {top: 20, right: 0, bottom: 30, left: 0};
  contentWidth = 0;
  contentHeight = 0;

  loading = true;
  lastWeek: number[] = [];

  animation: any;
  svg: any;
  canvas: any;
  context: any;
  g: any;
  x: any;
  y: any;
  color: any;
  xAxis: any;
  yAxis: any;
  dataPoint: any;
  tooltip: any;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private globalService: GlobalService,
    private pikobarService: PikobarService,
    private chartService: ChartRasioService,
    private translateService: TranslateService,
    private localize: LocalizeRouterService
  ) {
    setTimeout(() => {
      if (this.dataFilters) {
        if (!_.isObject(this.dataFilters)) {
          this.dataFilters = JSON.parse(this.dataFilters);
        }
      }
      this.initData();
    }, 500);
  }

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    if (this.dataFilters) {
      if (!_.isObject(this.dataFilters)) {
        this.dataFilters = JSON.parse(this.dataFilters);
      }
    }
    this.updateData();
  }

  ngOnDestroy(): void {
    window.cancelAnimationFrame(this.animation);
  }

  ngAfterViewInit(): void {}

  // Main Function
  onResize(event: any): void {
    this.updateData(100);
  }

  initData(): void {
    this.updateData();
  }

  updateData(percent: number = 0): void {
    if (!this.dataRasio) {
      setTimeout(() => {
        this.updateData();
      }, 500);
      return;
    }

    this.dataset = this.dataRasio.map((content: any) => {
      return {
        kode: content[this.dataFilters.level.kode],
        nama: content[this.dataFilters.level.nama],
        terkonfirmasi: content[this.dataFilters.sebaranType.index][this.dataFilters.sebaranType.subindex]
      };
    });

    const element = this.chartContainer.nativeElement;
    d3.select(element).selectAll('svg').remove();
    d3.select(element).selectAll('canvas').remove();

    this.createChart(percent);

    this.loading = false;
  }

  createChart(percent: number = 0): void {
    if (this.animation) {
      window.cancelAnimationFrame(this.animation);
    }

    this.initDataChart();

    const element = this.chartContainer.nativeElement;

    this.contentWidth = element.offsetWidth - this.margin.left - this.margin.right;
    this.contentHeight = element.offsetHeight - this.margin.top - this.margin.bottom;

    // Draw axis
    this.initSVG('axis-container');
    this.drawAxis();
    // this.drawBars();
    // End draw

    // Draw bar canvas
    this.initCanvas('bar-container');
    this.drawBarCanvas(percent);
    // End draw
  }

  // Graphic Function
  initDataChart(): void {
    const data = this.dataset.sort((a: any, b: any) => d3.descending(a.terkonfirmasi, b.terkonfirmasi));

    this.color = d3
      .scaleLinear()
      .domain([
        d3.min(data, (d: any) => _.toNumber(d.terkonfirmasi)) as number,
        d3.max(data, (d: any) => _.toNumber(d.terkonfirmasi)) as number
      ])
      .range(['#f5f5f5', this.dataFilters.sebaranType.color] as any);

    this.x = d3
      .scaleLinear()
      .range([70, this.contentWidth])
      .domain([
        d3.min(data, (d: any) => _.toNumber(d.terkonfirmasi)) as number,
        d3.max(data, (d: any) => _.toNumber(d.terkonfirmasi)) as number
      ]);

    this.y = d3
      .scaleBand()
      .rangeRound([0, this.contentHeight])
      .domain(data.map((d: any) => d.nama))
      .padding(.5);

    this.dataPoint = [];
    this.dataset.forEach((d: any) => {
      this.dataPoint.push({
        x: this.x(d.terkonfirmasi),
        y: this.y(d.nama),
        ...d
      });
    });

    this.xAxis = d3.axisBottom(this.x)
      .tickSize(-this.contentHeight)
      .ticks(6);

    this.yAxis = d3.axisLeft(this.y)
      .tickFormat((d: any) => d.replace('Kabupaten', 'Kab.'))
      .tickSize(0);

    d3.selectAll('.rasio-tooltip').remove();
    this.tooltip = d3.select('body').append('div')
      .attr('class', 'chart-tooltip rasio-tooltip')
      .style('opacity', 0);
  }

  initSVG(className: string = ''): void {
    const element = this.chartContainer.nativeElement;

    this.svg = d3.select(element).append('svg')
      .attr('width', element.offsetWidth)
      .attr('height', element.offsetHeight + 50);

    if (className) {
      this.svg = this.svg.attr('class', className);
    }

    this.g = this.svg.append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
  }

  initCanvas(className: string): void {
    const element = this.chartContainer.nativeElement;

    this.canvas = d3.select(element).append('canvas')
      .attr('width', this.contentWidth)
      .attr('height', this.contentHeight)
      .style('position', 'absolute');

    this.context = this.canvas.node().getContext('2d');

    let highlightCanvas = d3.select(element).append('canvas')
      .attr('width', this.contentWidth)
      .attr('height', this.contentHeight)
      .style('position', 'absolute') as any;

    const highlightContext = highlightCanvas.node().getContext('2d');

    if (className) {
      this.canvas = this.canvas.attr('class', className);
      highlightCanvas = highlightCanvas.attr('class', 'highlight-' + className);
    }

    d3.select('.highlight-' + className)
      .on('mouseover', (event: any, d: any) => {
        this.tooltip.transition()
          .duration(100)
          .style('opacity', .9);
      })
      .on('mousemove', (event: any, d: any) => {
        const rect = highlightCanvas.node().getBoundingClientRect();
        const canvasX = event.clientX - rect.left;
        const canvasY = event.clientY - rect.top;
        const selectedData = this.dataPoint.filter((content: any) => ( // cek kalo cursor dalam object
          canvasX >= this.x(Math.min(0, content.terkonfirmasi))
          && canvasY >= content.y
          && canvasX <= this.x(Math.min(0, content.terkonfirmasi)) + Math.abs(content.x - this.x(0))
          && canvasY <= content.y + this.y.bandwidth()));

        if (selectedData.length > 0) {
          const selectedX = selectedData[0].x;
          const selectedY = selectedData[0].y;

          highlightContext.clearRect(0, 0, this.contentWidth, this.contentHeight);
          this.drawRect(
            highlightContext,
            this.x(Math.min(0, selectedData[0].terkonfirmasi)),
            selectedY,
            Math.abs(selectedX - this.x(0)),
            this.y.bandwidth(),
            '#4F5050'
            );

          this.tooltip.transition()
            .duration(100)
            .style('opacity', .9);
          this.tooltip.html('<b>' + selectedData[0].nama
            + `</b><br/>${this.dataFilters.sebaranType.text}: `
            + this.chartService.numberFormat(selectedData[0].terkonfirmasi))
              .style('left', (event.pageX + 10) + 'px')
              .style('top', (event.pageY - 28) + 'px');
        } else {
          highlightContext.clearRect(0, 0, this.contentWidth, this.contentHeight);

          this.tooltip.transition()
            .duration(500)
            .style('opacity', 0);
        }
      })
      .on('mouseout', (d: any) => {
        highlightContext.clearRect(0, 0, this.contentWidth, this.contentHeight);

        this.tooltip.transition()
          .duration(500)
          .style('opacity', 0);
      });
  }

  drawAxis(): void {
    const axis = this.svg.append('g')
      .attr('class', 'axis-container');

    axis.append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', 'translate(0,' + this.contentHeight + ')')
      .call(this.xAxis)
      .selectAll('text')
        .style('text-anchor', 'start')
        .attr('dx', '.9em')
        .attr('dy', '.15em')
        .attr('transform', 'rotate(65)');

    axis.append('g')
      .attr('class', 'axis axis--y')
      .attr('transform', 'translate(55, 0)')
      .call(this.yAxis);
  }

  drawBarCanvas(percent: number = 0): void {
    // refresh frame
    if (percent++ < 100) {
      this.animation = window.requestAnimationFrame(() => this.drawBarCanvas(percent));
    }

    const data = this.dataset;

    this.context.clearRect(0, 0, this.contentWidth, this.contentHeight);

    data.forEach((d: any) => {
      const width = Math.abs(this.x(d.terkonfirmasi) - this.x(0)) * percent / 100;

      this.drawRect(
        this.context,
        this.x(Math.min(0, d.terkonfirmasi)),
        this.y(d.nama),
        width,
        this.y.bandwidth(),
        this.color(d.terkonfirmasi)
      );
    });
  }

  drawRect(context: any, x: number, y: number, width: number, height: number, color: string): void {
    context.fillStyle = color;
    context.fillRect(x, y, width, height);
  }

  drawBars(): void {
    const data = this.dataset;

    const bars = this.svg.append('g')
      .attr('class', 'bars')
      .selectAll('.bar')
      .data(data)
      .enter().append('rect');

    bars.on('mouseover', (event: any, d: any) => {
        this.tooltip.transition()
          .duration(100)
          .style('opacity', .9);
        this.tooltip.html('<b>' + d.nama + '</b><br/>Terkonfirmasi: ' + this.chartService.numberFormat(d.terkonfirmasi))
          .style('left', (event.pageX + 16) + 'px')
          .style('top', (event.pageY - 28) + 'px');
        })
      .on('mouseout', (d: any) => {
        this.tooltip.transition()
          .duration(500)
          .style('opacity', 0);
        })
      .transition()
      .duration(1000)
      .delay(100);

    bars.attr('class', (d: any) => 'bar bar--' + d.nama.replace(' ', '_'))
      .attr('fill', (d: any) => this.color(d.terkonfirmasi))
      .attr('x', (d: any) => this.x(Math.min(0, d.terkonfirmasi)))
      .attr('y', (d: any) => this.y(d.nama))
      .attr('width', 0)
      .attr('height', this.y.bandwidth());

    bars.transition()
      .duration(1000)
      .attr('width', (d: any) => Math.abs(this.x(d.terkonfirmasi) - this.x(0)));
  }

  drawAxisMiddle(): void {
    this.svg.append('g')
      .attr('class', 'axis axis--y axis-line')
      .attr('transform', 'translate(' + (this.x(0)) + ', 0)')
      .call(this.yAxis);
  }

  drawLegends(): void {
    const data = this.dataset;

    const legends = this.svg
      .append('g')
      .attr('class', 'legends');

    const legend = legends.selectAll('.legend')
      .data(data.slice(0, 2))
      .enter()
        .append('g')
        .attr('class', 'legend')
        .attr('transform', (d: any, i: any) => `translate(${((i * this.contentWidth / 4) + this.contentWidth / 4 + 15)}, ${(this.contentHeight + 30)})`);

    legend.append('rect')
      .attr('class', (d: any) => 'legend--' + d.nama.replace(' ', '_'))
      .attr('stroke', 'none')
      .attr('width', 15)
      .attr('height', 12);

    legend.append('text')
      .style('font-size', '12')
      .attr('transform', 'translate(19, 10)')
      .text((d: any) => _.capitalize(d.nama));
  }
}
