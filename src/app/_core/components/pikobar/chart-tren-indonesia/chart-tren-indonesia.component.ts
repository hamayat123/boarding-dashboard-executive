import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  AfterViewInit,
  SimpleChanges,
  ChangeDetectorRef,
  ViewChild,
  ElementRef,
  ViewEncapsulation, Input,
} from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

// SERVICE
import { GlobalService, PikobarService } from '@core/services';
import { ChartTrenIndonesiaService } from './chart-tren-indonesia.service';

// PACKAGE
import * as _ from 'lodash';
import * as moment from 'moment';
import * as d3 from 'd3';
import { TranslateService } from '@ngx-translate/core';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';

@Component({
  selector: 'app-comp-chart-tren-indonesia',
  templateUrl: './chart-tren-indonesia.component.html',
  styleUrls: ['./chart-tren-indonesia.component.scss'],
  providers: [ChartTrenIndonesiaService],
  encapsulation: ViewEncapsulation.None
})

export class ChartTrenIndonesiaComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  @ViewChild('chartTrenIndonesia')
  private chartContainer!: ElementRef;

  @Input() dataTren!: any;
  @Input() dataFilters!: any;

  dataset!: any;

  moment: any = moment;

  margin = {top: 20, right: 20, bottom: 100, left: 40};
  contentWidth = 0;
  contentHeight = 0;

  loading = true;
  lastWeek: number[] = [];

  svg: any;
  canvas: any;
  context: any;
  g: any;
  x: any;
  y: any;
  xAxis: any;
  yAxis: any;
  yAxisGrid: any;
  factory: any;
  valueline: any;
  boundline: any;
  path: any;
  pathLength: any;
  zoom: any;
  dataPoint: any;
  legends: any;
  importantDates: any;
  tooltip: any;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private globalService: GlobalService,
    private pikobarService: PikobarService,
    private chartService: ChartTrenIndonesiaService,
    private translateService: TranslateService,
    private localize: LocalizeRouterService
  ) {
    setTimeout(() => {
      this.initData();
    }, 500);
  }

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.dataTren && (changes.dataTren.previousValue === undefined)) {
      return;
    }

    this.updateData();
  }

  ngOnDestroy(): void {}

  ngAfterViewInit(): void {}

  // Main Function
  onResize(event: any): void {
    this.updateData(false);
  }

  initData(): void {
    this.updateData();
  }

  updateData(animate: boolean = true): void {
    if (!this.dataTren) {
      setTimeout(() => {
        this.updateData();
      }, 500);
      return;
    }

    this.dataset = this.dataTren.map((content: any, key: any) => {
      return {
        date: content.date,
        estimated_rt: content.estimated_rt,
        lower_bound: content.lower_bound,
        upper_bound: content.upper_bound,
      };
    }).filter((content: any) => this.chartService.filterDate(this.dataFilters, content.date));

    const element = this.chartContainer.nativeElement;
    d3.select(element).selectAll('svg').remove();
    d3.select(element).selectAll('canvas').remove();

    this.createChart(animate);

    this.loading = false;
  }

  createChart(animate: boolean = true): void {
    const element = this.chartContainer.nativeElement;

    this.contentWidth = element.offsetWidth - this.margin.left - this.margin.right;
    this.contentHeight = element.offsetHeight - this.margin.top - this.margin.bottom;

    this.initChartData();
    // Draw axis line & grid
    this.initSVG('axis-grid-container');
    this.drawGrid();
    this.drawAxis();
    this.drawDates();
    // End draw

    // Draw scatter plot & line
    // this.initCanvas('scatter-container');
    this.initSVG('tren-indonesia-line');
    // this.drawScatterCanvas();
    this.drawBound();
    this.drawLine(animate);
    // End draw

    this.initHighlightCanvas();
    this.drawLegend();
  }

  // Graphic Function
  initChartData(): void {
    const data = this.dataset;

    this.zoom = d3.zoomIdentity;

    this.x = d3
      .scaleBand()
      .range([20, this.contentWidth - 20])
      .padding(0.5)
      .domain(data.map((d: any) => d.date));

    this.y = d3
      .scaleLinear()
      .range([this.contentHeight, 0])
      .domain([0, 10]);

    this.dataPoint = [];
    this.dataset.forEach((d: any, key: any) => {
      this.dataPoint.push({
        x: this.x(d.date),
        y: this.y(d.estimated_rt),
        yUpper: this.y(d.upper_bound),
        yLower: this.y(d.lower_bound),
        ...d
      });
    });

    this.xAxis = d3
      .axisBottom(this.x)
      .tickFormat((d: any) => this.chartService.reformatDate(d))
      .tickValues(this.x.domain().filter((d: any, i: any) => !(i % (Math.floor(this.x.domain().length / 16)))));

    this.yAxis = d3.axisLeft(this.y)
      .tickSize(-this.contentWidth)
      .ticks(5);

    this.yAxisGrid = d3.axisLeft(this.y)
      .tickSize(-this.contentWidth)
      .ticks(10)
      .tickFormat((d: any) =>  '');

    this.valueline = d3.line()
      .x((d: any) => (this.x(d.date) + (this.x.bandwidth() / 2)) || 0)
      .y((d: any) => this.y(d.estimated_rt) || 0);

    this.boundline = d3.area()
      .x((d: any) => (this.x(d.date) + (this.x.bandwidth() / 2)) || 0)
      .y0((d: any) => this.y(d.lower_bound) || 0)
      .y1((d: any) => this.y(d.upper_bound) || 0);

    if (this.importantDates === undefined) {
      this.importantDates = this.chartService.getImportantDates();
    }

    if (this.legends === undefined) {
      this.legends = [
        {
          text: 'Rata - rata 7 hari',
          color: '#FFA600',
          type: 'line',
          fillType: 'solid',
          show: true,
          selector: '.tren-indonesia-line'
        },
        ...this.importantDates.map((d: any) => {
          return {
            ...d,
            selector: '.tren-indonesia-' + d.date
          };
        }),
      ];
    }

    d3.selectAll('.tren-indonesia-tooltip').remove();
    this.tooltip = d3.select('body').append('div')
      .attr('class', 'chart-tooltip tren-indonesia-tooltip')
      .style('opacity', 0);
  }

  initSVG(className: string = ''): void {
    const element = this.chartContainer.nativeElement;

    this.svg = d3.select(element).append('svg')
      .attr('width', element.offsetWidth)
      .attr('height', element.offsetHeight + 50);

    if (className) {
      this.svg = this.svg.attr('class', className);
    }

    this.g = this.svg.append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
  }

  initCanvas(className: string): void {
    const element = this.chartContainer.nativeElement;

    this.canvas = d3.select(element).append('canvas')
      .attr('width', this.contentWidth)
      .attr('height', this.contentHeight)
      .style('position', 'absolute')
      .style('left', this.margin.left + 'px')
      .style('top', this.margin.top + 'px');

    if (className) {
      this.canvas = this.canvas.attr('class', className);
    }

    this.context = this.canvas.node().getContext('2d');
  }

  initHighlightCanvas(): void {
    const element = this.chartContainer.nativeElement;

    const highlightCanvas = d3.select(element).append('canvas')
      .attr('width', this.contentWidth)
      .attr('height', this.contentHeight)
      .style('position', 'absolute')
      .attr('class', 'highlight-tren-indonesia')
      .style('left', this.margin.left + 'px')
      .style('top', this.margin.top + 'px') as any;

    const highlightContext = highlightCanvas.node().getContext('2d');

    d3.select('.highlight-tren-indonesia')
      .on('mouseover', (event: any, d: any) => {
        this.tooltip.transition()
          .duration(100)
          .style('opacity', .9);
      })
      .on('mousemove', (event: any, d: any) => {
        let html = '';
        const rect = highlightCanvas.node().getBoundingClientRect();
        const canvasX = event.clientX - rect.left;
        const canvasY = event.clientY - rect.top;

        let radius = 10;
        if (this.x.bandwidth() < 5) {
          radius = this.x.bandwidth();
        }

        // cek kalo cursor dalam area
        const selectedData = this.dataPoint.filter((content: any) => (
          canvasX >= ((content.x + (this.x.bandwidth() / 2)) || 0 ) - (this.x.bandwidth() / 2)
          && canvasX <= ((content.x + (this.x.bandwidth() / 2)) || 0 ) + (this.x.bandwidth() / 2)
        ));

        if (selectedData.length > 0) {
          const cx = selectedData[0].x + (this.x.bandwidth() / 2);
          const cy = selectedData[0].y;

          highlightContext.clearRect(0, 0, this.contentWidth, this.contentHeight);
          this.drawCircle(highlightContext, cx, cy, '#FFA600', 5, true);

          html = '<b>'
            + this.chartService.reformatDate(selectedData[0].date)
            + '</b><br/>Ambang bawah: ' + selectedData[0].lower_bound.toFixed(2)
            + '<br/>Nilai: ' + selectedData[0].estimated_rt.toFixed(2)
            + '<br/>Ambang Atas: ' + selectedData[0].upper_bound.toFixed(2);
        }

        if (selectedData.length > 0) {
          this.tooltip.transition()
            .duration(100)
            .style('opacity', .9);
          this.tooltip.html(html)
            .style('left', (event.pageX + 10) + 'px')
            .style('top', (event.pageY - 28) + 'px');
        } else {
          this.tooltip.transition()
            .duration(500)
            .style('opacity', 0);
        }
      })
      .on('mouseout', (d: any) => {
        highlightContext.clearRect(0, 0, this.contentWidth, this.contentHeight);

        this.tooltip.transition()
          .duration(500)
          .style('opacity', 0);
      });
  }

  drawGrid(): void {
    this.g.append('g')
      .attr('class', 'grid grid--horizontal')
      .call(this.yAxisGrid);
  }

  drawAxis(): void {
    // bottom axis
    this.g.append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', 'translate(0,' + this.contentHeight + ')')
      .call(this.xAxis)
      .selectAll('text')
      .style('text-anchor', 'start')
      .attr('dx', '.9em')
      .attr('dy', '.15em')
      .attr('transform', 'rotate(65)');

    // left axis
    this.g.append('g')
      .attr('class', 'axis axis--y')
      .call(this.yAxis)
      .append('text')
      .attr('transform', 'rotate(-90)')
      .attr('y', 6)
      .attr('dy', '0.71em')
      .attr('text-anchor', 'end')
      .text('tren');
  }

  drawDates(): void {
    const element = this.chartContainer.nativeElement;
    const dateAxis = d3.axisBottom(this.x)
      .tickSize(-this.contentHeight)
      .tickFormat((d: any) =>  '')
      .tickValues(this.x.domain().filter((d: any, i: any) =>
        this.importantDates.filter((d2: any) => d === d2.date).length > 0));

    this.g.append('g')
      .attr('class', 'axis axis--date')
      .attr('transform', `translate(0, ${this.contentHeight})`)
      .call(dateAxis)
      .call((d: any) => d.select('.domain').remove());

    d3.select(element).selectAll('.axis--date .tick')
      .each((d: any, k: any, n: any) => {
        const tick = d3.select(n[k]);
        const line = tick.select('line');
        const selectedDate = this.importantDates.filter((d2: any) => d === d2.date);

        tick.classed('tren-indonesia-' + selectedDate[0].date, true);
        tick.classed('d-none', !selectedDate[0]?.show);
        line.attr('stroke', selectedDate[0]?.color)
          .attr('stroke-width', 2);

        if (selectedDate[0]?.fillType === 'dashed') {
          line.attr('stroke-dasharray', '5, 5');
        }
      });
  }

  drawCircle(context: any, cx: number, cy: number, color: string = '#069550', r: number = 1.5, stroke: boolean = false): void {
    context.fillStyle = color;
    context.beginPath();
    context.arc(cx, cy, r, 0, 2 * Math.PI);
    // context.closePath();
    context.fill();

    if (stroke && (r > 1.5)) {
      context.strokeStyle = '#BDBDBD';
      context.strokeWidth = 1;
      context.stroke();
    }
  }

  drawBound(): void {
    const data = this.dataset;

    this.path = this.g.append('path')
      .data([data])
      .attr('class', 'bound')
      .attr('d', this.boundline as any);

    this.pathLength = this.path.node().getTotalLength();
  }

  drawLine(animate: boolean = true): void {
    const data = this.dataset;

    this.path = this.g.append('path')
      .data([data])
      .attr('class', 'line')
      .attr('d', this.valueline as any);

    this.pathLength = this.path.node().getTotalLength();

    if (animate) {
      this.animatePath();
    }
  }

  // repeat function to animate path
  animatePath(): void {
    if (this.path) {
      this.path.attr('stroke-dasharray', this.pathLength + ' ' + this.pathLength)
        .attr('stroke-dashoffset', this.pathLength)
        .transition()
        .ease(d3.easeLinear)
        .attr('stroke-dashoffset', 0)
        .duration(2000)
        .on('end', () => setTimeout(this.animatePath, 500));
    }
  }

  drawLegend(): void {
    const element = this.chartContainer.nativeElement;
    const legendSize = 20;
    const legendContainer = d3.select('.legend-tren-indonesia');
    legendContainer.selectAll('.legend-item').remove();

    d3.select('#btn-tren-indonesia-more').text('Tampilkan lebih banyak');

    const legendItem = legendContainer.selectAll('.legend-item')
      .data(this.legends)
      .enter().append('div')
      .attr('class', 'legend-item me-3 d-inline-flex align-items-center mb-3')
      .attr('id', (d: any, k: any) => 'legend-' + d.selector.replace('.', ''))
      .on('click', (e: any, d: any) => {
        let legendElement: any;

        if (e.target.nodeName === 'svg' || e.target.nodeName.toLowerCase() === 'span') {
          legendElement = e.target.parentElement;
        } else if (e.target.nodeName === 'line' || e.target.nodeName === 'circle' || e.target.nodeName === 'rect') {
          legendElement = e.target.parentElement.parentElement;
        } else {
          legendElement = e.target;
        }

        const itemSelector = d3.select(d.selector);
        itemSelector.classed('d-none', d3.select(legendElement).classed('active'));
        d3.select(legendElement).classed('active', !d3.select(legendElement).classed('active'));

        this.legends = this.legends.map((content: any) => {
          content.show = d3.select('#legend-' + content.selector.replace('.', '')).classed('active');
          return {
            ...content
          };
        });
      });

    legendItem.each((d: any, i: any, n: any) => {
      const item = d3.select(n[i]);

      item.classed('active', d.show);
      item.classed('d-none', i > 2);
      item.classed('legend-more', i > 2);

      const svg = item.append('svg')
        .attr('width', legendSize)
        .attr('height', legendSize)
        .attr('class', 'me-2');

      if (d.type === 'circle') {
        svg.append('circle')
          .attr('cx', legendSize / 2)
          .attr('cy', legendSize / 2)
          .attr('r', legendSize / 3)
          .attr('fill', d.color);
      } else if (d.type === 'line') {
        const line = svg.append('line')
          .attr('x1', 0)
          .attr('y1', legendSize / 2)
          .attr('x2', legendSize)
          .attr('y2', legendSize / 2)
          .attr('stroke', d.color)
          .attr('stroke-width', 2);

        if (d.fillType === 'dashed') {
          line.attr('stroke-dasharray', '5, 5');
        }
      } else {
        svg.append('rect')
          .attr('x', 0)
          .attr('y', 0)
          .attr('width', legendSize)
          .attr('height', legendSize);
      }

      item.append('span')
        .attr('style', 'font-size: 12px')
        .text(d.text);
    });

    this.legends.forEach((d: any) => {
      d3.select(d.selector).classed('d-none', !d.show);
    });
  }

  toggleLegend(e: any): void {
    d3.selectAll('.legend-tren-indonesia .legend-more')
      .each((d: any, i: any, n: any) => {
        const item = d3.select(n[i]);

        item.classed('d-none', !item.classed('d-none'));
      });

    if (d3.select('.legend-tren-indonesia .legend-more').classed('d-none')) {
      e.target.innerText = 'Tampilkan lebih banyak';
    } else {
      e.target.innerText = 'Tutup legend';
    }
  }
}
