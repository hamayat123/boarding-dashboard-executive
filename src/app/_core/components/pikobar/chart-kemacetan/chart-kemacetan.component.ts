import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  AfterViewInit,
  SimpleChanges,
  ChangeDetectorRef,
  ViewChild,
  ElementRef,
  ViewEncapsulation, Input,
} from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

// SERVICE
import { GlobalService, PikobarService } from '@core/services';
import { ChartKemacetanService } from './chart-kemacetan.service';

// PACKAGE
import * as _ from 'lodash';
import * as moment from 'moment';
import * as d3 from 'd3';
import { TranslateService } from '@ngx-translate/core';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';

@Component({
  selector: 'app-comp-chart-kemacetan',
  templateUrl: './chart-kemacetan.component.html',
  styleUrls: ['./chart-kemacetan.component.scss'],
  providers: [ChartKemacetanService],
  encapsulation: ViewEncapsulation.None
})

export class ChartKemacetanComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  @ViewChild('chartKemacetan')
  private chartContainer!: ElementRef;

  @Input() dataKemacetan!: any;
  @Input() dataFilters!: any;

  dataset: any = [];
  labelDate: any = [];
  labelHour: any = [...Array(24).keys()];

  moment: any = moment;

  margin = {top: 20, right: 20, bottom: 30, left: 40};
  contentWidth = 0;
  contentHeight = 0;

  loading = true;
  lastWeek: number[] = [];

  animation: any;
  svg: any;
  canvas: any;
  context: any;
  g: any;
  x: any;
  y: any;
  xAxis: any;
  yAxis: any;
  yAxisGrid: any;
  dataPoint: any;
  color: any;
  colorLegend: any;
  legends: any;
  importantDates: any;
  tooltip: any;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private globalService: GlobalService,
    private pikobarService: PikobarService,
    private chartService: ChartKemacetanService,
    private translateService: TranslateService,
    private localize: LocalizeRouterService
  ) {
    setTimeout(() => {
      this.initData();
    }, 500);
  }

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.dataKemacetan && (changes.dataKemacetan.previousValue === undefined)) {
      return;
    }

    if (changes.dataFilters
      && (!_.isEqual(changes.dataFilters.previousValue.wilayahOption, changes.dataFilters.currentValue.wilayahOption))) {
      if (!changes.dataKemacetan) {
        return;
      }
    }

    this.updateData();
  }

  ngOnDestroy(): void {
    window.cancelAnimationFrame(this.animation);
  }

  ngAfterViewInit(): void {}

  // Main Function
  onResize(event: any): void {
    this.updateData(100);
  }

  initData(): void {
    this.updateData();
  }

  updateData(percent: number = 0): void {
    if (!this.dataKemacetan) {
      setTimeout(() => {
        this.updateData();
      }, 500);
      return;
    }

    this.dataset = [];
    this.labelDate = [];

    this.dataKemacetan.map((content: any, key: any) => {
      this.labelDate.push(content.date);

      return content.detail_hour.map((content2: any, key2: any) => {
        this.dataset.push({
          date: content.date,
          hour: content2.hour,
          value: content2.median,
        });
        return;
      });
    });

    this.dataset = this.dataset.filter((content: any) => this.chartService.filterDate(this.dataFilters, content.date));
    this.labelDate = this.labelDate.filter((content: any) => this.chartService.filterDate(this.dataFilters, content));

    const element = this.chartContainer.nativeElement;
    d3.select(element).selectAll('svg').remove();
    d3.select(element).selectAll('canvas').remove();

    this.createChart(percent);

    this.loading = false;
  }

  createChart(percent: number = 0): void {
    if (this.animation) {
      window.cancelAnimationFrame(this.animation);
    }
    const element = this.chartContainer.nativeElement;

    this.contentWidth = element.offsetWidth - this.margin.left - this.margin.right;
    this.contentHeight = element.offsetHeight - this.margin.top - this.margin.bottom;

    this.initDataChart();

    this.initSVG();
    this.drawAxis();
    this.initCanvas('kemacetan-bar');
    this.drawBarCanvas(percent);
    this.initSVG('kemacetan-dates');
    this.drawDates();

    this.initHighlightCanvas();
    this.drawColorLegend();
    this.drawLegend();
  }

  // Graphic Function
  initDataChart(): void {
    const data = this.dataset;
    const maxValue = d3.max(data, (d: any) => _.toNumber(d.value)) as number;
    const colorDomain = this.chartService.getColorDomain(maxValue);

    this.color = d3
      .scaleLinear()
      .domain(colorDomain)
      .range(['#f5f5f5', '#fed462', '#f7a83b', '#e4792f', '#ba424e', '#a7315b', '#862462', '#440b67', '#090410'] as any);

    const n = Math.min(this.color.domain().length, this.color.range().length);
    this.colorLegend = this.color.copy()
      .rangeRound(
        d3.quantize(
          d3.interpolate(
            (this.contentWidth / 4), (this.contentWidth / 2)
          ), n)
      );

    this.x = d3
      .scaleBand()
      .range([0, this.contentWidth])
      .padding(0.01)
      .domain(this.labelDate);

    this.y = d3
      .scaleBand()
      .range([this.contentHeight, 0])
      .padding(0.01)
      .domain(this.labelHour);

    this.dataPoint = [];
    this.dataset.forEach((d: any) => {
      this.dataPoint.push({
        x: this.x(d.date),
        y: this.y(d.hour),
        date: d.date,
        hour: d.hour,
        value: d.value
      });
    });

    this.xAxis = d3
      .axisBottom(this.x)
      .tickValues(this.x.domain().filter((d: any, i: any) => !(i % (Math.floor(this.x.domain().length / 16)))))
      .tickFormat((d: any) => this.chartService.reformatDate(d));

    this.yAxis = d3
      .axisLeft(this.y)
      .tickFormat((d: any) => this.chartService.reformatHour(d));

    if (this.importantDates === undefined) {
      this.importantDates = this.chartService.getImportantDates();
    }

    if (this.legends === undefined) {
      this.legends = [
        ...this.importantDates.map((d: any) => {
          return {
            ...d,
            selector: '.kemacetan-' + d.date
          };
        })
      ];
    }

    d3.selectAll('.kemacetan-tooltip').remove();
    this.tooltip = d3.select('body').append('div')
      .attr('class', 'chart-tooltip kemacetan-tooltip')
      .style('opacity', 0);
  }

  initSVG(className: string = ''): void {
    const element = this.chartContainer.nativeElement;

    this.svg = d3.select(element).append('svg')
      .attr('width', element.offsetWidth)
      .attr('height', element.offsetHeight);

    if (className) {
      this.svg = this.svg.attr('class', className);
    }

    this.g = this.svg.append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
  }

  initCanvas(className: string): void {
    const element = this.chartContainer.nativeElement;
    this.canvas = d3.select(element).append('canvas')
      .attr('width', this.contentWidth)
      .attr('height', this.contentHeight)
      .style('position', 'absolute')
      .style('left', this.margin.left + 'px')
      .style('top', this.margin.top + 'px');

    if (className) {
      this.canvas = this.canvas.attr('class', className);
    }

    this.context = this.canvas.node().getContext('2d');
  }

  initHighlightCanvas(): void {
    const element = this.chartContainer.nativeElement;

    const highlightCanvas = d3.select(element).append('canvas')
      .attr('width', this.contentWidth)
      .attr('height', this.contentHeight)
      .style('position', 'absolute')
      .attr('class', 'highlight-kemacetan')
      .style('left', this.margin.left + 'px')
      .style('top', this.margin.top + 'px') as any;

    const highlightContext = highlightCanvas.node().getContext('2d');

    d3.select('.highlight-kemacetan')
      .on('mouseover', (event: any, d: any) => {
        this.tooltip.transition()
          .duration(100)
          .style('opacity', .9);
      })
      .on('mousemove', (event: any, d: any) => {
        const rect = highlightCanvas.node().getBoundingClientRect();
        const canvasX = event.clientX - rect.left;
        const canvasY = event.clientY - rect.top;
        const selectedData = this.dataPoint.filter((content: any) => ( // cek kalo cursor dalam object
          canvasX >= content.x
          && canvasY >= content.y
          && canvasX <= (content.x + this.x.bandwidth())
          && canvasY <= (content.y + this.y.bandwidth())
        ));

        if (selectedData.length > 0) {
          const selectedX = selectedData[0].x;
          const selectedY = selectedData[0].y;

          highlightContext.clearRect(0, 0, this.contentWidth, this.contentHeight);
          this.drawRect(
            highlightContext,
            selectedX,
            selectedY,
            this.x.bandwidth(),
            this.y.bandwidth(),
            '#069550',
            true
          );
          this.selectLegend(selectedData[0].value);

          this.tooltip.transition()
            .duration(100)
            .style('opacity', .9);
          this.tooltip.html('<b>'
            + this.chartService.getDayName(selectedData[0].date)
            + '</b><br/>'
            + this.chartService.reformatDate(selectedData[0].date)
            + ' @ '
            + this.chartService.reformatHour(selectedData[0].hour))
              .style('left', (event.pageX + 10) + 'px')
              .style('top', (event.pageY - 28) + 'px');
        } else {
          highlightContext.clearRect(0, 0, this.contentWidth, this.contentHeight);

          this.tooltip.transition()
            .duration(500)
            .style('opacity', 0);
        }
      })
      .on('mouseout', (d: any) => {
        highlightContext.clearRect(0, 0, this.contentWidth, this.contentHeight);

        this.tooltip.transition()
          .duration(500)
          .style('opacity', 0);
      });
  }

  drawAxis(): void {
    // bottom axis
    this.g.append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', 'translate(0,' + this.contentHeight + ')')
      .call(this.xAxis)
      .selectAll('text')
      .style('text-anchor', 'start')
      .attr('dx', '.9em')
      .attr('dy', '.15em')
      .attr('transform', 'rotate(65)');

    // left axis
    this.g.append('g')
      .attr('class', 'axis axis--y')
      .call(this.yAxis)
      .append('text')
      .attr('transform', 'rotate(-90)')
      .attr('y', 6)
      .attr('dy', '0.71em')
      .attr('text-anchor', 'end');
  }

  drawDates(): void {
    const element = this.chartContainer.nativeElement;
    const dateAxis = d3.axisBottom(this.x)
      .tickSize(-this.contentHeight)
      .tickFormat((d: any) =>  '')
      .tickValues(this.x.domain().filter((d: any, i: any) =>
        this.importantDates.filter((d2: any) => d === d2.date).length > 0));

    this.g.append('g')
      .attr('class', 'axis axis--date')
      .attr('transform', `translate(0, ${this.contentHeight})`)
      .call(dateAxis)
      .call((d: any) => d.select('.domain').remove());

    d3.select(element).selectAll('.axis--date .tick')
      .each((d: any, k: any, n: any) => {
        const tick = d3.select(n[k]);
        const line = tick.select('line');
        const selectedDate = this.importantDates.filter((d2: any) => d === d2.date);

        tick.classed('kemacetan-' + selectedDate[0].date, true);
        tick.classed('d-none', !selectedDate[0]?.show);
        line.attr('stroke', selectedDate[0]?.color)
          .attr('stroke-width', 2);

        if (selectedDate[0]?.fillType === 'dashed') {
          line.attr('stroke-dasharray', '5, 5');
        }
      });
  }

  drawBarCanvas(percent: number = 0): void {
    // refresh frame
    if (percent++ < 100) {
      this.animation = window.requestAnimationFrame(() => this.drawBarCanvas(percent));
    }

    const data = this.dataset;

    data.forEach((d: any) => {
      const value = d.value * percent / 100;

      this.drawRect(
        this.context,
        this.x(d.date),
        this.y(d.hour),
        this.x.bandwidth(),
        this.y.bandwidth(),
        this.color(value)
      );
    });
  }

  drawRect(context: any, x: number, y: number, width: number, height: number, color: string, stroke: boolean = false): void {
    context.fillStyle = color;
    context.fillRect(x, y, width, height);

    if (stroke) {
      context.strokeStyle = '#fff';
      context.strokeRect(x, y, width, height);
    }
  }

  drawLegend(): void {
    const legendSize = 20;
    const legendContainer = d3.select('.legend-kemacetan');
    legendContainer.selectAll('.legend-item').remove();

    d3.select('#btn-kemacetan-more').text('Tampilkan lebih banyak');

    const legendItem = legendContainer.selectAll('.legend-item')
      .data(this.legends)
      .enter().append('div')
      .attr('class', 'legend-item me-3 d-inline-flex align-items-center mb-3')
      .attr('id', (d: any) => 'legend-' + d.selector.replace('.', ''))
      .on('click', (e: any, d: any) => {
        let legendElement: any;

        if (e.target.nodeName === 'svg' || e.target.nodeName.toLowerCase() === 'span') {
          legendElement = e.target.parentElement;
        } else if (e.target.nodeName === 'line' || e.target.nodeName === 'circle' || e.target.nodeName === 'rect') {
          legendElement = e.target.parentElement.parentElement;
        } else {
          legendElement = e.target;
        }

        const itemSelector = d3.select(d.selector);
        itemSelector.classed('d-none', d3.select(legendElement).classed('active'));
        d3.select(legendElement).classed('active', !d3.select(legendElement).classed('active'));

        this.legends = this.legends.map((content: any) => {
          content.show = d3.select('#legend-' + content.selector.replace('.', '')).classed('active');
          return {
            ...content
          };
        });
      });

    legendItem.each((d: any, i: any, n: any) => {
      const item = d3.select(n[i]);

      item.classed('active', d.show);
      item.classed('d-none', i > 2);
      item.classed('legend-more', i > 2);

      const svg = item.append('svg')
        .attr('width', legendSize)
        .attr('height', legendSize)
        .attr('class', 'me-2');

      if (d.type === 'circle') {
        svg.append('circle')
          .attr('cx', legendSize / 2)
          .attr('cy', legendSize / 2)
          .attr('r', legendSize / 3)
          .attr('fill', d.color);
      } else if (d.type === 'line') {
        const line = svg.append('line')
          .attr('x1', 0)
          .attr('y1', legendSize / 2)
          .attr('x2', legendSize)
          .attr('y2', legendSize / 2)
          .attr('stroke', d.color)
          .attr('stroke-width', 2);

        if (d.fillType === 'dashed') {
          line.attr('stroke-dasharray', '5, 5');
        }
      } else {
        svg.append('rect')
          .attr('x', 0)
          .attr('y', 0)
          .attr('width', legendSize)
          .attr('height', legendSize);
      }

      item.append('span')
        .attr('style', 'font-size: 12px')
        .text(d.text);
    });

    this.legends.forEach((d: any) => {
      d3.select(d.selector).classed('d-none', !d.show);
    });
  }

  drawColorLegend(): void {
    const legendContainer = d3.select('.legend-kemacetan-color');
    legendContainer.selectAll('canvas').remove();
    legendContainer.selectAll('svg').remove();

    const data = this.dataset;
    const maxValue = d3.max(data, (d: any) => _.toNumber(d.value)) as number;
    const dataColor = [...new Array(maxValue).keys()];

    const legendCanvas = d3.select('.legend-kemacetan-color')
      .append('canvas')
      .attr('width', this.contentWidth)
      .attr('height', 100)
      .style('position', 'absolute')
      .style('top', '50px')
      .style('left', `${this.contentWidth / 8}px`) as any;
    const legendContext = legendCanvas.node()?.getContext('2d');

    const legendSVG = d3.select('.legend-kemacetan-color')
      .append('svg')
      .attr('width', this.contentWidth + (this.contentWidth / 8))
      .append('g')
        .attr('transform', `translate(0, 30)`);

    legendSVG.append('g')
      .attr('transform', `translate(${this.contentWidth / 8}, 0)`)
      .append('text')
        .attr('class', 'legend-title')
        .attr('text-anchor', 'middle')
        .attr('dominant-baseline', 'central')
        .attr('x', this.colorLegend(Math.floor(maxValue / 2)))
        .attr('y', 0)
        .attr('font-size', 14)
        .text('Tingkat Kemacetan');

    legendSVG.append('g')
      .attr('class', 'axis axis--color')
      .attr('transform', `translate(${this.contentWidth / 8}, 20)`)
      .call(d3.axisBottom(this.colorLegend).tickSize(20).ticks(8))
      .call((d: any) => d.select('.domain').remove())
      .selectAll('text')
      .style('text-anchor', 'start')
      .attr('dx', '2.2em')
      .attr('dy', '-.5em')
      .attr('transform', 'rotate(65)');

    dataColor.forEach((d: any) => {
      legendContext.fillStyle = this.color(d);
      legendContext.fillRect(this.colorLegend(d), 0, 5, 20);
    });
  }

  selectLegend(value: any): void {
    if (!d3.select('.legend-kemacetan-color .legend-selector').node()) {
      const selectedLegend = d3.select('.legend-kemacetan-color svg');

      selectedLegend.append('g')
        .attr('transform', `translate(${this.contentWidth / 8}, 50)`)
        .append('path')
          .attr('class', 'legend-selector')
          .attr('d', d3.symbol().type(d3.symbolTriangle))
          .attr('fill', '#212121')
          .attr('stroke', '#BDBDBD')
          .attr('stroke-width', 1)
          .attr('transform', `translate(${this.colorLegend(value)}, 0) rotate(180)`);
    } else {
      d3.select('.legend-kemacetan-color .legend-selector')
        .attr('transform', `translate(${this.colorLegend(value)}, 0) rotate(180)`);
    }
  }

  toggleLegend(e: any): void {
    d3.selectAll('.legend-kemacetan .legend-more')
      .each((d: any, i: any, n: any) => {
        const item = d3.select(n[i]);

        item.classed('d-none', !item.classed('d-none'));
      });

    if (d3.select('.legend-kemacetan .legend-more').classed('d-none')) {
      e.target.innerText = 'Tampilkan lebih banyak';
    } else {
      e.target.innerText = 'Tutup legend';
    }
  }
}
