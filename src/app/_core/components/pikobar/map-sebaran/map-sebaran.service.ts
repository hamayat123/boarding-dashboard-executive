import { Injectable } from '@angular/core';

// PACKAGE

@Injectable()
export class MapSebaranService {
  constructor() {}

  numberFormat(d: any): string {
    return d.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }

  getColorDomain(max: any): any {
    const loop = new Array(9);
    const data = [0];
    const splitMax = Math.floor(max / 9);

    for (let i = 0; i < 9; i++) {
      if (i === 8) {
        data.push(max);
      } else {
        data.push(splitMax * i);
      }
    }

    return data;
  }
}
