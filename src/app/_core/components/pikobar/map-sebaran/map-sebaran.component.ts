import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  AfterViewInit,
  SimpleChanges,
  ChangeDetectorRef,
  ViewChild,
  ElementRef,
  ViewEncapsulation, Input, Output, EventEmitter,
} from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

// SERVICE
import { GlobalService, PikobarService } from '@core/services';
import { MapSebaranService } from './map-sebaran.service';

// PACKAGE
import * as _ from 'lodash';
import * as moment from 'moment';
import * as d3 from 'd3';
import * as L from 'leaflet';
import { TranslateService } from '@ngx-translate/core';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';
import { latLng, Map, tileLayer } from 'leaflet';

@Component({
  selector: 'app-comp-map-sebaran',
  templateUrl: './map-sebaran.component.html',
  styleUrls: ['./map-sebaran.component.scss'],
  providers: [MapSebaranService],
  encapsulation: ViewEncapsulation.None
})

export class MapSebaranComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  @ViewChild('chartRasio')
  private chartContainer!: ElementRef;

  @Input() dataPolygon!: any;
  @Input() dataRasio!: any;
  @Input() dataFilters!: any;

  @Output() setFilters = new EventEmitter<any>();

  dataset!: any;

  moment: any = moment;

  loading = true;
  polygon: any;

  map: any;
  color: any;
  colorLegend: any;
  contentWidth!: any;
  tooltip: any;
  totalKasus = 0;

  options = {
    layers: [
      tileLayer('https://tiles.stadiamaps.com/tiles/alidade_smooth/{z}/{x}/{y}{r}.png', {
        attribution: '&copy; <a href="https://stadiamaps.com/">Stadia Maps</a>, &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
        maxZoom: 18,
      })
    ],
    zoom: 8,
    center: latLng([ -7.0051667, 107.2264099 ])
  };

  sebaranType: any = [{
    index : 'confirmation',
    subindex : 'confirmation_diisolasi',
    text: 'Positif - Isolasi / Dalam Perawatan',
    color: '#ae9420'
  }, {
    index : 'confirmation',
    subindex : 'confirmation_selesai',
    text: 'Positif - Selesai Isolasi / Sembuh',
    color: '#00441f'
  }, {
    index : 'confirmation',
    subindex : 'confirmation_meninggal',
    text: 'Positif - Meninggal',
    color: '#7d0005'
  }, {
    index : 'suspect',
    subindex : 'suspect_diisolasi',
    text: 'Suspek - Isolasi / Dalam perawatan',
    color: '#beab8d'
  }, {
    index : 'closecontact',
    subindex : 'closecontact_dikarantina',
    text: 'Kontak Erat - Masih Dikarantina',
    color: '#905a08'
  }, {
    index : 'probable',
    subindex : 'probable_diisolasi',
    text: 'Probable - Isolasi / Dalam Perawatan',
    color: '#ceb546'
  }, {
    index : 'probable',
    subindex : 'probable_meninggal',
    text: 'Probable - Meninggal',
    color: '#7d0005'
  }];

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private globalService: GlobalService,
    private pikobarService: PikobarService,
    private mapService: MapSebaranService,
    private translateService: TranslateService,
    private localize: LocalizeRouterService
  ) {
  }

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    if (this.dataFilters) {
      if (!_.isObject(this.dataFilters)) {
        this.dataFilters = JSON.parse(this.dataFilters);
      }
    }
    this.initData();
  }

  ngOnDestroy(): void {}

  ngAfterViewInit(): void {}

  // Main Function
  onResize(event: any): void {
    this.drawLegend();
  }

  onMapReady(map: L.Map): void {
    this.map = map;

    this.map.removeControl(this.map.zoomControl);

    L.control.zoom({
      position: 'bottomleft'
    }).addTo(this.map);
  }

  initData(): void {
    this.initDataPolygon();
    this.initDataRasio();

    this.createMap();

    this.loading = false;
  }

  initDataPolygon(): void {
    if (this.dataPolygon) {
      this.dataPolygon.features = this.dataPolygon.features.filter((content: any) =>
        content.properties[this.dataFilters.level.parent] === this.dataFilters.kode
      );
      this.polygon = Object.assign({}, this.dataPolygon);
    }
  }

  initDataRasio(): void {
    if (this.dataRasio) {
      this.dataset = this.dataRasio.map((content: any) => {
        return {
          kode: content[this.dataFilters.level.kode],
          nama: content[this.dataFilters.level.nama],
          terkonfirmasi: content[this.dataFilters.sebaranType.index][this.dataFilters.sebaranType.subindex]
        };
      });

      this.totalKasus = this.dataset.map((a: any) => a.terkonfirmasi).reduce((a: any, b: any) => a + b);
    }
  }

  createMap(): void {
    if (this.polygon && this.dataset) {
      this.initMap();
      this.drawMap();
      this.initLegend();
      this.drawLegend();
    }
  }

  // Graphic Function
  initLegend(): void {
    const data = this.dataset;
    const maxValue = d3.max(data, (d: any) => _.toNumber(d.terkonfirmasi)) as number;
    const colorDomain = this.mapService.getColorDomain(maxValue);

    this.contentWidth = d3.select('.legend-sebaran').style('width').slice(0, -2);

    const n = Math.min(this.color.domain().length, this.color.range().length);
    this.colorLegend = this.color.copy()
      .rangeRound(
        d3.quantize(
          d3.interpolate(
            0, this.contentWidth
          ), n)
      );
  }

  initMap(): void {
    const data = this.dataset;

    d3.selectAll('.leaflet-interactive').remove();
    d3.selectAll('.map-tooltip').remove();
    this.map.invalidateSize();

    this.color = d3
      .scaleLinear()
      .domain([
        d3.min(data, (d: any) => _.toNumber(d.terkonfirmasi)) as number,
        d3.max(data, (d: any) => _.toNumber(d.terkonfirmasi)) as number
      ])
      .range(['#f5f5f5', this.dataFilters.sebaranType.color]);

    this.tooltip = d3.select('body').append('div')
      .attr('class', 'chart-tooltip map-tooltip')
      .style('opacity', 0);

    d3.select('.map-title')
      .style('background', this.dataFilters.sebaranType.color)
      .style('border-color', this.dataFilters.sebaranType.color);
  }

  drawMap(): void {
    const polygonWilayah = this.getPolygon();

    this.map.addLayer(polygonWilayah);
    this.map.fitBounds(polygonWilayah.getBounds());
  }

  getPolygon(): any {
    return L.geoJSON(this.polygon, {
      style: (feature) => {
        if (this.dataset.filter((d: any) => (d.kode === feature?.properties[this.dataFilters.level.polygon_kode])).length === 0) {
          return {};
        }

        return {
          color: '#666',
          fillColor: this.color(this.dataset.filter((d: any) =>
            (d.kode === feature?.properties[this.dataFilters.level.polygon_kode]))[0].terkonfirmasi
          ),
          fillOpacity: 1,
          weight: 1
        };
      },
      onEachFeature: (feature, layer) => {
        if (this.dataset.filter((d: any) => (d.kode === feature?.properties[this.dataFilters.level.polygon_kode])).length === 0) {
          return;
        }

        const that = this;

        layer.on('mouseover', (event: any) => {
          const selectedCity = this.dataset.filter((d: any) =>
            (d.kode === feature?.properties[this.dataFilters.level.polygon_kode]))[0];

          this.tooltip.transition()
            .duration(100)
            .style('opacity', .9);

          this.tooltip.html('<b>'
              + feature?.properties[this.dataFilters.level.polygon_nama]
              + `</b><br/>${this.dataFilters.sebaranType.text}:`
              + that.mapService.numberFormat(selectedCity.terkonfirmasi || 0))
            .style('left', (event.originalEvent.pageX + 10) + 'px')
            .style('top', (event.originalEvent.pageY - 28) + 'px');
        });

        layer.on('mouseout', (e) => {
          this.tooltip.transition()
            .duration(500)
            .style('opacity', 0);
        });
      }
    });
  }

  selectSebaranType(type: any): void {
    this.dataFilters.sebaranType = type;

    this.initData();
    this.setFilters.emit(this.dataFilters);
  }

  numberFormat(d: any): string {
    return this.mapService.numberFormat(d);
  }

  drawLegend(): void {
    d3.select('.legend-sebaran canvas').remove();
    d3.select('.legend-sebaran svg').remove();

    const data = this.dataset;
    const maxValue = d3.max(data, (d: any) => _.toNumber(d.terkonfirmasi)) as number;
    const dataColor = [...new Array(maxValue).keys()];

    const legendCanvas = d3.select('.legend-sebaran')
      .append('canvas')
      .attr('width', this.contentWidth)
      .attr('height', 100)
      .style('position', 'absolute')
      .style('top', '50px')
      .style('left', 0) as any;
    const legendContext = legendCanvas.node()?.getContext('2d');

    const legendSVG = d3.select('.legend-sebaran')
      .append('svg')
      .attr('width', this.contentWidth)
      .append('g')
      .attr('transform', `translate(0, 30)`);

    legendSVG.append('g')
      .attr('transform', `translate(0, 0)`)
      .append('text')
      .attr('class', 'legend-title')
      .attr('text-anchor', 'middle')
      .attr('dominant-baseline', 'central')
      .attr('x', this.colorLegend(Math.floor(maxValue / 2)))
      .attr('y', 0)
      .attr('font-size', 14)
      .text('Tingkat Banyaknya Kasus');

    legendSVG.append('g')
      .attr('class', 'axis axis--color')
      .attr('transform', `translate(0, 20)`)
      .call(d3.axisBottom(this.colorLegend).tickSize(20))
      .call((d: any) => d.select('.domain').remove())
      .selectAll('text')
      .style('text-anchor', 'start')
      .attr('dx', '2.2em')
      .attr('dy', '-.5em')
      .attr('transform', 'rotate(65)');

    dataColor.forEach((d: any) => {
      legendContext.fillStyle = this.color(d);
      legendContext.fillRect(this.colorLegend(d), 0, 5, 20);
    });
  }
}
