import { Injectable } from '@angular/core';

@Injectable()
export class ChartDirumahService {
  constructor() {}

  getColorDomain(max: any): any {
    const loop = new Array(9);
    const data = [0];
    const splitMax = Math.floor(max / 9);

    for (let i = 0; i < 9; i++) {
      if (i === 8) {
        data.push(max);
      } else {
        data.push(splitMax * i);
      }
    }

    return data;
  }

  reformatDate(date: any): string {
    const dateArr = date.split('-');
    const months = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'];
    const reformat = `${dateArr[2]} ${months[(dateArr[1] * 1)]} ${dateArr[0]}`;

    return reformat;
  }

  reformatHour(hour: any): string {
    hour = hour * 1;

    return (hour < 10) ? `0${hour}:00` : `${hour}:00`;
  }

  percentage(x: any): string {
    return x.toFixed(2) + '%';
  }

  numberFormat(x: any): string {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }

  filterDate(dataFilters: any, tanggal: any): boolean{
    const arrDate = tanggal.split('-');
    const newDate = `${arrDate[1]}/${arrDate[2]}/${arrDate[0]}`;
    const curDate = new Date(newDate);

    const fromDate = new Date(`${dataFilters.range.from.month}/${dataFilters.range.from.day}/${dataFilters.range.from.year}`);
    const toDate = new Date(`${dataFilters.range.to.month}/${dataFilters.range.to.day}/${dataFilters.range.to.year}`);

    return curDate.getTime() >= fromDate.getTime() && curDate.getTime() <= toDate.getTime();
  }
}
