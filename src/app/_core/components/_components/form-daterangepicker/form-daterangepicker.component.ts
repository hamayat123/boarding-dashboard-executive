import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  AfterViewInit,
  Input,
  SimpleChanges,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Output,
  EventEmitter,
  Attribute,
  ViewEncapsulation,
} from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';

// SERVICE
import { GlobalService, PikobarService } from '@core/services';
import { FormDaterangepickerService } from './form-daterangepicker.service';

// PACKAGE
import * as _ from 'lodash';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';
import { FormControl, Validators } from '@angular/forms';
import { NgbCalendar, NgbDate, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-comp-daterangepicker',
  templateUrl: './form-daterangepicker.component.html',
  styleUrls: ['./form-daterangepicker.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [FormDaterangepickerService],
  encapsulation: ViewEncapsulation.None
})

export class FormDaterangepickerComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  moment: any = moment;

  @Input() dataFilters!: any;
  @Output() setFilters = new EventEmitter<any>();

  hoveredDate: NgbDate | null = null;
  fromDate: NgbDate | null = null;
  toDate: NgbDate | null = null;
  minDate = {
    year: 2019,
    month: 1,
    day: 1
  };

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private globalService: GlobalService,
    private pikobarService: PikobarService,
    private formDaterangepickerService: FormDaterangepickerService,
    private translateService: TranslateService,
    private localize: LocalizeRouterService,
    private calendar: NgbCalendar,
    public formatter: NgbDateParserFormatter
  ) {
  }

  ngOnInit(): void {
    this.fromDate = this.dataFilters.range.from;
    this.toDate = this.dataFilters.range.to;
  }

  ngOnChanges(changes: SimpleChanges): void {}

  ngOnDestroy(): void {}

  ngAfterViewInit(): void {}

  onDateSelection(date: NgbDate): void {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate)) {
      this.toDate = date;

      this.dataFilters = {
        ...this.dataFilters
      };
      this.dataFilters.range.from = this.fromDate;
      this.dataFilters.range.to = this.toDate;

      this.setFilters.emit(this.dataFilters);
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  isHovered(date: NgbDate): any {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate): any {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate): any {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  get getDatePickerValue(): string {
    if (this.fromDate && this.toDate) {
      return `${this.formatter.format(this.fromDate)} s/d ${this.formatter.format(this.toDate)}`;
    } else if (this.fromDate && !this.toDate) {
      return `${this.formatter.format(this.fromDate)}`;
    }

    return '';
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }
}
