import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  AfterViewInit,
  Input,
  SimpleChanges,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Output,
  EventEmitter,
  Attribute,
} from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';

// SERVICE
import { GlobalService } from '@core/services';
import { FormInputService } from './form-input.service';

// PACKAGE
import * as _ from 'lodash';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-comp-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [FormInputService],
})

export class FormInputComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  moment: any = moment;

  @Input() label: any;
  @Input() classRow: any;
  @Input() customFormControl: any;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private globalService: GlobalService,
    private formInputService: FormInputService,
    private translateService: TranslateService,
    private localize: LocalizeRouterService,

    @Attribute('type') public type?: any,
    @Attribute('placeholder') public placeholder?: any,
    @Attribute('class') public className?: any
  ) {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {}

  ngOnDestroy(): void {}

  ngAfterViewInit(): void {}
}
