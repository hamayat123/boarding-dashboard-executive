import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  AfterViewInit,
  Input,
  SimpleChanges,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Output,
  EventEmitter,
  Attribute,
  ViewEncapsulation,
} from '@angular/core';

@Component({
  selector: 'app-comp-button-loading',
  templateUrl: './button-loading.component.html',
  styleUrls: ['./button-loading.component.scss']
})
export class ButtonLoadingComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {

  @Input() isLoading!: boolean;
  @Input() isDisabled!: boolean;
  @Input() text!: string;
  @Input() className!: string;

  constructor(
    @Attribute('type') public type?: any,
    @Attribute('value') public value?: any
  ) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {}

  ngOnDestroy(): void {}

  ngAfterViewInit(): void {}
}
