import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

// SERVICE
import { AuthService } from '@core/services';

// PACKAGE
import { BehaviorSubject } from 'rxjs';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';
import { JwtHelperService } from '@auth0/angular-jwt';


@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

  // VARIABLE

  constructor(
    private router: Router,
    private authService: AuthService,
    private localize: LocalizeRouterService
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const helper = new JwtHelperService();

    const data = this.authService?.dashboardUserValue;
    if (data) {
      const isExpiredBantu = helper.isTokenExpired(data.jwt);
      if (!isExpiredBantu) {
        return true;
      }
    }

    const token = this.authService?.dashboardTokenValue;
    if (token) {
      const isExpired = helper.isTokenExpired(token);
      if (!isExpired) {
        return true;
      }
    }

    const tempTranslatedPath: any = this.localize.translateRoute('/auth/login');
    this.router.navigate([tempTranslatedPath], {
      queryParams: { returnUrl: state.url }
    });
    return false;
  }
}
