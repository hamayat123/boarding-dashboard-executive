import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { BehaviorSubject, Observable, of } from 'rxjs';

@Injectable()
export class SidebarService {
  constructor() {}

  private sidebarSource = new BehaviorSubject([]);
  currentSidebar = this.sidebarSource.asObservable();

  changeSidebar(sidebar: any): void {
    this.sidebarSource.next(sidebar);
  }

  getComponents(): any {
    const menu = [
      {
        name: 'avatar',
        link: '/components/avatar',
        children: [{ name: 'avatar-item', link: '/avatar-item' }],
      },
      { name: 'breadcrumbs', link: '/components/breadcrumbs' },
      // {
      //   name: 'button',
      //   link: '/components/button',
      //   children: [{ name: 'button-group', link: '/button-group' }],
      // },
      { name: 'button', link: '/components/button' },
      { name: 'checkbox', link: '/components/checkbox' },
      { name: 'dropdown-menu', link: '/components/dropdown-menu' },
      { name: 'pagination', link: '/components/pagination' },
      { name: 'radio', link: '/components/radio' },
      { name: 'section-message', link: '/components/section-message' },
      { name: 'select', link: '/components/select' },
      { name: 'table', link: '/components/table' },
      { name: 'tabs', link: '/components/tabs' },
      { name: 'textfield', link: '/components/textfield' },
      { name: 'tooltip', link: '/components/tooltip' },
    ];

    return menu;
  }

  getDashboardPikobar(): any {
    const menu = [
      { name: 'Trace', link: '/dashboard-pikobar/trace', icon: 'fas fa-chart-line', children: [
        { name: 'Statistik COVID-19', link: '/statistik' },
        { name: 'Proyeksi', link: '/proyeksi' }
      ]},
      { name: 'Test', link: '/dashboard-pikobar/test', icon: 'fas fa-vials', children: [
        { name: 'Analisis', link: '/analisis' },
        { name: 'PCR', link: '/pcr' }
      ]},
      { name: 'Treatment', link: '/dashboard-pikobar/treatment', icon: 'fas fa-clinic-medical' },
      { name: 'Support', link: '/dashboard-pikobar/support', icon: 'fas fa-shield-alt' },
    ];

    return menu;
  }

  getDashboardBapenda(): any {
    const menu = [
      { name: 'Pajak', link: '/dashboard-bapenda/pajak', icon: 'fas fa-file-invoice-dollar', children: [
        { name: 'Kendaraan Bermotor', link: '/kendaraan-bermotor' },
        { name: 'Bahan Bakar Kendaraan', link: '/bahan-bakar' },
        { name: 'Air Permukaan', link: '/air-permukaan' },
        { name: 'Rokok', link: '/rokok' }
      ]},
      { name: 'Kinerja Utama', link: '/dashboard-bapenda/indikator-kinerja-utama', icon: 'fas fa-chart-line' },
      { name: 'Kinerja Instansi', link: '/dashboard-bapenda/kinerja-instansi-pemerintah', icon: 'fas fa-project-diagram' }
    ];

    return menu;
  }
}
