import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { UserData } from '@auth-models';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // API URL
  apiUrl = `${environment.apiDashboardUrl}api-auth/auth/login`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  private dashboardUserSubject: BehaviorSubject<UserData>;
  public dashboardUser: Observable<UserData>;

  private dashboardTokenSubject: BehaviorSubject<string>;
  public dashboardToken: Observable<string>;

  constructor(private http: HttpClient) {
    this.dashboardUserSubject = new BehaviorSubject<UserData>(
      JSON.parse(localStorage.getItem('dashboard-user') || 'null')
    );
    this.dashboardUser = this.dashboardUserSubject.asObservable();

    this.dashboardTokenSubject = new BehaviorSubject<string>(
      localStorage.getItem('dashboard-token') || ''
    );
    this.dashboardToken = this.dashboardTokenSubject.asObservable();
  }

  public get dashboardUserValue(): UserData {
    return this.dashboardUserSubject.value;
  }

  public get dashboardTokenValue(): string {
    return this.dashboardTokenSubject.value;
  }

  public get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('dashboard-user') || 'null');
    return user !== null ? true : false;
  }

  login(body = {}): Observable<any> {
    return this.http.post<any>(this.apiUrl, body, this.httpOptions).pipe(
      map(result => {
        if (result.error === 0) {
          localStorage.setItem('dashboard-user', JSON.stringify(result.data));
          this.dashboardUserSubject.next(result.data);

          localStorage.setItem('dashboard-token', result.data.jwt);
          this.dashboardTokenSubject.next(result.data.jwt);
        }
        return result;
      })
    );
  }

  logout(): void {
    localStorage.removeItem('dashboard-user');
    localStorage.removeItem('dashboard-token');

    // @ts-ignore
    this.dashboardUserSubject.next(null);
    this.dashboardTokenSubject.next('');
  }
}
