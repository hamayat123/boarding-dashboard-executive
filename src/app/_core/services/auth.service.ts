import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class AuthService {
  // VARIABLE
  private userSource = new BehaviorSubject(null);
  currentUser = this.userSource.asObservable();
  
  userList: any[] = [{
    username: 'superadmin',
    password: '123',
    level: 1,
    name: 'Sumarno'
  }, {
    username: 'pikobar',
    password: '123',
    level: 2,
    name: 'Amir Saripudin'
  }, {
    username: 'bapenda',
    password: '123',
    level: 3,
    name: 'Dadang Konelo'
  }];

  constructor() {}

  login(username: string, password: string): any {
    const selectedUser = this.userList.filter((d: any) => {
      return d.username === username && d.password === password
    });
    
    if(selectedUser.length == 1){
      const tempUser = Object.assign({}, selectedUser[0]);
      delete tempUser.password;

      sessionStorage.setItem('auth', JSON.stringify(tempUser));
      this.userSource.next(tempUser);
      
      return tempUser;
    }

    return false;
  }

  logout(): void {
    const auth = sessionStorage.getItem('auth');

    sessionStorage.removeItem('auth');
    this.userSource.next(null);
  }

  getAuthSession(): void {
    const auth = sessionStorage.getItem('auth');

    if (auth) {
      const user = JSON.parse(auth);
      this.userSource.next(user);
    } 
  }
}
