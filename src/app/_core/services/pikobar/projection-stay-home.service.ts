import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

import { RecapitulationDaily } from '@pikobar-models';

@Injectable({
  providedIn: 'root'
})
export class ProjectionStayHomeService {
  // API URL
  apiUrl = `${environment.apiDashboardUrl}api-pt-pos/analytic/stayput`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'api-key': '480d0aeb78bd0064d45ef6b2254be9b3'
    })
  };

  constructor(public http: HttpClient) {}

  getItems(params: string = ''): Observable<any> {
    return this.http.get<any>(this.apiUrl + params, this.httpOptions);
  }
}
