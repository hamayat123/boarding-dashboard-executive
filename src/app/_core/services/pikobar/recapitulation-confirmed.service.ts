import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

import { RecapitulationDaily } from '@pikobar-models';

@Injectable({
  providedIn: 'root'
})
export class RecapitulationConfirmedService {
  // API URL
  apiUrl = `${environment.apiDashboardApiUrl}v2/kasus`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'api-key': '480d0aeb78bd0064d45ef6b2254be9b3'
    })
  };

  constructor(public http: HttpClient) {}

  getDailyItems(params: string = ''): Observable<any> {
    return this.http.get<any>(this.apiUrl + '/harian' + params, this.httpOptions);
  }

  getWeeklyItems(params: string = ''): Observable<any> {
    return this.http.get<any>(this.apiUrl + '/mingguan_v3' + params, this.httpOptions);
  }

  getTwoWeeklyItems(params: string = ''): Observable<any> {
    return this.http.get<any>(this.apiUrl + '/dwimingguan_v3' + params, this.httpOptions);
  }

  getNasionalItems(params: string = ''): Observable<any> {
    return this.http.get<any>(this.apiUrl + '/nasional' + params, this.httpOptions);
  }
}
