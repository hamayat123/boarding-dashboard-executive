import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MapPolygonService {
  // API URL
  kotaPolygonUrl = '/assets/json/kotaV2.json';
  kecamatanPolygonUrl = '/assets/json/kecamatanV2.json';
  kelurahanPolygonUrl = '/assets/json/kelurahanV2.json';

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(public http: HttpClient) {}

  getKota(params: string = ''): Observable<any> {
    return this.http.get<any>(this.kotaPolygonUrl + params, this.httpOptions);
  }

  getKecamatan(params: string = ''): Observable<any> {
    return this.http.get<any>(this.kecamatanPolygonUrl + params, this.httpOptions);
  }

  getKelurahan(params: string = ''): Observable<any> {
    return this.http.get<any>(this.kelurahanPolygonUrl + params, this.httpOptions);
  }
}
