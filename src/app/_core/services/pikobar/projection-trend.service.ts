import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

import { RecapitulationDaily } from '@pikobar-models';

@Injectable({
  providedIn: 'root'
})
export class ProjectionTrendService {
  // API URL
  apiUrl = `${environment.apiDashboardUrl}api-pt-pos/analytic/`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'api-key': '480d0aeb78bd0064d45ef6b2254be9b3'
    })
  };

  constructor(public http: HttpClient) {}

  getIndonesiaItems(params: string = ''): Observable<any> {
    return this.http.get<any>(this.apiUrl + 'trend_indonesia' + params, this.httpOptions);
  }

  getJabarItems(params: string = ''): Observable<any> {
    return this.http.get<any>(this.apiUrl + 'trend_jabar' + params, this.httpOptions);
  }

  getKotaItems(params: string = ''): Observable<any> {
    return this.http.get<any>(this.apiUrl + 'trend_kota' + params, this.httpOptions);
  }
}
