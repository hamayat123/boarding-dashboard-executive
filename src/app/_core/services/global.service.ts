import {Injectable, Inject, Renderer2} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class GlobalService {
  constructor(@Inject(DOCUMENT) private dom: any) {}

  readonly title: string = 'This Title';

  private sidebarSource = new BehaviorSubject('This Sidebar');
  currentSidebar = this.sidebarSource.asObservable();

  private toggleSidebarSource = new BehaviorSubject(false);
  currentToggleSidebar = this.toggleSidebarSource.asObservable();

  private labelSource = new BehaviorSubject('This Label');
  currentLabel = this.labelSource.asObservable();

  private descriptionSource = new BehaviorSubject('This Description');
  currentDescription = this.descriptionSource.asObservable();

  private breadcrumbSource = new BehaviorSubject([]);
  currentBreadcrumb = this.breadcrumbSource.asObservable();

  private togglePublicSource = new BehaviorSubject(false);
  currentTogglePublic = this.togglePublicSource.asObservable();

  private toggleDarkModeSource = new BehaviorSubject(false);
  currentDarkMode = this.toggleDarkModeSource.asObservable();

  changeSidebar(sidebar: string): void {
    this.sidebarSource.next(sidebar);
  }

  changeToggleSidebar(toggleSidebar: boolean): void {
    setTimeout(() => {
      window.dispatchEvent(new Event('resize'));
    }, 500);
    this.toggleSidebarSource.next(toggleSidebar);
  }

  changeTogglePublic(togglePublic: boolean): void {
    this.togglePublicSource.next(togglePublic);
  }

  changeToggleDarkMode(toggleDarkMode: boolean): void {
    localStorage.setItem('dark-mode', JSON.stringify(toggleDarkMode));
    if (toggleDarkMode) {
      this.dom.body.classList.add('bootstrap-dark');
    } else {
      this.dom.body.classList.remove('bootstrap-dark');
    }
    this.toggleDarkModeSource.next(toggleDarkMode);
  }

  changeLabel(label: string): void {
    this.labelSource.next(label);
  }

  changeDescription(description: string): void {
    this.descriptionSource.next(description);
  }

  changeBreadcrumb(breadcrumb: any): void {
    this.breadcrumbSource.next(breadcrumb);
  }

  changeCanonicalURL(url?: string): void {
    const canURL = url === undefined ? this.dom.URL : url;
    const link: HTMLLinkElement = this.dom.createElement('link');
    link.setAttribute('rel', 'canonical');
    this.dom.head.appendChild(link);
    link.setAttribute('href', canURL);
  }

  currentURL(url?: string): string {
    const URL = url === undefined ? this.dom.URL : url;
    return URL;
  }
}
