import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class PikobarService {
  constructor() {}

  private dataRekapSource = new BehaviorSubject([]);
  dataRekap = this.dataRekapSource.asObservable();

  private filterHarianSource = new BehaviorSubject([]);
  filterHarian = this.filterHarianSource.asObservable();

  private dataSebaranSource = new BehaviorSubject([]);
  dataSebaran = this.dataSebaranSource.asObservable();

  private dataRasioSource = new BehaviorSubject([]);
  dataRasio = this.dataRasioSource.asObservable();

  private filterRasioSource = new BehaviorSubject([]);
  filterRasio = this.filterRasioSource.asObservable();
  
  setDataRekap(data: any): void {
    this.dataRekapSource.next(data);
  }
  
  setFilterHarian(data: any): void {
    this.filterHarianSource.next(data);
  }
  
  setDataRasio(data: any): void {
    this.dataRasioSource.next(data);
  }
  
  setFilterRasio(data: any): void {
    this.filterRasioSource.next(data);
  }
  
  setDataSebaran(data: any): void {
    this.dataSebaranSource.next(data);
  }
}
