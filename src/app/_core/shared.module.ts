import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// PACKAGE
import { TranslateModule } from '@ngx-translate/core';
import { LocalizeRouterModule } from '@gilsdav/ngx-translate-router';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { HighlightJsModule } from 'ngx-highlight-js';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

// COMPONENT
import { AvatarComponent as CompAvatarComponent } from '@components/_components/avatar/avatar.component';
import { AvatarItemComponent as CompAvatarItemComponent } from '@components/_components/avatar-item/avatar-item.component';
import { FormInputComponent as CompInputComponent } from '@components/_components/form-input/form-input.component';
import { ChartTerkonfirmasiComponent as CompChartTerkonfirmasiComponent } from '@components/pikobar/chart-terkonfirmasi/chart-terkonfirmasi.component';
import { ChartTerkonfirmasiPcrComponent as CompChartTerkonfirmasiPcrComponent } from '@components/pikobar/chart-terkonfirmasi-pcr/chart-terkonfirmasi-pcr.component';
import { ChartPcrSumberComponent as CompChartPcrSumberComponent } from '@components/pikobar/chart-pcr-sumber/chart-pcr-sumber.component';
import { ChartKemacetanComponent as CompChartKemacetanComponent } from './components/pikobar/chart-kemacetan/chart-kemacetan.component';
import { ChartKonsentrasiKasusComponent as CompChartKonsentrasiKasusComponent } from './components/pikobar/chart-konsentrasi-kasus/chart-konsentrasi-kasus.component';
import { ChartPertumbuhanKasusComponent as CompChartPertumbuhanKasusComponent } from './components/pikobar/chart-pertumbuhan-kasus/chart-pertumbuhan-kasus.component';
import { ChartDirumahComponent as CompChartDirumahComponent } from './components/pikobar/chart-dirumah/chart-dirumah.component';
import { ChartRasioComponent as CompChartRasioComponent } from '@components/pikobar/chart-rasio/chart-rasio.component';
import { ChartProyeksiComponent as CompChartProyeksiComponent } from '@components/pikobar/chart-proyeksi/chart-proyeksi.component';
import { ChartTrenComponent as CompChartTrenComponent } from '@components/pikobar/chart-tren/chart-tren.component';
import { ChartTrenIndonesiaComponent as CompChartTrenIndonesiaComponent } from '@components/pikobar/chart-tren-indonesia/chart-tren-indonesia.component';
import { ChartTrenPositivityComponent as CompChartTrenPositivityComponent } from '@components/pikobar/chart-tren-positivity/chart-tren-positivity.component';
import { ChartMobilitasComponent as CompChartMobilitasComponent } from '@components/pikobar/chart-mobilitas/chart-mobilitas.component';
import { MapSebaranComponent as CompMapSebaranComponent } from '@components/pikobar/map-sebaran/map-sebaran.component';
import { FormDaterangepickerComponent as CompDaterangepickerComponent } from '@components/_components/form-daterangepicker/form-daterangepicker.component';
import { ButtonLoadingComponent as CompButtonLoadingComponent } from './components/_components/button-loading/button-loading.component';

import { PageComponent as HeaderPageComponent } from '@components/header/page/page.component';

// WEB COMPONENT
import 'rendikit-avatar';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    LeafletModule,
    // PACKAGE
    TranslateModule,
    LocalizeRouterModule,
    LoadingBarRouterModule,
    HighlightJsModule,
    NgSelectModule
  ],
  declarations: [
    // COMPONENT
    CompAvatarComponent,
    CompAvatarItemComponent,
    HeaderPageComponent,
    CompInputComponent,
    CompDaterangepickerComponent,
    // COMPONENT PIKOBAR
    CompChartTerkonfirmasiComponent,
    CompChartTerkonfirmasiPcrComponent,
    CompChartPcrSumberComponent,
    CompChartKemacetanComponent,
    CompChartKonsentrasiKasusComponent,
    CompChartPertumbuhanKasusComponent,
    CompChartDirumahComponent,
    CompChartRasioComponent,
    CompChartProyeksiComponent,
    CompChartTrenComponent,
    CompChartTrenIndonesiaComponent,
    CompChartTrenPositivityComponent,
    CompChartMobilitasComponent,
    CompMapSebaranComponent,
    CompButtonLoadingComponent,
  ],
  exports: [
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    // PACKAGE
    TranslateModule,
    LocalizeRouterModule,
    LoadingBarRouterModule,
    HighlightJsModule,
    NgSelectModule,
    // COMPONENT
    CompAvatarComponent,
    CompAvatarItemComponent,
    HeaderPageComponent,
    CompInputComponent,
    CompDaterangepickerComponent,
    // COMPONENT PIKOBAR
    CompChartTerkonfirmasiComponent,
    CompChartTerkonfirmasiPcrComponent,
    CompChartPcrSumberComponent,
    CompChartKemacetanComponent,
    CompChartKonsentrasiKasusComponent,
    CompChartPertumbuhanKasusComponent,
    CompChartDirumahComponent,
    CompChartRasioComponent,
    CompChartProyeksiComponent,
    CompChartTrenComponent,
    CompChartTrenIndonesiaComponent,
    CompChartTrenPositivityComponent,
    CompChartMobilitasComponent,
    CompMapSebaranComponent,
    CompButtonLoadingComponent
  ],
  providers: [],
  schemas: [],
})
export class SharedModule {}
