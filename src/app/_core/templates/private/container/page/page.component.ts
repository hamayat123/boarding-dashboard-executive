import { Component } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

// SERVICE
import { GlobalService } from '@core/services';

@Component({
  selector: 'app-private-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],
})
export class PageComponent {
  toggleSidebar!: boolean;
  toggleSidebarEmitter$ = new BehaviorSubject<boolean>(this.toggleSidebar);

  togglePublic!: boolean;
  togglePublicEmitter$ = new BehaviorSubject<boolean>(this.togglePublic);

  constructor(private globalService: GlobalService) {
    this.globalService.currentToggleSidebar.subscribe((current) => {
      this.toggleSidebar = current;
      this.toggleSidebarEmitter$.next(current);
    });
    this.globalService.currentTogglePublic.subscribe((current) => {
      this.togglePublic = current;
      this.togglePublicEmitter$.next(current);
    });
  }
}
