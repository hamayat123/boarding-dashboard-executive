import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

// SERVICE
import { GlobalService, SidebarService } from '@core/services';

// PACKAGE
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';

@Component({
  selector: 'app-private-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class SidebarComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  moment: any = moment;

  currentRoute!: any;
  routeEmitter$ = new BehaviorSubject<any>(this.currentRoute);

  title!: string;
  titleEmitter$ = new BehaviorSubject<string>(this.title);

  menu!: any[];
  menuEmitter$ = new BehaviorSubject<any[]>(this.menu);

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private globalService: GlobalService,
    private sidebarService: SidebarService,
    private translateService: TranslateService,
    private localize: LocalizeRouterService
  ) {
    this.globalService.currentSidebar.subscribe((current) => {
      this.title = current;
      this.titleEmitter$.next(current);
      this.routeEmitter$.next(null);
    });

    this.sidebarService.currentSidebar.subscribe((current) => {
      this.menu = current;
      this.menuEmitter$.next(current);
    });
  }

  ngOnInit(): void {
    this.routeEmitter$.next(this.router.url.split('/')[3]);
  }

  ngOnChanges(changes: SimpleChanges): void {}

  ngOnDestroy(): void {}

  ngAfterViewInit(): void {}

  redirectTo(link: string): void {
    const tempTranslatedPath: any = this.localize.translateRoute(link);
    this.router.navigate([tempTranslatedPath]).then(() => {
      this.routeEmitter$.next(this.router.url.split('/')[3]);
    });
  }
}
