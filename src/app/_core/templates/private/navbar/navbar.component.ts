import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  AfterViewInit,
  SimpleChanges,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';

// SERVICE
import { AuthService, GlobalService } from '@core/services';

// PACKAGE
import * as _ from 'lodash';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-private-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class NavbarComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  moment: any = moment();

  public isMenuCollapsed = true;

  // Variable
  toggle = false;
  currentDarkmode = false;
  currentDashboard = 'Pikobar';
  currentUser!: any;
  assetUrl = `${environment.apiDashboardUrl}api-backend/`;

  levelList = ['', 'Super Admin', 'Admin Pikobar', 'Admin Bapenda'];

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private globalService: GlobalService,
    private translateService: TranslateService,
    private localize: LocalizeRouterService,
    private authService: AuthService
  ) {
    this.globalService.currentDarkMode.subscribe(
      d => this.currentDarkmode = d
    );
    this.authService.dashboardUser.subscribe(
      d => this.currentUser = d
    );
  }

  ngOnInit(): void {
    // if (this.currentUser?.level === 1) {
    //   const currentUrl = this.router.url.replace('/id', '').split('/');
    //   this.currentDashboard = _.capitalize(currentUrl[1].replace('dashboard-', ''));
    // }
  }

  ngOnChanges(changes: SimpleChanges): void {}

  ngOnDestroy(): void {}

  ngAfterViewInit(): void {}

  toggleSidebar(): void {
    this.toggle = !this.toggle;
    this.globalService.changeToggleSidebar(this.toggle);
  }

  toggleDarkMode(): void {
    this.currentDarkmode = !this.currentDarkmode;
    this.globalService.changeToggleDarkMode(this.currentDarkmode);
  }

  logout(e: any): void {
    e.preventDefault();

    this.authService.logout();

    const tempTranslatedPath: any = this.localize.translateRoute('/auth/login');
    this.router.navigate([tempTranslatedPath]);
  }

  setDashboard(e: any, type: string): void {
    e.preventDefault();

    this.currentDashboard = type;

    const tempTranslatedPath: any = this.localize.translateRoute('/dashboard-' + type.toLowerCase());
    this.router.navigate([tempTranslatedPath]);
  }
}
