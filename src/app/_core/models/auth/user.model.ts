import { Deserializable } from '../deserializable.model';

export class User implements Deserializable {
  [x: string]: any;
  message!: string;
  data!: UserData;
  error!: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class UserData implements Deserializable {
  [x: string]: any;
  nama_skpdunit!: string; // tslint:disable-line
  nama_skpd!: string; // tslint:disable-line
  username!: string;
  is_deleted!: boolean; // tslint:disable-line
  kode_kemendagri!: string; // tslint:disable-line
  regional_level!: number; // tslint:disable-line
  email!: string;
  kode_skpdsub!: string; // tslint:disable-line
  id!: string;
  profile_pic!: string; // tslint:disable-line
  role!: Role;
  is_external!: boolean; // tslint:disable-line
  app!: App;
  nama_skpdsub!: string; // tslint:disable-line
  nama_kemendagri!: string; // tslint:disable-line
  kode_skpd!: string; // tslint:disable-line
  name!: string;
  cdate!: Date;
  last_login!: Date; // tslint:disable-line
  jwt!: string;
  logo_skpd!: string; // tslint:disable-line
  kode_skpdunit!: string; // tslint:disable-line

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class App implements Deserializable {
  [x: string]: any;
  id!: string;
  url!: string;
  name!: string;
  code!: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class Role implements Deserializable {
  [x: string]: any;
  id!: string;
  name!: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
