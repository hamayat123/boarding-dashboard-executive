import { Deserializable } from '../deserializable.model';

export class RecapitulationDaily implements Deserializable {
  [x: string]: any;
  status_code!: number; // tslint:disable-line
  data!: RecapitulationDailyData;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class RecapitulationDailyData implements Deserializable {
  [x: string]: any;
  metadata!: RecapitulationDailyMetadata;
  content!: RecapitulationDailyContent;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class RecapitulationDailyContent implements Deserializable {
  [x: string]: any;
  tanggal!: Date;
  kode_prov!: string; // tslint:disable-line
  nama_prov!: string; // tslint:disable-line
  kode_kab!: string; // tslint:disable-line
  nama_kab!: string; // tslint:disable-line
  odp!: number;
  pdp!: number;
  positif!: number;
  sembuh!: number;
  meninggal!: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class RecapitulationDailyMetadata implements Deserializable {
  [x: string]: any;
  last_update!: null; // tslint:disable-line

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
