export default function dateFormatter(d: any): string {
	const dateArr = d.split('-');
	const months = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'];
	const reformat = `${dateArr[2]} ${months[(dateArr[1] * 1)]} ${dateArr[0]}`;

	return reformat;
}