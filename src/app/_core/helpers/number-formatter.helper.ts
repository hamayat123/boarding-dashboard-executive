export default function numberFormatter(d: any): string {
	return d.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
}