import { Routes } from '@angular/router';
import { AuthGuard } from '@guards';

// COMPONENT (PUBLIC)
import { PageComponent as PrivatePageComponent } from '@templates/private/container/page/page.component';
import { PageComponent as PublicPageComponent } from '@templates/public/container/page/page.component';

export const AppRoutingModule: Routes = [
  {
    path: '',
    component: PublicPageComponent,
    data: {
      discriminantPathKey: 'PUBLICPATH',
    },
    children: [
      {
        path: '',
        redirectTo: 'auth/login',
        pathMatch: 'full'
      },
      {
        path: 'auth/login',
        loadChildren: () => import('./views/login/login.module').then((m) => m.LoginModule),
      },
    ],
  },
  {
    path: '',
    component: PrivatePageComponent,
    canActivate: [ AuthGuard ],
    data: {
      discriminantPathKey: 'PRIVATEPATH',
    },
    children: [
      {
        path: 'dashboard-pikobar',
        loadChildren: () => import('./views/dashboard-pikobar/dashboard-pikobar.module').then((m) => m.DashboardPikobarModule),
      },
      {
        path: 'dashboard-bapenda',
        loadChildren: () => import('./views/dashboard-bapenda/dashboard-bapenda.module').then((m) => m.DashboardBapendaModule),
      },
    ],
  },
];
