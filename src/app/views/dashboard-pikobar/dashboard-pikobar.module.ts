import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardPikobarRoutingModule } from './dashboard-pikobar-routing.module';

@NgModule({
  imports: [CommonModule, DashboardPikobarRoutingModule]
})
export class DashboardPikobarModule {}
