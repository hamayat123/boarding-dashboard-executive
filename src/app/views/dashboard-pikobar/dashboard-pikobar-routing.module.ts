import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LocalizeRouterModule } from '@gilsdav/ngx-translate-router';

// RESOLVER
import { SidebarDashboardPikobarResolver } from '@resolvers';

const routes: Routes = [
  {
    path: '',
    resolve: {
      hero: SidebarDashboardPikobarResolver,
    },
    children: [
      {
        path: '',
        loadChildren: () => import('./trace-page/trace-page.module').then((m) => m.TracePageModule),
      },
      {
        path: 'trace',
        loadChildren: () => import('./trace-page/trace-page.module').then((m) => m.TracePageModule),
      },
      {
        path: 'test',
        loadChildren: () => import('./test-page/test-page.module').then((m) => m.TestPageModule),
      },
      {
        path: 'treatment',
        loadChildren: () => import('./treatment-page/treatment-page.module').then((m) => m.TreatmentPageModule),
      },
      {
        path: 'support',
        loadChildren: () => import('./support-page/support-page.module').then((m) => m.SupportPageModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), LocalizeRouterModule.forChild(routes)],
  exports: [RouterModule, LocalizeRouterModule],
})
export class DashboardPikobarRoutingModule {}
