import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  AfterViewInit,
  SimpleChanges,
  ChangeDetectorRef,
  ViewChild,
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

// SERVICE
import { GlobalService } from '@services';
import {
  RecapitulationCaseTotalService,
  RecapitulationConfirmedService,
  RecapitulationPcrService, RecapitulationPcrSourceService
} from '@pikobar-services';

// PACKAGE
import * as _ from 'lodash';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { ChartTerkonfirmasiComponent } from '@core/components/pikobar/chart-terkonfirmasi/chart-terkonfirmasi.component';

@Component({
  selector: 'app-analisis-page',
  templateUrl: './analisis-page.component.html',
  styleUrls: ['./analisis-page.component.scss'],
})
export class AnalisisPageComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  moment: any = moment;
  JSON: any = JSON;

  // Settings
  title!: string;
  label!: string;
  description!: string;
  breadcrumb!: any[];
  activeTab = 1;

  terkonfirmasiOptions = [
    {
      text: 'Terkonfirmasi',
      key: 'confirmation_total',
    },
    {
      text: 'Dalam Perawatan (Aktif)',
      key: 'confirmation_diisolasi',
    },
    {
      text: 'Selesai Isolasi / Sembuh',
      key: 'confirmation_selesai',
    },
    {
      text: 'Meninggal',
      key: 'confirmation_meninggal',
    },
  ];

  periodeOptions = [
    {
      text: 'Harian',
      key: 'harian',
    },
    {
      text: 'Mingguan',
      key: 'mingguan',
    },
    {
      text: 'Dwi Mingguan',
      key: 'dwimingguan',
    },
  ];
  dataTerkonfirmasiPcrFilter: any = {
    kategori: '',
    jenisPeriode: 'mundur',
    periodeOption: this.periodeOptions[2],
    range: {
      from: {
        year: 2020,
        month: 3,
        day: 1
      },
      to: null
    }
  };

  dataPcrSumberFilter: any = {
    jenisPeriode: 'mundur',
    periodeOption: this.periodeOptions[2],
    range: {
      from: {
        year: 2020,
        month: 3,
        day: 1
      },
      to: null
    }
  };

  dataTrenPcrFilter: any = {
    jenisPeriode: 'mundur',
    periodeOption: this.periodeOptions[2],
    range: {
      from: {
        year: 2020,
        month: 3,
        day: 1
      },
      to: null
    }
  };

  dataPertumbuhanKasusFilter: any = {
    jenisPeriode: 'mundur',
    terkonfirmasiOption: this.terkonfirmasiOptions[0],
    periodeOption: this.periodeOptions[2],
    range: {
      from: {
        year: 2020,
        month: 3,
        day: 1
      },
      to: null
    }
  };

  // Variable
  dataWilayah: any;
  dataTerkonfirmasiSubscription: any;
  dataPcrSumberSubscription: any;
  dataPcrSubscription: any;
  dataTrenPcrSubscription: any;
  dataPertumbuhanKasusSubscription: any;

  dataTerkonfirmasiPcr = {
    terkonfirmasi: null,
    pcr: null
  };
  dataPcrSumber: any;
  dataTrenPcr: any;
  dataKonsentrasiKasus: any;
  dataPertumbuhanKasus: any;
  loading = true;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private titleService: Title,
    private globalService: GlobalService,
    private translateService: TranslateService,
    private calendar: NgbCalendar,
    private recapitulationConfirmedService: RecapitulationConfirmedService,
    private recapitulationPcrService: RecapitulationPcrService,
    private recapitulationPcrSourceService: RecapitulationPcrSourceService,
    private recapitulationCaseTotalService: RecapitulationCaseTotalService,
  ) {
    this.settingsAll();

    this.translateService.onLangChange.subscribe((event) => {
      this.settingsAll();
    });

    this.dataTerkonfirmasiPcrFilter.range.to = calendar.getPrev(calendar.getToday(), 'd', 1);
    this.dataPcrSumberFilter.range.to = calendar.getPrev(calendar.getToday(), 'd', 1);
    this.dataTrenPcrFilter.range.to = calendar.getPrev(calendar.getToday(), 'd', 1);
    this.dataPertumbuhanKasusFilter.range.to = calendar.getPrev(calendar.getToday(), 'd', 1);
  }

  settingsAll(): void {
    this.translateService.get('seo.analisis-page').subscribe((trans) => {
      this.label = trans.title;
      this.description = trans.description;

      // Title & Description
      this.title = this.globalService.title;
      this.titleService.setTitle(this.label);
      this.globalService.changeLabel(this.label);
      this.globalService.changeDescription(this.description);
    });
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.loading = false;
    }, 2000);

    this.getAllData();
  }

  ngOnChanges(changes: SimpleChanges): void {}

  ngOnDestroy(): void {}

  ngAfterViewInit(): void {}

  getAllData(): void {
    this.getRecapitulationConfirmPcr();
    this.getRecapitulationPcrSource();
    this.getRecapitulationTrenPcr();
    this.getRecapitulationKonsentrasiKasus();
  }

  // PCR Sumber
  get getPcrSumberJenisPeriode(): string {
    return `Periode ${_.capitalize(this.dataPcrSumberFilter.jenisPeriode)}`;
  }

  getRecapitulationPcrSource(): void {
    const params = '?periode=' + this.dataPcrSumberFilter.jenisPeriode;

    if (this.dataPcrSumberSubscription) {
      this.dataPcrSumberSubscription.unsubscribe();
    }

    switch (this.dataPcrSumberFilter.periodeOption.key) {
      case 'harian' :
        this.dataPcrSumberSubscription = this.recapitulationPcrSourceService.getDailyItems(params).subscribe(d => {
          this.dataPcrSumber = d.data;
        });

        break;
      case 'mingguan' :
        this.dataPcrSumberSubscription = this.recapitulationPcrSourceService.getWeeklyItems(params).subscribe(d => {
          this.dataPcrSumber = d.data;
        });

        break;
      default :
        this.dataPcrSumberSubscription = this.recapitulationPcrSourceService.getTwoWeeklyItems(params).subscribe(d => {
          this.dataPcrSumber = d.data;
        });

        break;
    }
  }

  filterPcrSumberPeriode(option: any): void {
    this.dataPcrSumberFilter = {
      jenisPeriode: this.dataPcrSumberFilter.jenisPeriode,
      periodeOption: option,
      range: this.dataPcrSumberFilter.range
    };

    this.getRecapitulationPcrSource();
  }

  filterPcrSumberJenisPeriode(periode: any): void {
    this.dataPcrSumberFilter = {
      jenisPeriode: periode,
      periodeOption: this.dataPcrSumberFilter.periodeOption,
      range: this.dataPcrSumberFilter.range
    };

    this.getRecapitulationPcrSource();
  }

  // Terkonfirmasi PCR
  get getKategoriTerkonfirmasiPcr(): string {
    return this.dataTerkonfirmasiPcrFilter.kategori === '' ? 'Rilis Pusat' : 'Penetapan Lab';
  }

  getRecapitulationConfirmPcr(): void {
    const confirmedParams = '?wilayah=provinsi'
      + (this.dataTerkonfirmasiPcrFilter.kategori ? ('&tanggal=' + this.dataTerkonfirmasiPcrFilter.kategori) : '');
    const pcrParams = '';

    if (this.dataTerkonfirmasiSubscription) {
      this.dataTerkonfirmasiSubscription.unsubscribe();
    }

    if (this.dataPcrSubscription) {
      this.dataPcrSubscription.unsubscribe();
    }

    switch (this.dataTerkonfirmasiPcrFilter.periodeOption.key) {
      case 'harian' :
        this.dataTerkonfirmasiSubscription = this.recapitulationConfirmedService.getDailyItems(confirmedParams).subscribe(d => {
          this.dataTerkonfirmasiPcr.terkonfirmasi = d.data;
        });

        this.dataPcrSubscription = this.recapitulationPcrService.getDailyItems(pcrParams).subscribe(d => {
          this.dataTerkonfirmasiPcr.pcr = d.data;
        });

        break;

      case 'mingguan' :
        this.dataTerkonfirmasiSubscription = this.recapitulationConfirmedService.getWeeklyItems(confirmedParams).subscribe(d => {
          this.dataTerkonfirmasiPcr.terkonfirmasi = d.data;
        });

        this.dataPcrSubscription = this.recapitulationPcrService.getWeeklyItems(pcrParams).subscribe(d => {
          this.dataTerkonfirmasiPcr.pcr = d.data;
        });

        break;

      default :
        this.dataTerkonfirmasiSubscription = this.recapitulationConfirmedService.getTwoWeeklyItems(confirmedParams).subscribe(d => {
          this.dataTerkonfirmasiPcr.terkonfirmasi = d.data;
        });

        this.dataPcrSubscription = this.recapitulationPcrService.getTwoWeeklyItems(pcrParams).subscribe(d => {
          this.dataTerkonfirmasiPcr.pcr = d.data;
        });

        break;
    }
  }

  filterTerkonfirmasiPcrPeriode(option: any): void {
    this.dataTerkonfirmasiPcrFilter = {
      jenisPeriode: this.dataTerkonfirmasiPcrFilter.jenisPeriode,
      kategori: this.dataTerkonfirmasiPcrFilter.kategori,
      periodeOption: option,
      range: this.dataTerkonfirmasiPcrFilter.range
    };

    this.getRecapitulationConfirmPcr();
  }

  filterTerkonfirmasiPcrJenisPeriode(periode: any): void {
    this.dataPcrSumberFilter = {
      jenisPeriode: periode,
      kategori: this.dataTerkonfirmasiPcrFilter.kategori,
      periodeOption: this.dataPcrSumberFilter.periodeOption,
      range: this.dataPcrSumberFilter.range
    };

    this.getRecapitulationPcrSource();
  }

  filterKategoriTerkonfirmasiPcr(category: any): void {
    this.dataTerkonfirmasiPcrFilter = {
      jenisPeriode: this.dataTerkonfirmasiPcrFilter.jenisPeriode,
      kategori: category,
      periodeOption: this.dataTerkonfirmasiPcrFilter.periodeOption,
      range: this.dataTerkonfirmasiPcrFilter.range
    };

    this.getRecapitulationConfirmPcr();
  }

  // Tren Positivity
  get getTrenPcrJenisPeriode(): string {
    return `Periode ${_.capitalize(this.dataTrenPcrFilter.jenisPeriode)}`;
  }

  get getPeriodeMingguanOptions(): Array<any> {
    return this.periodeOptions.filter(d => d.key !== 'harian');
  }

  getRecapitulationTrenPcr(): void {
    const pcrParams = '';

    if (this.dataTrenPcrSubscription) {
      this.dataTrenPcrSubscription.unsubscribe();
    }

    switch (this.dataTrenPcrFilter.periodeOption.key) {
      case 'mingguan' :
        this.dataTrenPcrSubscription = this.recapitulationPcrService.getWeeklyItems(pcrParams).subscribe(d => {
          this.dataTrenPcr = d.data;
        });

        break;

      default :
        this.dataTrenPcrSubscription = this.recapitulationPcrService.getTwoWeeklyItems(pcrParams).subscribe(d => {
          this.dataTrenPcr = d.data;
        });

        break;
    }
  }

  filterTrenPcrPeriode(option: any): void {
    this.dataTrenPcrFilter = {
      jenisPeriode: this.dataTrenPcrFilter.jenisPeriode,
      periodeOption: option,
      range: this.dataTrenPcrFilter.range
    };

    this.getRecapitulationTrenPcr();
  }

  filterTrenPcrJenisPeriode(periode: any): void {
    this.dataTrenPcrFilter = {
      jenisPeriode: periode,
      periodeOption: this.dataTrenPcrFilter.periodeOption,
      range: this.dataTrenPcrFilter.range
    };

    this.getRecapitulationTrenPcr();
  }

  // Konsentrasi Kasus
  getRecapitulationKonsentrasiKasus(): void {
    const params = '?wilayah=kota';

    this.recapitulationCaseTotalService.getItems(params).subscribe(d => {
      this.dataKonsentrasiKasus = d.data;
    });
  }

  // Pertumbuhan Kasus
  get getPertumbuhanKasusJenisPeriode(): string {
    return `Periode ${_.capitalize(this.dataPertumbuhanKasusFilter.jenisPeriode)}`;
  }

  getRecapitulationPertumbuhanKasus(): void {
    const confirmedParams = '?wilayah=kota'
      + (this.dataTerkonfirmasiPcrFilter.kategori ? ('&tanggal=' + this.dataTerkonfirmasiPcrFilter.kategori) : '');
    const pcrParams = '';

    if (this.dataPertumbuhanKasusSubscription) {
      this.dataPertumbuhanKasusSubscription.unsubscribe();
    }

    if (this.dataPcrSubscription) {
      this.dataPcrSubscription.unsubscribe();
    }

    switch (this.dataPertumbuhanKasusFilter.periodeOption.key) {
      case 'harian' :
        this.dataPertumbuhanKasusSubscription = this.recapitulationConfirmedService.getDailyItems(confirmedParams).subscribe(d => {
          this.dataPertumbuhanKasus.jabar = d.data;
        });

        this.dataPcrSubscription = this.recapitulationPcrService.getDailyItems(pcrParams).subscribe(d => {
          this.dataPertumbuhanKasus.pcr = d.data;
        });

        break;

      case 'mingguan' :
        this.dataPertumbuhanKasusSubscription = this.recapitulationConfirmedService.getWeeklyItems(confirmedParams).subscribe(d => {
          this.dataPertumbuhanKasus.terkonfirmasi = d.data;
        });

        this.dataPcrSubscription = this.recapitulationPcrService.getWeeklyItems(pcrParams).subscribe(d => {
          this.dataPertumbuhanKasus.pcr = d.data;
        });

        break;

      default :
        this.dataPertumbuhanKasusSubscription = this.recapitulationConfirmedService.getTwoWeeklyItems(confirmedParams).subscribe(d => {
          this.dataPertumbuhanKasus.terkonfirmasi = d.data;
        });

        this.dataPcrSubscription = this.recapitulationPcrService.getTwoWeeklyItems(pcrParams).subscribe(d => {
          this.dataPertumbuhanKasus.pcr = d.data;
        });

        break;
    }
  }

  filterPertumbuhanKasusPeriode(option: any): void {
    this.dataPertumbuhanKasusFilter = {
      jenisPeriode: this.dataPertumbuhanKasusFilter.jenisPeriode,
      periodeOption: option,
      terkonfirmasiOption: this.dataPertumbuhanKasusFilter.terkonfirmasiOption,
      range: this.dataPertumbuhanKasusFilter.range
    };

    this.getRecapitulationPertumbuhanKasus();
  }

  filterPertumbuhanKasusJenisPeriode(periode: any): void {
    this.dataPertumbuhanKasusFilter = {
      jenisPeriode: periode,
      periodeOption: this.dataPertumbuhanKasusFilter.periodeOption,
      terkonfirmasiOption: this.dataPertumbuhanKasusFilter.terkonfirmasiOption,
      range: this.dataPertumbuhanKasusFilter.range
    };

    this.getRecapitulationPertumbuhanKasus();
  }

  filterPertumbuhanKasusJenis(jenis: any): void {
    this.dataPertumbuhanKasusFilter = {
      jenisPeriode: this.dataPertumbuhanKasusFilter.jenisPeriode,
      periodeOption: this.dataPertumbuhanKasusFilter.periodeOption,
      terkonfirmasiOption: jenis,
      range: this.dataPertumbuhanKasusFilter.range
    };

    this.getRecapitulationPertumbuhanKasus();
  }
}
