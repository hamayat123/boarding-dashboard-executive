import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { LocalizeRouterModule } from '@gilsdav/ngx-translate-router';

import { SharedModule } from '@core/shared.module';
import {
  NgbCollapseModule,
  NgbDropdownModule,
  NgbNavModule
} from '@ng-bootstrap/ng-bootstrap';

import { AnalisisPageComponent } from './analisis-page.component';
import { MapPolygonService, RecapitulationDailyService, RecapitulationGrowthService } from '@pikobar-services';

export const routes: Routes = [
  {
    path: '',
    component: AnalisisPageComponent,
  },
];

@NgModule({
  declarations: [AnalisisPageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule,
    LocalizeRouterModule.forChild(routes),
    SharedModule,
    NgbCollapseModule,
    NgbDropdownModule,
    NgbNavModule
  ],
  providers: [
    RecapitulationDailyService,
    RecapitulationGrowthService,
    MapPolygonService
  ],
})
export class AnalisisPageModule {}
