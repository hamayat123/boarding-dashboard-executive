import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { LocalizeRouterModule } from '@gilsdav/ngx-translate-router';

import { SharedModule } from '@core/shared.module';
import { NgbCollapseModule, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

import { TracePageComponent } from './trace-page.component';

export const routes: Routes = [
  {
    path: '',
    component: TracePageComponent,
  },
  {
    path: 'statistik',
    loadChildren: () =>
      import('./statistik-page/statistik-page.module').then((m) => m.StatistikPageModule),
  },
  {
    path: 'proyeksi',
    loadChildren: () =>
      import('./proyeksi-page/proyeksi-page.module').then((m) => m.ProyeksiPageModule),
  },
];

@NgModule({
  declarations: [TracePageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule,
    LocalizeRouterModule.forChild(routes),
    SharedModule,
    NgbCollapseModule,
    NgbDropdownModule,
  ],
})
export class TracePageModule {}
