import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { LocalizeRouterModule } from '@gilsdav/ngx-translate-router';

import { SharedModule } from '@core/shared.module';
import { NgbCollapseModule, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

import { ProyeksiPageComponent } from './proyeksi-page.component';
import { ProjectionTrafficJamService } from '@pikobar-services';

export const routes: Routes = [
  {
    path: '',
    component: ProyeksiPageComponent,
  },
];

@NgModule({
  declarations: [ProyeksiPageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule,
    LocalizeRouterModule.forChild(routes),
    SharedModule,
    NgbCollapseModule,
    NgbDropdownModule,
  ],
  providers: [
    ProjectionTrafficJamService
  ]
})
export class ProyeksiPageModule {}
