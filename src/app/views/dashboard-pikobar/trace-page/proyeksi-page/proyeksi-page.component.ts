import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  AfterViewInit,
  SimpleChanges,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

// SERVICE
import { GlobalService } from '@core/services';
import {
  ProjectionJabarService,
  ProjectionMobilityService,
  ProjectionStayHomeService,
  ProjectionTrafficJamService,
  ProjectionTrendService,
  RegionService
} from '@pikobar-services';

// PACKAGE
import * as _ from 'lodash';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { NgbCalendar } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-proyeksi-page',
  templateUrl: './proyeksi-page.component.html',
  styleUrls: ['./proyeksi-page.component.scss']
})
export class ProyeksiPageComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  moment: any = moment;
  JSON = JSON;

  // Settings
  title!: string;
  label!: string;
  description!: string;
  breadcrumb!: any[];

  // Variable
  public isMenuCollapsed = true;
  dataTrenJabarSubscription: any;
  dataKemacetanSubscription: any;
  dataKemacetan: any;
  dataDirumah: any;
  dataTrenJabar: any;
  dataTrenIndonesia: any;
  dataWilayah: any;
  loading = true;
  dataKemacetanFilter: any = {
    wilayahOption: {
      nama_wilayah: 'JAWA BARAT',
      kode_kemendagri: '32'
    },
    range: {
      from: {
        year: 2020,
        month: 1,
        day: 1
      },
      to: null
    }
  };
  dataDirumahFilter: any = {
    range: {
      from: {
        year: 2020,
        month: 3,
        day: 1
      },
      to: null
    }
  };
  dataTrenJabarFilter: any = {
    kategori: 'pengumuman',
    wilayahOption: {
      nama_wilayah: 'JAWA BARAT',
      kode_bps: '32'
    },
    range: {
      from: {
        year: 2020,
        month: 3,
        day: 10
      },
      to: null
    }
  };
  dataTrenIndonesiaFilter: any = {
    range: {
      from: {
        year: 2020,
        month: 3,
        day: 10
      },
      to: null
    }
  };

  // Mobilitas
  dataMobilitas: any;
  mobilitasOptions: any = [
    {
      key: 'grocery_and_pharmacy_percent_change_from_baseline',
      text: 'Market & Apotik',
    },
    {
      key: 'parks_percent_change_from_baseline',
      text: 'Parkir',
    },
    {
      key: 'residential_percent_change_from_baseline',
      text: 'Perumahan',
    },
    {
      key: 'workplaces_percent_change_from_baseline',
      text: 'Tempat Kerja',
    },
    {
      key: 'retail_and_recreation_percent_change_from_baseline',
      text: 'Toko & Pariwisata',
    },
    {
      key: 'transit_stations_percent_change_from_baseline',
      text: 'Transportasi',
    },
  ];
  dataMobilitasFilter: any = {
    mobilitasOption: this.mobilitasOptions[0],
    range: {
      from: {
        year: 2020,
        month: 3,
        day: 1
      },
      to: null
    }
  };

  // Proyeksi
  dataProyeksi: any;
  proyeksiOptions: any = [
    {
      text: 'Kumulatif',
      incidence: 'cumulative_incidence',
      lower_bound: 'cumulative_lower_bound',
      upper_bound: 'cumulative_upper_bound',
    },
    {
      text: 'Angka Harian',
      incidence: 'daily_incidence',
      lower_bound: 'daily_lower_bound',
      upper_bound: 'daily_upper_bound',
    },
  ];
  dataProyeksiFilter: any = {
    proyeksiOption: this.proyeksiOptions[0],
    range: {
      from: null,
      to: null
    }
  };

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private titleService: Title,
    private globalService: GlobalService,
    private translateService: TranslateService,
    private projectionTrafficJamService: ProjectionTrafficJamService,
    private projectionStayHomeService: ProjectionStayHomeService,
    private projectionJabarService: ProjectionJabarService,
    private projectionTrendService: ProjectionTrendService,
    private projectionMobilityService: ProjectionMobilityService,
    private regionService: RegionService,
    private calendar: NgbCalendar
  ) {
    this.settingsAll();

    this.translateService.onLangChange.subscribe((event) => {
      this.settingsAll();
    });

    this.initCalendar(calendar);
  }

  settingsAll(): void {
    this.translateService.get('seo.proyeksi-page').subscribe((trans) => {
      this.label = trans.title;
      this.description = trans.description;

      // Title & Description
      this.title = this.globalService.title;
      this.titleService.setTitle(this.label);
      this.globalService.changeLabel(this.label);
      this.globalService.changeDescription(this.description);
    });
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.loading = false;
    }, 2000);

    this.getAllData();
  }

  ngOnChanges(changes: SimpleChanges): void {}

  ngOnDestroy(): void {}

  ngAfterViewInit(): void {}

  initCalendar(calendar: any): void {
    this.dataKemacetanFilter.range.from = calendar.getPrev(calendar.getToday(), 'y', 1);
    this.dataDirumahFilter.range.from = calendar.getPrev(calendar.getToday(), 'y', 1);
    this.dataTrenIndonesiaFilter.range.from = calendar.getPrev(calendar.getToday(), 'y', 1);
    this.dataProyeksiFilter.range.from = calendar.getPrev(calendar.getToday(), 'd', 14);
    this.dataMobilitasFilter.range.from = calendar.getPrev(calendar.getToday(), 'y', 1);

    this.dataKemacetanFilter.range.to = calendar.getPrev(calendar.getToday(), 'd', 1);
    this.dataDirumahFilter.range.to = calendar.getPrev(calendar.getToday(), 'd', 1);
    this.dataTrenJabarFilter.range.to = calendar.getPrev(calendar.getToday(), 'd', 1);
    this.dataTrenIndonesiaFilter.range.to = calendar.getPrev(calendar.getToday(), 'd', 1);
    this.dataMobilitasFilter.range.to = calendar.getPrev(calendar.getToday(), 'd', 1);
    this.dataProyeksiFilter.range.to = calendar.getNext(calendar.getToday(), 'd', 1);
  }

  getAllData(): void {
    this.getDataWilayah();
    this.getDataKemacetan();
    this.getDataDirumah();
    this.getDataProyeksi();
    this.getDataTrenJabar();
    this.getDataTrenIndonesia();
    this.getDataMobility();
  }

  getDataKemacetan(): void {
    const params = '?kode_kab_kota=' + this.dataKemacetanFilter.wilayahOption.kode_kemendagri;

    if (this.dataKemacetanSubscription) {
      this.dataKemacetanSubscription.unsubscribe();
    }

    this.dataKemacetanSubscription = this.projectionTrafficJamService.getItems(params).subscribe(d => {
      this.dataKemacetan = d.data[0].detail_date;
    });
  }

  getDataDirumah(): void {
    const params = '?limit=100000';

    this.projectionStayHomeService.getItems(params).subscribe(d => {
      this.dataDirumah = d.data;
    });
  }

  getDataTrenJabar(): void {
    let params = '?sort=date:asc&start_date=2020-03-19';

    if (this.dataTrenJabarSubscription) {
      this.dataTrenJabarSubscription.unsubscribe();
    }

    if (this.dataTrenJabarFilter.wilayahOption.kode_bps === '32') {
      params += '&category=' + this.dataTrenJabarFilter.kategori;

      this.dataTrenJabarSubscription = this.projectionTrendService.getJabarItems(params).subscribe(d => {
        this.dataTrenJabar = d.data;
      });
    } else {
      params += '&kode_kab=' + this.dataTrenJabarFilter.wilayahOption.kode_bps;

      this.dataTrenJabarSubscription = this.projectionTrendService.getKotaItems(params).subscribe(d => {
        this.dataTrenJabar = d.data;
      });
    }
  }

  getDataProyeksi(): void {
    const params = '?sort=date:asc';

    this.projectionJabarService.getItems(params).subscribe(d => {
      this.dataProyeksi = d.data;
    });
  }

  getDataTrenIndonesia(): void {
    const params = '?sort=date:asc&start_date=2020-03-19&category=pengumuman';

    this.projectionTrendService.getIndonesiaItems(params).subscribe(d => {
      this.dataTrenIndonesia = d.data;
    });
  }

  getDataMobility(): void {
    const params = '?sort=date:asc&country_region_code=ID&sub_region_1=West%20Java';

    this.projectionMobilityService.getItems(params).subscribe(d => {
      this.dataMobilitas = d.data;
    });
  }

  getDataWilayah(): void {
    const params = '?level=kabupaten';

    this.regionService.getItems(params).subscribe(d => {
      this.dataWilayah = d.data;
    });
  }

  get getKategori(): string {
    return this.dataTrenJabarFilter.kategori === 'pengumuman' ? 'RILIS PUBLIK' : 'PENETAPAN LAB';
  }

  filterWilayahTrenJabar(wilayah: any): void {
    this.dataTrenJabarFilter = {
      kategori: this.dataTrenJabarFilter.kategori,
      wilayahOption: wilayah,
      range: this.dataTrenJabarFilter.range
    };

    this.getDataTrenJabar();
  }

  filterWilayahKemacetan(wilayah: any): void {
    this.dataKemacetanFilter = {
      wilayahOption: wilayah,
      range: this.dataKemacetanFilter.range
    };

    this.getDataKemacetan();
  }

  filterKategoriTrenJabar(category: any): void {
    this.dataTrenJabarFilter = {
      kategori: category,
      wilayahOption: this.dataTrenJabarFilter.wilayahOption,
      range: this.dataTrenJabarFilter.range
    };

    this.getDataTrenJabar();
  }

  filterMobilitasType(option: any): void {
    this.dataMobilitasFilter = {
      mobilitasOption: option,
      range: this.dataMobilitasFilter.range
    };
  }

  filterProyeksiType(option: any): void {
    this.dataProyeksiFilter = {
      proyeksiOption: option,
      range: this.dataProyeksiFilter.range
    };
  }
}
