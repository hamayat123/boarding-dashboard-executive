import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  AfterViewInit,
  SimpleChanges,
  ChangeDetectorRef,
  ViewChild,
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

// SERVICE
import { GlobalService } from '@services';
import { MapPolygonService, RecapitulationDailyService, RecapitulationGrowthService } from '@pikobar-services';

// PACKAGE
import * as _ from 'lodash';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { ChartTerkonfirmasiComponent } from '@core/components/pikobar/chart-terkonfirmasi/chart-terkonfirmasi.component';

@Component({
  selector: 'app-statistik-page',
  templateUrl: './statistik-page.component.html',
  styleUrls: ['./statistik-page.component.scss'],
})
export class StatistikPageComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  @ViewChild('chartTerkonfirmasi') chartTerkonfirmasi!: ChartTerkonfirmasiComponent;
  moment: any = moment;
  JSON: any = JSON;

  // Settings
  title!: string;
  label!: string;
  description!: string;
  breadcrumb!: any[];
  dataHarianFilter: any = {
    range: {
      from: {
        year: 2020,
        month: 3,
        day: 1
      },
      to: null
    }
  };
  dataRasioFilter = {
    kode: '32',
    level: {
      param: 'kota',
      kode: 'kode_kab',
      nama: 'nama_kab',
      polygon_kode: 'bps_kabupaten_kode',
      polygon_nama: 'kemendagri_kabupaten_nama',
      parent: 'bps_provinsi_kode'
    },
    sebaranType: {
      index : 'confirmation',
      subindex : 'confirmation_diisolasi',
      text: 'Positif - Isolasi / Dalam Perawatan',
      color: '#ae9420'
    }
  };
  levelType: any = [{
    param: 'kota',
    kode: 'kode_kab',
    nama: 'nama_kab',
    polygon_kode: 'bps_kabupaten_kode',
    polygon_nama: 'kemendagri_kabupaten_nama',
    parent: 'bps_provinsi_kode'
  }, {
    param: 'kecamatan',
    kode: 'kode_kec',
    nama: 'nama_kec',
    polygon_kode: 'bps_kecamatan_kode',
    polygon_nama: 'kemendagri_kecamatan_nama',
    parent: 'bps_kabupaten_kode'
  }, {
    param: 'kelurahan',
    kode: 'kode_kel',
    nama: 'nama_kel',
    polygon_kode: 'bps_desa_kode',
    polygon_nama: 'kemendagri_desa_nama',
    parent: 'bps_kecamatan_kode'
  }];

  sebaranType: any = [{
    index : 'confirmation',
    subindex : 'confirmation_diisolasi',
    text: 'Positif - Isolasi / Dalam Perawatan',
    color: '#ae9420'
  }, {
    index : 'confirmation',
    subindex : 'confirmation_selesai',
    text: 'Positif - Selesai Isolasi / Sembuh',
    color: '#00441f'
  }, {
    index : 'confirmation',
    subindex : 'confirmation_meninggal',
    text: 'Positif - Meninggal',
    color: '#7d0005'
  }, {
    index : 'suspect',
    subindex : 'suspect_diisolasi',
    text: 'Suspek - Isolasi / Dalam perawatan',
    color: '#beab8d'
  }, {
    index : 'closecontact',
    subindex : 'closecontact_dikarantina',
    text: 'Kontak Erat - Masih Dikarantina',
    color: '#905a08'
  }, {
    index : 'probable',
    subindex : 'probable_diisolasi',
    text: 'Probable - Isolasi / Dalam Perawatan',
    color: '#ceb546'
  }, {
    index : 'probable',
    subindex : 'probable_meninggal',
    text: 'Probable - Meninggal',
    color: '#7d0005'
  }];

  // Variable
  public isMenuCollapsed = true;
  dataWilayah: any;
  dataHarian: any;
  dataRasio: any;
  dataPolygon: any;
  loading = true;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private titleService: Title,
    private globalService: GlobalService,
    private translateService: TranslateService,
    private calendar: NgbCalendar,
    private recapitulationDailyService: RecapitulationDailyService,
    private recapitulationGrowthService: RecapitulationGrowthService,
    private mapPolygonService: MapPolygonService
  ) {
    this.settingsAll();

    this.translateService.onLangChange.subscribe((event) => {
      this.settingsAll();
    });

    this.dataHarianFilter.range.to = calendar.getPrev(calendar.getToday(), 'd', 1);
  }

  settingsAll(): void {
    this.translateService.get('seo.statistik-page').subscribe((trans) => {
      this.label = trans.title;
      this.description = trans.description;

      // Title & Description
      this.title = this.globalService.title;
      this.titleService.setTitle(this.label);
      this.globalService.changeLabel(this.label);
      this.globalService.changeDescription(this.description);
    });
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.loading = false;
    }, 2000);

    this.getAllData();
  }

  ngOnChanges(changes: SimpleChanges): void {}

  ngOnDestroy(): void {}

  ngAfterViewInit(): void {}

  getAllData(): void {
    this.getRecapitulationDaily();
    this.getRecapitulationGrowth();
    this.getMapPolygon();
  }

  getRecapitulationDaily(): void {
    const params = '?wilayah=provinsi';

    this.recapitulationDailyService.getItems(params).subscribe(d => {
      this.dataHarian = d.data;
    });
  }

  getRecapitulationGrowth(): void {
    const params = '?wilayah=kota';

    this.recapitulationGrowthService.getItems(params).subscribe(d => {
      this.dataRasio = d.data;
    });
  }

  getMapPolygon(): void {
    this.mapPolygonService.getKota().subscribe(d => {
      this.dataPolygon = d;
    });
  }
}
