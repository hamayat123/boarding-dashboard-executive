import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  AfterViewInit,
  SimpleChanges,
  ChangeDetectionStrategy,
  ChangeDetectorRef, ViewChild,
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { FormControl, FormGroup, Validators } from '@angular/forms';

// SERVICE
import { AuthService, GlobalService } from '@core/services';

// PACKAGE
import * as _ from 'lodash';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';
import { Store } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';
import { fromAuthActions } from '@stores/auth/auth.actions';
import Swal from 'sweetalert2';

// MODEL
import { User } from '@auth-models';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginPageComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  moment: any = moment;

  // Settings
  title!: string;
  label!: string;
  description!: string;

  // Variable
  loading = false;
  returnUrl!: string;
  loginForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  });

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private titleService: Title,
    private globalService: GlobalService,
    private translateService: TranslateService,
    private localize: LocalizeRouterService,
    private authService: AuthService,
    private store: Store,
    private actions$: Actions
  ) {
    if (this.authService.dashboardUserValue) {
      const translatedUrl = this.localize.translateRoute('/dashboard-pikobar/trace');
      this.router.navigate([translatedUrl]);
    }

    this.settingsAll();

    this.translateService.onLangChange.subscribe((event) => {
      this.settingsAll();
    });
  }

  settingsAll(): void {
    this.translateService.get('seo.login-page').subscribe((translate) => {
      this.label = translate.title;
      this.description = translate.description;

      // Title & Description
      this.title = this.globalService.title;
      this.titleService.setTitle(this.label);
      this.globalService.changeLabel(this.label);
      this.globalService.changeDescription(this.description);
    });
  }

  ngOnInit(): void {
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/dashboard-pikobar/trace';
  }

  ngOnChanges(changes: SimpleChanges): void {}

  ngOnDestroy(): void {}

  ngAfterViewInit(): void {}

  get form(): any {
    return this.loginForm.controls;
  }

  login(): void {
    if (this.loginForm.invalid) {
      return;
    }

    const body = {
      username: this.form.username.value,
      password: this.form.password.value,
      app_code: 'dashboard-pikobar'
    };

    this.loading = true;
    this.store.dispatch(
      fromAuthActions.loginAuth({ body })
    );

    this.actions$
      .pipe(ofType(fromAuthActions.loginAuthSuccess))
      .subscribe((result: any) => {
        this.loading = false;

        if (result.data.error === 0) {
          this.returnUrl = this.returnUrl.replace('/id', '');

          const translatedUrl = this.localize.translateRoute(this.returnUrl);
          this.router.navigate([translatedUrl]);
        } else if (result.data.error === 1) {
          let message = '';
          if (
            result.data.message ===
            'Login failed, Username or App Code not found'
          ) {
            message = 'Username dan Password tidak ditemukan';
          } else if (
            result.data.message === 'Login failed, Password not match'
          ) {
            message = 'Password tidak cocok';
          }

          Swal.fire(
            'Login Gagal!',
            message,
            'error'
          );
        }
      });

    this.actions$
      .pipe(ofType(fromAuthActions.loginAuthFailure))
      .subscribe(data => {
        this.loading = false;

        Swal.fire(
          'Login Gagal!',
          data.error,
          'error'
        );
      });
  }
}
