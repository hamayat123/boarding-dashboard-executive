import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LocalizeRouterModule } from '@gilsdav/ngx-translate-router';

// RESOLVER
import { SidebarDashboardBapendaResolver } from '@resolvers';

const routes: Routes = [
  {
    path: '',
    resolve: {
      hero: SidebarDashboardBapendaResolver,
    },
    children: [
      {
        path: '',
        loadChildren: () => import('./pajak-page/pajak-page.module').then((m) => m.PajakPageModule),
      },
      {
        path: 'pajak',
        loadChildren: () => import('./pajak-page/pajak-page.module').then((m) => m.PajakPageModule),
      },
      {
        path: 'indikator-kinerja-utama',
        loadChildren: () => import('./liku-page/liku-page.module').then((m) => m.LikuPageModule),
      },
      {
        path: 'kinerja-instansi-pemerintah',
        loadChildren: () => import('./lkip-page/lkip-page.module').then((m) => m.LkipPageModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), LocalizeRouterModule.forChild(routes)],
  exports: [RouterModule, LocalizeRouterModule],
})
export class DashboardBapendaRoutingModule {}
