import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardBapendaRoutingModule } from './dashboard-bapenda-routing.module';

@NgModule({
  imports: [CommonModule, DashboardBapendaRoutingModule]
})
export class DashboardBapendaModule {}
