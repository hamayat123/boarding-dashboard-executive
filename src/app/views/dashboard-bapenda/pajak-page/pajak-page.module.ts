import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { LocalizeRouterModule } from '@gilsdav/ngx-translate-router';

import { SharedModule } from '@core/shared.module';
import { NgbCollapseModule, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

import { PajakPageComponent } from './pajak-page.component';

export const routes: Routes = [
  {
    path: '',
    component: PajakPageComponent,
  },
  {
    path: 'kendaraan-bermotor',
    loadChildren: () =>
      import('./pajak-kendaraan-page/pajak-kendaraan-page.module').then((m) => m.PajakKendaraanPageModule),
  },
  {
    path: 'bahan-bakar',
    loadChildren: () =>
      import('./pajak-bahan-bakar-page/pajak-bahan-bakar-page.module').then((m) => m.PajakBahanBakarPageModule),
  },
  {
    path: 'air-permukaan',
    loadChildren: () =>
      import('./pajak-air-permukaan-page/pajak-air-permukaan-page.module').then((m) => m.PajakAirPermukaanPageModule),
  },
  {
    path: 'rokok',
    loadChildren: () =>
      import('./pajak-rokok-page/pajak-rokok-page.module').then((m) => m.PajakRokokPageModule),
  },
];

@NgModule({
  declarations: [PajakPageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule,
    LocalizeRouterModule.forChild(routes),
    SharedModule,
    NgbCollapseModule,
    NgbDropdownModule,
  ],
})
export class PajakPageModule {}
