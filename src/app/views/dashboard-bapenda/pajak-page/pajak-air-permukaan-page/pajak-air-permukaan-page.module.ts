import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { LocalizeRouterModule } from '@gilsdav/ngx-translate-router';

import { SharedModule } from '@core/shared.module';
import { NgbCollapseModule, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

import { PajakAirPermukaanPageComponent } from './pajak-air-permukaan-page.component';

export const routes: Routes = [
  {
    path: '',
    component: PajakAirPermukaanPageComponent,
  },
];

@NgModule({
  declarations: [PajakAirPermukaanPageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule,
    LocalizeRouterModule.forChild(routes),
    SharedModule,
    NgbCollapseModule,
    NgbDropdownModule,
  ],
})
export class PajakAirPermukaanPageModule {}
