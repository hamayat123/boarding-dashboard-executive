
export const environment = {
  refreshData: 60 * 1000,
  environment: 'undefined',
  production: false,
  locales: 'http://localhost:4200/',
  apiPublicUrl: 'https://covid19-public.digitalservice.id/api/v1/',
  apiExecutiveUrl: 'https://covid19-executive.digitalservice.id/api/v1/',
  apiDashboardUrl: 'https://dashboard-pikobar-dev.digitalservice.id/',
  apiDashboardApiUrl: 'https://dashboard-pikobar-api.digitalservice.id/'
};
